<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewComposerProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bankAccountsViewComposer();
        $this->banksViewComposer();
        $this->chestsViewComposer();
        $this->customersViewComposer();
        $this->factorCreditsViewComposer();        
        $this->factorNumberViewComposer();
        $this->goodsViewComposer();
        $this->incomeReportFiltersViewCompoer();        
        $this->incomeCheckReportFiltersViewCompoer();        
        $this->spendItemNumberViewComposer();        
        $this->spendItemsViewComposer();
        $this->subscriptionStatusViewComposer();        
        $this->subscriptionViewComposer();        
        $this->typesViewComposer();
        $this->warehousesViewComposer();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function incomeReportFiltersViewCompoer()
    {
        View::composer(
            [
                'dashboard.turnovers.incomes.bank.report',
                'dashboard.turnovers.incomes.chest.report',
                'dashboard.turnovers.incomes.check.report',                
            ],
            \App\Http\ViewComposers\IncomeReportFilterViewComposer::class
        );
    }        
    
    public function incomeCheckReportFiltersViewCompoer()
    {
        View::composer(
            [
                'dashboard.turnovers.incomes.check.report',                
            ],
            \App\Http\ViewComposers\IncomeCheckReportFilterViewComposer::class
        );
    }        

    protected function warehousesViewComposer()
    {
        View::composer(
            [
                'dashboard.goods.form',
                'dashboard.goods.index',
            ],
            \App\Http\ViewComposers\WarehousesViewComposer::class
        );
    }

     protected function typesViewComposer()
    {
        View::composer(
            [
                'dashboard.customers.form',
                'dashboard.customers.index',
            ],
            \App\Http\ViewComposers\TypesViewComposer::class
        );
    }

    protected function banksViewComposer()
    {
        View::composer(
            [
                'dashboard.turnovers.incomes.bank.report',
                'dashboard.turnovers.incomes.check.report',
                'dashboard.bank_accounts.form',
                'dashboard.turnovers.incomes.check.form',
                'dashboard.turnovers.outcomes.check.form',
            ],
            \App\Http\ViewComposers\BanksViewComposer::class
        );
    }

    protected function customersViewComposer()
    {
        View::composer(
            [
                // 'dashboard.factors.dealers',
                // 'dashboard.sell_factors.dealers',
                // 'dashboard.buy_factors.dealers',
                'dashboard.turnovers.incomes.bank.report',
                'dashboard.turnovers.incomes.chest.report',
                'dashboard.turnovers.incomes.check.report',                
                'dashboard.turnovers.incomes.bank.form',
                'dashboard.turnovers.incomes.chest.form',
                'dashboard.turnovers.incomes.check.spend',
                'dashboard.turnovers.incomes.check.form',
                'dashboard.turnovers.outcomes.bank.form',
                'dashboard.turnovers.outcomes.chest.form',
                'dashboard.turnovers.outcomes.check.form',
            ],
            \App\Http\ViewComposers\CustomersViewComposer::class
        );
    }

    protected function bankAccountsViewComposer()
    {
        View::composer(
            [
                'dashboard.turnovers.incomes.bank.report',
                'dashboard.turnovers.incomes.chest.report',
                'dashboard.turnovers.incomes.check.report',
                'dashboard.turnovers.incomes.bank.form',
                'dashboard.turnovers.incomes.check.pass',
                'dashboard.turnovers.outcomes.bank.form',
                'dashboard.expenses.form',
            ],
            \App\Http\ViewComposers\BankAccountsViewComposer::class
        );
    }

    protected function chestsViewComposer()
    {
        View::composer(
            [
                'dashboard.turnovers.incomes.chest.report',
                'dashboard.turnovers.incomes.chest.form',
                'dashboard.turnovers.outcomes.chest.form',
                'dashboard.expenses.form',
            ],
            \App\Http\ViewComposers\ChestsViewComposer::class
        );
    }

    protected function spendItemsViewComposer()
    {
        View::composer(
            [
                'dashboard.expenses.form',
            ],
            \App\Http\ViewComposers\SpendItemsViewComposer::class
        );
    }

    protected function goodsViewComposer()
    {
        View::composer(
            [
                // 'dashboard.factors.goods',
                // 'dashboard.sell_factors.goods',
                // 'dashboard.buy_factors.goods',
            ],
            \App\Http\ViewComposers\GoodsViewComposer::class
        );
    }

    protected function factorCreditsViewComposer($value='')
    {
        View::composer('dashboard.partials.sidebar',
            \App\Http\ViewComposers\FactorCreditsViewComposer::class
        );
    }

    protected function factorNumberViewComposer()
    {
        View::composer('dashboard.factors.dealers',
            \App\Http\ViewComposers\FactorNumberViewComposer::class
        );
        View::composer('dashboard.sell_factors.dealers',
            \App\Http\ViewComposers\SellFactorNumberViewComposer::class
        );
        View::composer('dashboard.buy_factors.dealers',
            \App\Http\ViewComposers\BuyFactorNumberViewComposer::class
        );
    }

    protected function spendItemNumberViewComposer()
    {
        View::composer('dashboard.spend_items.create',
            \App\Http\ViewComposers\SpendItemNumberViewComposer::class
        );
    }

    protected function subscriptionStatusViewComposer()
    {
        View::composer([
                'dashboard.partials.page-bar',                
            ],
            \App\Http\ViewComposers\SubscriptionStatusViewComposer::class
        );
    }

    protected function subscriptionViewComposer()
    {
        View::composer([
                'dashboard.sell_factors.dealers',                
                'dashboard.buy_factors.dealers',                
            ],
            \App\Http\ViewComposers\SubscriptionViewComposer::class
        );
    }
}
