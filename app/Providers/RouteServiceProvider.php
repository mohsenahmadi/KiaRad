<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();    
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapTenantRoutes();
        
        $this->mapWebRoutes();
        
        $this->mapTenantDashboardRoutes();
        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapwebRoutes()
    {
        Route::middleware('web', 'system.enforce')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "tenant" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapTenantRoutes()
    {        
        Route::middleware('web', 'tenant.check')
             ->namespace($this->namespace)
             ->domain('{account}.'.config('app.url_base'))
             ->group(base_path('routes/tenant.php'));
    
    }

    /**
     * Define the "dashboard" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapTenantDashboardRoutes()
    {
        Route::middleware(['web', 'auth', 'tenant.check'])
             ->domain('{account}.'.config('app.url_base'))
             ->as('dashboard.')
             ->prefix('dashboard')
             ->namespace($this->namespace.'\Dashboard')
             ->group(base_path('routes/dashboard.php'));        
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
