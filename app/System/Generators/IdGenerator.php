<?php

namespace App\System\Generators;

use Hyn\Tenancy\Contracts\Website\UuidGenerator;
use Hyn\Tenancy\Contracts\Website;
use Hyn\Tenancy\Generators\Uuid\ShaGenerator;

class IdGenerator extends ShaGenerator
{
    /**
     * @param Website $website
     * @return string
     */
    public function generate(Website $website) : string
    {        
        if (config('database.connections.system.driver') == "mysql") {
            $app_name = substr(camel_case(config('app.name')), 0, 10);
            $customer_name = substr(camel_case($website->customer->name), 0, 14);
            $id = $app_name.'-'
                    .$customer_name.'-'
                    .str_random(36 - strlen($app_name) - strlen($customer_name));
            
            if (config('tenancy.website.uuid-limit-length-to-32')) {
                $id = substr($id, 0, 32);
            }
        } else {
            $id = parent::generate($website);
        }

        return $id;
    }
}
