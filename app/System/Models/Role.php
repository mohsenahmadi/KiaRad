<?php

namespace App\System\Models;

use Hyn\Tenancy\Traits\UsesSystemConnection;
use Spatie\Permission\Models\Role as BaseRole;

class Role extends BaseRole
{
    use UsesSystemConnection;
}