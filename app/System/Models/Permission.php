<?php

namespace App\System\Models;

use Hyn\Tenancy\Traits\UsesSystemConnection;
use Spatie\Permission\Models\Permission as BasePermission;

class Permission extends BasePermission
{
    use UsesSystemConnection;
}