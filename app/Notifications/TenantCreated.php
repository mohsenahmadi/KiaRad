<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Password;

class TenantCreated extends Notification
{
    private $hostname;

    public function __construct($hostname)
    {
        $this->hostname = $hostname;
    }

    public function via()
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        $token = Password::broker()->createToken($notifiable);
        $resetUrl = "https://{$this->hostname->fqdn}/password/reset/{$token}";

        $app = config('app.name');

        return (new MailMessage())
            ->subject("دعوتنامه {$app}")
            ->greeting("کاربرگرامی سلام")
            ->line("به نرم افزار کیاراد خوش آمدید")
            ->line('برای استفاده ازنرم افزار می بایست \"رمز عبور\" خود را بابت امنیت بیشتر تنظیم نمایید.')
            ->action('تنظیم رمز عبور', $resetUrl)
            ->line('با آرزوی موفقیت');
    }
}
