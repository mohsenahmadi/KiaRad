<?php

namespace App\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

class SpendItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->spend_item ? $this->spend_item->id : null;
        
        $rules = [
            'name' => 'required|unique:tenant.spend_items,name'.( $id ? ','.$id : '')
        ];

        if(!$id) {
            $rules['code'] = 'required|unique:tenant.spend_items,code';
        }
        
        return $rules;
    }
}
