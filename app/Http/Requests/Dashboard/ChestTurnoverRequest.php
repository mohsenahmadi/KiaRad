<?php

namespace App\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

class ChestTurnoverRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_id' => 'required|exists:tenant.customers,id',
            'transaction_id' => 'required|exists:tenant.chests,id',
            'turnedover_at' => 'required|date',
            'amount' => 'required|numeric|min:0'
        ];
    }
}
