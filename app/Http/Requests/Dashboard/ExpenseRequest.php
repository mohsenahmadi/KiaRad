<?php

namespace App\Http\Requests\Dashboard;

use App\Tenant\Models\BankAccount;
use App\Tenant\Models\Chest;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ExpenseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $transaction_type = '';
        if($this->request->get('transaction_type') === BankAccount::class) {
            $transaction_type = '|exists:tenant.bank_accounts,id';
        }else if($this->request->get('transaction_type') === Chest::class) {
            $transaction_type = '|exists:tenant.chest,id';
        }

        return [
            'spend_id' => 'required|exists:tenant.spend_items,id',
            'source_type' => ['required', Rule::in([BankAccount::class, Chest::class])],
            'source_id' => 'required'.$transaction_type,
            'amount' => 'required|numeric|min:0',
            'expended_at' => 'required|date'
        ];
    }
}
