<?php

namespace App\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

class CheckTurnoverRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_id' => 'required|exists:tenant.customers,id',
            'turnedover_at' => 'required|date',
            'amount' => 'required|numeric|min:0',
            'check.bank_id' => 'required|exists:tenant.banks,id',
            'check.due_at' => 'required|date',
            'check.number' => 'required|numeric|min:0|unique:tenant.checks,number',            
        ];
    }
}
