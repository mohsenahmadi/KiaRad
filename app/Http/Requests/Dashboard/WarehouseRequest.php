<?php

namespace App\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

class WarehouseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->warehouse ? $this->warehouse->id : null;

        return [
            'name' => 'required|unique:tenant.warehouses,name'. ($id ? ','.$id : ''),
            'type' => 'required|in:GOOD,SERVICE'
        ];
    }
}
