<?php

namespace App\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

class BankAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->bank_account ? $this->bank_account->id : null;
        
        return [
            'bank_id' => 'required|exists:tenant.banks,id',
            'account_number' => 'required|numeric|unique:tenant.bank_accounts,account_number'. ($id ? ','.$id : ''),
            'IBAN' => ['nullable', 'numeric', 'unique:tenant.bank_accounts,IBAN'. ($id ? ','.$id : ''), 'regex:/^\d{24}$/'],
            'card_number' => ['nullable', 'numeric', 'unique:tenant.bank_accounts,card_number'. ($id ? ','.$id : ''), 'regex:/^\d{16}$/'],
        ];
    }

    public function attributes()
    {
        return [
            'bank_id' => 'بانک',
            'account_number' => 'شماره حساب',
            'IBAN' => 'شماره شبا',
            'card_number' => 'شماره کارت'
        ];
    }

    public function messages()
    {
        return [
            'IBAN.regex' => 'شماره شبا ۲۴ رقم نمیباشد',
            'card_number.regex' => 'شماره کارت ۱۶ رقم نمیباشد'
        ];
    }
}
