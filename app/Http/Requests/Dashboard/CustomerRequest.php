<?php

namespace App\Http\Requests\Dashboard;

use App\Tenant\Models\Type;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->customer ? $this->customer->id : null;

        $rules = [
            'type_id' => 'required|exists:tenant.types,id',
            'name' => 'required|unique:tenant.customers,name'. ($id ? ','.$id : ''),
            'state_code' => 'nullable|required_with:phone|numeric|unique_with:customers,state_code,phone'. ($id ? ','.$id : ''),
            'phone' => 'nullable|required_with:state_code|numeric|unique_with:customers,state_code,phone'. ($id ? ','.$id : ''),
            'mobile' => 'nullable|numeric|unique:tenant.customers,mobile'. ($id ? ','.$id : ''),
            'address' => 'nullable|string',
        ];

        if(!$id) {
            $rules['code'] = 'required|unique:tenant.customers,code';
        }

        $type_id = $this->request->get('type_id');

        $type = Type::find($type_id);

        if($type) {
            if($type->name == 'person') {
                $rules = array_merge($rules, [
                    'data.id_number' => ['nullable', 'regex:/^\d{10}$/'],
                ]);
            } else if($type->name == 'company') {
                $rules = array_merge($rules, [
                    'data.national_number' => ['nullable', 'numeric', 'regex:/^\d{11}$/'],
                    'data.economic_code' => ['nullable', 'numeric', 'regex:/^\d{12}$/']
                ]);
            }
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'type_id' => 'نوع مشتری',
            'code' => 'کد مشتری',
            'name' => 'نام',
            'state_code' => 'کد شهر',
            'phone' => 'شماره تلفن',
            'mobile' => 'شماره موبایل',
            'address' => 'آدرس',
            'id_number' => 'شماره ملی',
            'national_number' => 'شناسه ملی',
            'economic_code' => 'کد اقتصادی'
        ];
    }

    public function messages()
    {
        return [
            'data.id_number.regex' => 'شماره ملی ۱۰ رقم نمیباشد',
            'data.national_number.regex' => 'شناسه ملی ۱۱ رقم نمیباشد',
            'data.economic_code.regex' => 'کد اقتصادی ۱۲ رقم نمیباشد',
            'state_code.unique_with' => 'ترکیب شماره تلفن و کد شهر تکراری است',
            'phone.unique_with' => 'ترکیب شماره تلفن و کد شهر تکراری است' 
        ];
    }
}
