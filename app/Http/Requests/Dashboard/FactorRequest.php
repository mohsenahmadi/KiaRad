<?php

namespace App\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

class FactorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->factor ? $this->factor->id : null;
        
        return [
            'number' => 'nullable|unique:tenant.factors,number'. ($id ? ','.$id : ''),
            'customer_id' => 'required|exists:tenant.customers,id',
            'goods' => 'required|array|min:1',
            'goods.*.id' => 'required|exists:tenant.goods,id',
            'goods.*.price' => 'required|numeric|min:0',
            'goods.*.quantity' => 'required|numeric|min:1',
            'goods.*.discount' => 'nullable|numeric|min:0',
            'goods.*.tax' => 'nullable|numeric|min:0',
            'official' => 'nullable|boolean'
        ];
    }
}
