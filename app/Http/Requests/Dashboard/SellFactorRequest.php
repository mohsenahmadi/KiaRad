<?php

namespace App\Http\Requests\Dashboard;

use Illuminate\Validation\Rule;

use Illuminate\Foundation\Http\FormRequest;

class SellFactorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $db = config('database.connections.tenant.database');
        config(['database.default' => 'tenant']);
        
        $id = $this->factor ? $this->factor->id : null;

        return [
            'number' => 'required|unique_with:sell_factors,number,official'.($id ? ','.$id : ''),
            'issued_at' => 'required|date',
            'customer_id' => 'required|exists:customers,id',
            'goods' => 'required|array|min:1',
            'goods.*.id' => 'required|exists:goods,id',
            'goods.*.price' => 'required|numeric|min:0',
            'goods.*.quantity' => 'required|numeric|min:1',
            'goods.*.discount' => 'nullable|numeric|min:0',
            'goods.*.tax' => 'nullable|numeric|min:0',
            'official' => 'nullable|boolean'
        ];
    }

    public function attributes()
    {
        return [
            'number' => 'شماره',
            'issued_at' => 'تاریخ',
            'customer_id' => 'مشتری',
            'goods' => 'کالاها',
            'goods.*.id' => 'کالا',
            'goods.*.price' => 'قیمت کالا',
            'goods.*.quantity' => 'تعداد کالا',
            'goods.*.discount' => 'تخفیف کالا',
            'goods.*.tax' => 'مالیات کالا',
            'official' => 'فاکتور رسمی'
        ];
    }
}
