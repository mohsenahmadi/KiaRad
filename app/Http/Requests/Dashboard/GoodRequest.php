<?php

namespace App\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

class GoodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->good ? $this->good->id : null;

        $rules = [
            'parent_id' => 'nullable|exists:tenant.goods,id,is_cat,1',
            'warehouse_id' => 'required_with:is_cat|exists:tenant.warehouses,id',
            'name' => 'nullable|unique:tenant.goods,name'. ($id ? ','.$id : ''), 
            'suggestion_buy_price' => 'nullable|numeric|min:0', 
            'suggestion_sell_price' => 'nullable|numeric|min:0', 
            'unit' => 'nullable|string',
            'service' => 'nullable|boolean', 
            'tax' => 'nullable|numeric|min:0|max:100', 
            'desctiption' => 'nullable|string',
            'is_cat' => 'nullable|boolean'
        ];

        if(!$id) {
            $rules['code'] = 'required|unique:tenant.goods,code';            
        }

        return $rules;
    }
}
