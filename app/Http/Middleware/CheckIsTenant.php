<?php

namespace App\Http\Middleware;

use Config;

use Hyn\Tenancy\Models\Hostname;
use Illuminate\Support\Facades\URL;

use Closure;

class CheckIsTenant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $domain = $request->root();
        if(($index = strpos($domain, '//')) !== false) {
            $domain = substr($domain, $index + 2);
        }

        if(!Hostname::where('fqdn', $domain)->exists()) {
            abort(404);
        }
        
        $subdomain = explode('.', $domain)[0];
        config(['app.subdomain' => $subdomain]);
        URL::defaults(['account' => $subdomain]);

        $request->route()->forgetParameter('account');

        return $next($request);
    }
}
