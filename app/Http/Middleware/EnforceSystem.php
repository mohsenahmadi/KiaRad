<?php

namespace App\Http\Middleware;

use App\System\Models\Permission as SystemPermission;
use App\System\Models\Role as SystemRole;

use Spatie\Permission\Contracts\Permission as PermissionContract;
use Spatie\Role\Contracts\Role as RoleContract;

use App;
use Closure;
use Illuminate\Support\Facades\Config;

class EnforceSystem
{
    public function handle($request, Closure $next)
    {
        config(['database.default' => 'system']);
        config(['auth.providers.users.model' => \App\System\Models\User::class]);
        
        // config(['permission.models.permission' => SystemPermission::class]);
        // config(['permission.models.role' => SystemRole::class]);
        
        App::bind(PermissionContract::class, SystemPermission::class);
        App::bind(RoleContract::class, SystemRole::class);

        return $next($request);
    }
}