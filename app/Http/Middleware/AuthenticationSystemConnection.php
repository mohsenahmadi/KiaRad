<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Contracts\Auth\Factory as Auth;

class AuthenticationSystemConnection extends Authenticate
{

    public function __construct(Auth $auth)
    {
        config(['database.default' => 'system']);
        config(['auth.providers.users.model' => \App\System\Models\User::class]);

        parent::__construct($auth);
    }
    
}
