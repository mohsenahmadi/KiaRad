<?php

namespace App\Http\Middleware;

use Carbon\Carbon;

use App\Tenant\Models\Subscription;

use Closure;

class CheckSubscription
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Subscription::where('expired_at', '>', Carbon::now())->exists())
            return $next($request);
        
        return back();
    }
}
