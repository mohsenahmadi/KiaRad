<?php
namespace App\Http\Middleware;

use Closure;

class EnforceTenancy
{
    public function handle($request, Closure $next)
    {
        config(['database.default' => 'tenant']);
 
        return $next($request);
    }
}