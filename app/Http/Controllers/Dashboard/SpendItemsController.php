<?php

namespace App\Http\Controllers\Dashboard;

use App\Tenant\Models\SpendItem;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\SpendItemRequest;

class SpendItemsController extends Controller
{
	public function index()
	{
		$spend_items = SpendItem::paginate(20);

		return view('dashboard.spend_items.index', compact('spend_items'));
	}

	public function search(Request $request)
	{
		$spend_items = SpendItem::where('code', 'like', '%'.$request->q.'%')
							->orWhere('name', 'like', '%'.$request->q.'%');				

		if($request->ajax()) {			
			$spend_items = $spend_items->take(10)->get();
			
			return response()->json($spend_items, 200);
		}
	}

	public function create()
	{
		return view('dashboard.spend_items.create');
	}

	public function store(SpendItemRequest $request)
	{
		$data = $request->all();

		$spend_item = SpendItem::create($data);

		if(!$spend_item){
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return redirect()->route('dashboard.spend_items.edit', $spend_item->id);
	}

	public function edit(SpendItem $spend_item)
	{
		return view('dashboard.spend_items.edit', compact('spend_item'));
	}

	public function update(SpendItemRequest $request, SpendItem $spend_item)
	{
		$data = $request->except('code');

		$update = $spend_item->update($data);

		if(!$update){
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return back();
	}

	public function destroy(SpendItem $spend_item)
	{
		$delete = $spend_item->delete();

		if(!$delete){
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return redirect()->route('dashboard.spend_items.index');
	}
}
