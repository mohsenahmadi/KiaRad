<?php

namespace App\Http\Controllers\Dashboard;

use App\Tenant\Models\Expense;
use App\Tenant\Models\SpendItem;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\ExpenseRequest;

class ExpensesController extends Controller
{
	public function index()
	{
		$spends = Expense::with('spend')->groupBy('spend_id')->selectRaw('spend_id, sum(amount) as total')->get();

		return view('dashboard.expenses.index', compact('spends'));
	}

	public function show(Request $request, $spend)
	{
		$spend = SpendItem::with('expenses')->find($spend);

		if(!$spend) return back();

		$expenses = $spend->expenses(); 

		if($request->has('begin') && !empty($request->begin)) {
			$expenses = $expenses->where('expended_at', '>=', $request->begin);
		}
		if($request->has('end') && !empty($request->end)) {
			$expenses = $expenses->where('expended_at', '<=', $request->end);
		}

		$expenses = $expenses->get();

		return view('dashboard.expenses.show', compact('spend', 'expenses'));
	}

	public function create()
	{
		return view('dashboard.expenses.create');
	}

	public function store(ExpenseRequest $request)
	{
		$data = $request->all();

		$expense = Expense::create($data);
		
		if(!$expense){
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return redirect()->route('dashboard.expenses.create');
		// return redirect()->route('dashboard.expenses.edit', $expense->id);
	}

	public function edit(Expense $expense)
	{
		return view('dashboard.expenses.edit', compact('expense'));
	}

	public function update(ExpenseRequest $request, Expense $expense)
	{
		$data = $request->all();

		$update = $expense->update($data);

		if(!$update){
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return back();	
	}

	public function destroy(Expense $expense)
	{
		$delete = $expense->delete();

		if(!$delete){
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return redirect()->route('dashboard.expenses.index');
	}
}
