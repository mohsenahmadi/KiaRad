<?php

namespace App\Http\Controllers\Dashboard;

use DB;

use Carbon\Carbon;

use App\Tenant\Models\SellFactor;
use App\Tenant\Models\BuyFactor;
use App\Tenant\Models\Expense;
use App\Tenant\Models\Good;
use App\Tenant\Models\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
	public function index()
	{
		$statistics = [
			'total_sell' => SellFactor::sum('total'),
			'total_buy' => BuyFactor::sum('total'),
			'total_expense' => Expense::sum('amount'),
			'goods_count' => Good::whereIsLeaf()->count()
		];

		$graphs = [
			'buys' => BuyFactor::select('issued_at', DB::raw('sum(total) as total'))
								->groupBy('issued_at')->orderBy('issued_at','asc')->get(),
			'sells' => SellFactor::select('issued_at', DB::raw('sum(total) as total'))
								->groupBy('issued_at')->orderBy('issued_at','asc')->get(),
		];

// dd($graphs['buys']->pluck('issued_at')->combine($graphs['buys']->pluck('total'))->toJson());
// dd($graphs['buys']->each(function($e){ return $e->values();}));
		$sell_factors = SellFactor::orderBy('created_at', 'desc')->take(5)->get();
		$buy_factors = SellFactor::orderBy('created_at', 'desc')->take(5)->get();

		return view('dashboard.home', compact('statistics', 'graphs', 'sell_factors', 'buy_factors'));
	}
}
