<?php

namespace App\Http\Controllers\Dashboard;

use Carbon\Carbon;

use Zarinpal\Laravel\Facade\Zarinpal;

use App\Tenant\Models\Payment;
use App\Tenant\Models\AccountCredit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentsController extends Controller
{
	public function plans()
	{
		return view('dashboard.payments.plans');
	}

	public function pay(Request $request)
	{
		$plans = ['pack1' => 50000, 'pack2' => 75000, 'pack2' => 100000];

		$payment = Payment::create(['amount' => $plans[$request->plan]]);		

        $response = Zarinpal::request(
            route('dashboard.payments.verify', $payment->id),
            $payment->amount,
            'پرداخت کیاراد'
        );
        
        if(isset($response['Authority'])) {
            $authority = $response['Authority'];
            return Zarinpal::redirect($authority);
        }

        return 'Error, Status: '.$response['error'];
	}

	public function verify(Request $request, Payment $payment)
	{
        if(is_null($payment->status) || empty($payment->status)){
            $plans = [50000 => 50, 75000 => 100, 100000 => 200];        	
            $response = Zarinpal::verify(
	        	$request->Status,    // $_GET['Status']
	        	$payment->amount,
	        	$request->Authority // $_GET['Authority']
	        );
            if(isset($response) && ($response['Status'] === 'verified_before' || $response['Status'] === 'success')) {
                $payment['transaction_code'] = $response['RefID'];
                $payment['status'] = 100;
                // $payment['message'] = $response['Message'];
                $payment->save();

                if(!$payment->factor_credit) {
                    $credit = $payment->factor_credit()->create([
                            'buy_factor' => $plans[$payment->amount], 
                            'sell_factor' => $plans[$payment->amount]
                        ]);
                }
            }else{
                // Log::alert('Zarinpal Error message: '.($response['status'] ?? print_r($response)).(json_encode($payment)));
                // dump($response);

                $payment['status'] = $response['Status'];
                // $payment['message'] = $response['Message'];
                $payment->save();
            }
        }

        return redirect()->route('dashboard.payments.verify.result', [$payment['status'] === 100 ? 'success' : 'fail', 'payment_id' => $payment->id]); 
	}

	public function result($status, Payment $payment)
	{
		return view('dashboard.payments.result', compact('payment'));
	}	
}
