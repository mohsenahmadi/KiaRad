<?php

namespace App\Http\Controllers\Dashboard;

use Validator;
use Route;

use App\Tenant\Models\BuyFactor as Factor;
use App\Tenant\Models\Customer;
use App\Tenant\Models\AccountData;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Dashboard\BuyFactorRequest as FactorRequest;

class BuyFactorsController extends Controller
{
	public function index(Request $request)
	{		
		$factors = new Factor;

		$official = $request->get('official', -1);
		if($official != -1) {
			$factors = $factors->where('official', $official);
		}

		$factors = $factors->with('customer')->paginate(20);
		
		return view('dashboard.buy_factors.index', compact('factors'));
	}

	public function create()
	{
	    	return view('dashboard.buy_factors.create');
	}

	public function store(FactorRequest $request)
	{
		$data = $request->except(['official', 'user', 'customer', 'goods']);
		$data['official'] = $request->get('official', false);

		if($data['official']) {
			$data['number'] *= -1;
		}

		$customer = Customer::find($data['customer_id']);

		if($data['official']) {
			$validator = $this->customValidate($request->only(['customer', 'user']), $customer);

			if ($validator->fails()) {
				return back()->withErrors($validator)->withInput();
			}

			$this->updateDealers($request->only(['customer', 'user']), $customer);
		}

		$factor = Factor::create($data);

		$totals = $factor->addGoods($request->goods);
		$update = $factor->update($totals);
		
		return redirect()->route('dashboard.buy.create');
		// return redirect()->route('dashboard.buy.edit', $factor->id);
	}

	public function edit($factor)
	{
		$factor = Factor::with('goods', 'customer', 'customer.data')->find($factor);

		return view('dashboard.buy_factors.edit', compact('factor'));
	}

	public function update(FactorRequest $request, Factor $factor)
	{
		$data = $request->except(['official', 'user', 'customer', 'goods']);
		$data['official'] = $request->get('official', false);

		if($data['official']) {
			$data['number'] *= -1;
		}
		
		$customer = Customer::find($data['customer_id']);

		if($request->get('official', false)) {
			$validator = $this->customValidate($request->only(['customer', 'user']), $customer);

			if ($validator->fails()) {
				return back()->withErrors($validator)->withInput();
			}

			$this->updateDealers($request->only(['customer', 'user']), $customer);
		}

		$update = $factor->update($data);

		$factor->goods()->detach();
		$totals = $factor->addGoods($request->goods);
		$update = $factor->update($totals);
		
		return back();
	}



	public function updateDealers($data, $customer)
	{

		if(isset($data['user'])) {
			foreach($data['user'] as $key => $value){
				AccountData::create(['key' => $key, 'value' => $value]);
			}
		}

		if(isset($data['customer'])) {
			$fields = ['address', 'phone', 'mobile', 'address'];
			$customer->update(array_only($data['customer'], $fields));
			foreach(array_except($data['customer'], $fields) as $key => $value){
				$customer->data()->create(['key' => $key, 'value' => $value]);
			}
		}
	}

	private function customValidate($data, $customer)
	{
		$rules = [];
		
		if(!AccountData::where('key', 'id_number')->exists()) 
			$rules['user.id_number'] = ['required', 'regex:/^\d{10}$/'];
		if(!AccountData::where('key', 'address')->exists())
			$rules['user.address'] = 'required|string';

		if($customer->type->name == 'person') {
			if(!$customer->id_number) $rules['customer.id_number'] = ['required', 'regex:/^\d{10}$/'];
		} else {
			if(!$customer->national_number) $rules['customer.national_number'] =  ['required', 'numeric', 'regex:/^\d{11}$/'];
			if(!$customer->economic_code) $rules['customer.economic_code'] =  ['required', 'numeric', 'regex:/^\d{12}$/'];
		}
		if(!$customer->mobile) $rules['customer.mobile'] = ['required', 'numeric', 'regex:/^09\d{9}$/'];
		if(!$customer->address) $rules['customer.address'] = 'required|string';
		
		$messages = [
			'user.id_number.regex' => 'شماره ملی شما ۱۰ رقم نمیباشد',
			'customer.mobile.regex' => 'شماره موبایل مشتری صحیح نمیباشد',
			'customer.id_number.regex' => 'شماره ملی مشتری ۱۰ رقم نمیباشد',
			'customer.national_number.regex' => 'شناسه ملی مشتری ۱۱ رقم نمیباشد',
			'customer.economic_code.regex' => 'کد اقتصادی مشتری ۱۲ رقم نمیباشد'
		];

		$attributes = [
			'user.id_number' => 'شماره ملی شما',
			'user.address' => 'آدرس شما',
			'customer.mobile' => 'شماره موبایل',
			'customer.address' => 'آدرس مشتری',
			'customer.id_number' => 'شماره ملی مشتری',
			'customer.national_number' => 'شناسه ملی',
			'customer.economic_code' => 'کد اقتصادی'
		];

		$validator = Validator::make($data, $rules, $messages);
		$validator->setAttributeNames($attributes);
		
		return $validator;
	}
}
