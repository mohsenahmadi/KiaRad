<?php

namespace App\Http\Controllers\Dashboard\Outcomes;

use App\Tenant\Models\Check;
use App\Tenant\Models\Turnover;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\CheckTurnoverRequest;

class ChecksController extends Controller
{
	public function index()
	{
		$transactions = Turnover::outcome()->ofType('check')->paginate(20);

		return view('dashboard.turnovers.index', compact('transactions'));
	}

	public function create()
	{
		return view('dashboard.turnovers.outcomes.check.create');
	}

	public function store(CheckTurnoverRequest $request)
	{
		$check = Check::create($request->check);
		
		$data = $request->except('check');		
		$data['transaction_id'] = $check->id;		
		$data['transaction_type'] = Check::class;		
		$data['type'] = 'CHECK';
		$data['income'] = false;

		$turnover = Turnover::create($data);

		if(!$turnover) {
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return redirect()->route('dashboard.outcomes.checks.edit', $turnover->id);
	}

	public function edit($turnover)
	{
		$turnover = Turnover::with('transaction')->find($turnover);
		$turnover->check = $turnover->transaction;

		return view('dashboard.turnovers.outcomes.check.edit', compact('turnover'));
	}

	public function update(CheckTurnoverRequest $request, $turnover)
	{
		$turnover = Turnover::with('transaction')->find($turnover);

		$check = $turnover->transaction;
		$update = $check->update($request->check);
		
		$data = $request->except('check');		
		
		$update = $turnover->update($data);

		if(!$update) {
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return back();
	}

	public function destroy($turnover)
	{
		$turnover = Turnover::find($turnover);

		$delete = $turnover->delete();

		if(!$delete) {
			return back()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return back();
	}
}
