<?php

namespace App\Http\Controllers\Dashboard\Outcomes;

use App\Tenant\Models\BankAccount;
use App\Tenant\Models\Turnover;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\BankTurnoverRequest;

class BanksController extends Controller
{
	public function index()
	{
		$transactions = Turnover::outcome()->ofType('bank')->paginate(20);

		return view('dashboard.turnovers.index', compact('transactions'));
	}

	public function create()
	{
		return view('dashboard.turnovers.outcomes.bank.create');
	}

	public function store(BankTurnoverRequest $request)
	{
		$data = $request->all();		
		$data['transaction_type'] = BankAccount::class;		
		$data['type'] = 'BANK_ACCOUNT';
		$data['income'] = false;

		$turnover = Turnover::create($data);

		if(!$turnover) {
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return redirect()->route('dashboard.outcomes.banks.edit', $turnover->id);
	}

	public function edit($turnover)
	{
		$turnover = Turnover::find($turnover);
		
		return view('dashboard.turnovers.outcomes.bank.edit', compact('turnover'));
	}

	public function update(BankTurnoverRequest $request, $turnover)
	{
		$turnover = Turnover::find($turnover);
		
		$data = $request->all();				

		$update = $turnover->update($data);

		if(!$update) {
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return back();
	}

	public function destroy($turnover)
	{
		$turnover = Turnover::find($turnover);

		$delete = $turnover->delete();

		if(!$delete) {
			return back()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return back();
	}
}
