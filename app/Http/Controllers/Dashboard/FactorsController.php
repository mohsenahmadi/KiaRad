<?php

namespace App\Http\Controllers\Dashboard;

use Validator;
use Route;

use App\Tenant\Models\Factor;
use App\Tenant\Models\Customer;
use App\Tenant\Models\AccountData;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Dashboard\FactorRequest;

class FactorsController extends Controller
{
	public function index()
	{
		if(Route::is('dashboard.buy.*')){
			$factors = Factor::with('customer')->where('type', 'BUY')->paginate(20);
		}else if(Route::is('dashboard.sell.*')){
			$factors = Factor::with('customer')->where('type', 'SELL')->paginate(20);
		} else {
			$factors = Factor::with('customer')->paginate(20);
		}

		return view('dashboard.factors.index', compact('factors'));
	}

	public function create()
	{
	    	return view('dashboard.factors.create');
	}

	public function store(FactorRequest $request)
	{
		$data = $request->except(['official', 'user', 'customer', 'goods']);
		$data['official'] = $request->get('official', false);

		$customer = Customer::find($data['customer_id']);

		if($data['official']) {
			$validator = $this->customValidate($request->only(['customer', 'user']), $customer);

			if ($validator->fails()) {
				return back()->withErrors($validator)->withInput();
			}

			$this->updateDealers($request->only(['customer', 'user']), $customer);
		}

		$factor = Factor::create($data);

		$factor = $factor->addGoods($request->goods);

		return redirect()->route('dashboard.factors.edit', $factor->id);
	}

	public function edit($factor)
	{
		$factor = Factor::with('goods', 'customer', 'customer.data')->find($factor);

		return view('dashboard.factors.edit', compact('factor'));
	}

	public function update(FactorRequest $request, Factor $factor)
	{
		$data = $request->except(['official', 'user', 'customer', 'goods']);
		$data['official'] = $request->get('official', false);

		$customer = Customer::find($data['customer_id']);

		if($request->get('official', false)) {
			$validator = $this->customValidate($request->only(['customer', 'user']), $customer);

			if ($validator->fails()) {
				return back()->withErrors($validator)->withInput();
			}

			$this->updateDealers($request->only(['customer', 'user']), $customer);
		}

		$update = $factor->update($data);

		$factor->goods()->detach();
		$factor = $factor->addGoods($request->goods);

		return back();
	}



	public function updateDealers($data, $customer)
	{

		if(isset($data['user'])) {
			foreach($data['user'] as $key => $value){
				AccountData::create(['key' => $key, 'value' => $value]);
			}
		}

		if(isset($data['customer'])) {
			$fields = ['address', 'phone', 'mobile', 'address'];
			$customer->update(array_only($data['customer'], $fields));
			foreach(array_except($data['customer'], $fields) as $key => $value){
				$customer->data()->create(['key' => $key, 'value' => $value]);
			}
		}
	}

	private function customValidate($data, $customer)
	{
		$rules = [];
		
		if(!AccountData::where('key', 'id_number')->exists()) 
			$rules['user.id_number'] = ['required', 'regex:/\d{10}/'];
		if(!AccountData::where('key', 'address')->exists())
			$rules['user.address'] = 'required|string';

		if($customer->type->name == 'person') {
			if(!$customer->id_number) $rules['customer.id_number'] = ['required', 'regex:/\d{10}/'];
		} else {
			if(!$customer->national_number) $rules['customer.national_number'] =  ['required', 'numeric', 'regex:/\d{11}'];
			if(!$customer->economic_code) $rules['customer.economic_code'] =  ['required', 'numeric', 'regex:/\d{12}'];
		}
		if(!$customer->mobile) $rules['customer.mobile'] = ['required', 'numeric', 'regex:/09\d{9}/'];
		if(!$customer->address) $rules['customer.address'] = 'required|string';
		
		return Validator::make($data, $rules);
	}
}
