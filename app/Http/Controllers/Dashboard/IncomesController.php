<?php

namespace App\Http\Controllers\Dashboard;

use App\Tenant\Models\Turnover;
use App\Tenant\Models\Chest;
use App\Tenant\Models\BankAccount;
use App\Tenant\Models\Customer;
use App\Tenant\Models\Bank;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IncomesController extends Controller
{
	public function chestCreate()
	{
		$chests = Chest::all();
		$customers = Customer::all();
		return view('dashboard.incomes.chest.create', compact('chests', 'customers'));
	}

	public function bankCreate()
	{
		$bank_accounts = BankAccount::all();
		$customers = Customer::all();
		return view('dashboard.incomes.bank.create', compact('bank_accounts', 'customers'));
	}

	public function checkCreate()
	{
		$banks = Bank::all();
		$customers = Customer::all();
		return view('dashboard.incomes.check.create', compact('banks', 'customers'));
	}
}
