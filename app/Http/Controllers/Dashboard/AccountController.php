<?php

namespace App\Http\Controllers\Dashboard;

use Carbon\Carbon;

use Zarinpal\Laravel\Facade\Zarinpal;

use App\Tenant\Models\Payment;
use App\Tenant\Models\Subscription;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{
	public function plans()
	{
		return view('dashboard.payments.prices');
	}

	public function subscribe(Request $request)
	{
		$payment = Payment::create(['amount' => 200000]);		

        $response = Zarinpal::request(
            route('dashboard.account.subscription.verify', $payment->id),
            $payment->amount,
            'پرداخت کیاراد'
        );
        
        if(isset($response['Authority'])) {
            $authority = $response['Authority'];
            return Zarinpal::redirect($authority);
        }

        return 'Error, Status: '.$response['error'];
	}

	public function subscriptionVerify(Request $request, Payment $payment)
	{
        if(is_null($payment->status) || empty($payment->status)){        	
            $response = Zarinpal::verify(
	        	$request->Status,    // $_GET['Status']
	        	$payment->amount,
	        	$request->Authority // $_GET['Authority']
	        );
            if(isset($response) && ($response['Status'] === 'verified_before' || $response['Status'] === 'success')) {
                $payment['transaction_code'] = $response['RefID'];
                $payment['status'] = 100;
                // $payment['message'] = $response['Message'];
                $payment->save();

                if(!$payment->factor_credit) {
                    $last = Subscription::orderByDesc('expired_at')->firstOrNew(['expired_at' => Carbon::now()]);
                    $subscription = $payment->subscription()->create(['expired_at' => $last->expired_at->addYear(1)]);    
                    $credit = $payment->factor_credit()->create(['buy_factor' => 200, 'sell_factor' => 200]);
                }
            }else{
                // Log::alert('Zarinpal Error message: '.($response['status'] ?? print_r($response)).(json_encode($payment)));
                // dump($response);

                $payment['status'] = $response['Status'];
                // $payment['message'] = $response['Message'];
                $payment->save();
            }
        }

        return redirect()->route('dashboard.account.subscription.result', [$payment['status'] === 100 ? 'success' : 'fail', 'payment_id' => $payment->id]); 
	}

	public function subscriptionResult($status, Payment $payment)
	{
		return view('dashboard.account.subscription.payment.result', compact('payment'));
	}	
}
