<?php

namespace App\Http\Controllers\Dashboard;

use App\Tenant\Models\Warehouse;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Dashboard\WarehouseRequest;

class WarehousesController extends Controller
{
	public function index()
	{
		$warehouses = Warehouse::paginate(20);

		return view('dashboard.warehouses.index', compact('warehouses'));
	}

	public function goods(Warehouse $warehouse)
	{
		$parent = null;
		$goods = $warehouse->goods()->whereIsLeaf()->paginate(10);

		return view('dashboard.goods.index', compact('goods', 'parent'));
	}

	public function create()
	{
		return view('dashboard.warehouses.create');
	}

	public function store(WarehouseRequest $request)
	{
		$data = $request->all();
		
		$warehouse = Warehouse::create($data);

		if(!$warehouse){
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return redirect()->route('dashboard.warehouses.edit', $warehouse->id); 
	}

	public function edit(Warehouse $warehouse)
	{
		return view('dashboard.warehouses.edit', compact('warehouse'));
	}

	public function update(WarehouseRequest $request, Warehouse $warehouse)
	{
		$data = $request->all();
		
		$update = $warehouse->update($data);

		if(!$update){
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return back(); 
	}

	public function destroy(Warehouse $warehouse)
	{
		$delete = $warehouse->delete();

		if(!$delete){
			return back()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return redirect()->route('dashboard.warehouses.index');
	}
}
