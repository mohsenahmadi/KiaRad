<?php

namespace App\Http\Controllers\Dashboard;

use App\Tenant\Models\Customer;
use App\Tenant\Models\Type;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Dashboard\CustomerRequest;

class CustomersController extends Controller
{
	public function index(Request $request)
	{
		if($request->has('type')) {
			$customers = Type::whereName($request->type)->firstOrFail()->customers();
		} else {
			$customers = new Customer;
		}

		if($request->has('q')) {
			$customers = $customers->where('name', 'like', '%'. $request->q .'%')
							->orWhere('code', 'like', '%'. $request->q .'%');
		}

		$customers = $customers->with('type')->paginate(20);

		return view('dashboard.customers.index', compact('customers'));
	}

	public function show(Request $request, $customer)
	{
		$customer = Customer::with('data', 'type')->find($customer);
		
		if($request->ajax()) {
			return response()->json($customer, 200);
		}
	}

	public function search(Request $request)
	{
		$customers = Customer::where(function($query) use ($request) {
				$query->where('code', 'like', '%'.$request->q.'%')
							->orWhere('name', 'like', '%'.$request->q.'%');
				});

		if($request->ajax()) {
			$customers = $customers->with('data', 'type')->take(10)->get();
			return response()->json($customers, 200);
		}
	}

	public function create()
	{
		return view('dashboard.customers.create');
	}

	public function store(CustomerRequest $request)
	{
		$data = $request->except('data');

		$customer = Customer::create($data);

		if(!$customer) {
			return back()->withIput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		$customer->addData($request->data);

		return redirect()->route('dashboard.customers.create');
		// return redirect()->route('dashboard.customers.edit', $customer->id);
	}

	public function edit($customer)
	{
		$customer = Customer::with('data')->find($customer);

		return view('dashboard.customers.edit', compact('customer'));
	}

	public function update(CustomerRequest $request, Customer $customer)
	{
		$data = $request->except('data', 'code');

		$update = $customer->update($data);

		if(!$update) {
			return back()->withIput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		$customer->data()->delete();
		$customer->addData($request->data);

		return back();
	}

	public function destroy(Customer $customer)
	{
		$delete = $customer->delete();

		if(!$delete){
			return back()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return redirect()->route('dashboard.customers.index');
	}
}
