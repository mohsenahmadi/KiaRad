<?php

namespace App\Http\Controllers\Dashboard;

use App\Tenant\Models\BankAccount;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Dashboard\BankAccountRequest;

class BankAccountsController extends Controller
{
	public function index()
	{
		$bank_accounts = BankAccount::all();

		return view('dashboard.bank_accounts.index', compact('bank_accounts'));
	}

	public function create()
	{
		return view('dashboard.bank_accounts.create');
	}

	public function store(BankAccountRequest $request)
	{
		$data = $request->all();

		$bank_account = BankAccount::create($data);

		if(!$bank_account) {
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return redirect()->route('dashboard.bank_accounts.edit', $bank_account->id);
	}

	public function edit(BankAccount $bank_account)
	{
		return view('dashboard.bank_accounts.edit', compact('bank_account'));
	}

	public function update(BankAccountRequest $request, BankAccount $bank_account)
	{
		$data = $request->all();

		$update = $bank_account->update($data);

		if(!$update) {
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return back();	
	}

	public function destroy(BankAccount $bank_account)
	{
		$delete = $bank_account->delete();

		if(!$delete) {
			return back()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return redirect()->route('dashboard.bank_accounts.index');
	}
}
