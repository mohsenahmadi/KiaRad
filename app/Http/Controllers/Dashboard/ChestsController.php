<?php

namespace App\Http\Controllers\Dashboard;

use App\Tenant\Models\Chest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Dashboard\ChestRequest;

class ChestsController extends Controller
{
	public function index()
	{
		$chests = Chest::all();

		return view('dashboard.chests.index', compact('chests'));
	}

	public function create()
	{
		return view('dashboard.chests.create');
	}

	public function store(ChestRequest $request)
	{
		$data = $request->all();

		$chest = Chest::create($data);

		if(!$chest) {
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return redirect()->route('dashboard.chests.edit', $chest->id);
	}

	public function edit(Chest $chest)
	{
		return view('dashboard.chests.edit', compact('chest'));
	}

	public function update(ChestRequest $request, Chest $chest)
	{
		$data = $request->all();

		$update = $chest->update($data);

		if(!$update) {
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return back();		
	}

	public function destroy(Chest $chest)
	{
		$delete = $chest->delete();

		if(!$delete) {
			return back()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return redirect()->route('dashboard.chests.index');	
	}
}
