<?php

namespace App\Http\Controllers\Dashboard;

use App\Tenant\Models\Good;
use App\Tenant\Models\Warehouse;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Dashboard\GoodRequest;

class GoodsController extends Controller
{
	public function index(Request $request, $good = null)
	{
		$parent = null;

		if($request->has('warehouse')) {
			$goods = Good::whereIsLeaf();
			
			$goods = $goods->whereHas('warehouse', function($query) use ($request) {
				$query->where('warehouses.id', $request->warehouse);
			});
		}else{			
			if($good){
				$parent = Good::find($good);
				$goods = $parent->children();
			}else{
				$goods = Good::whereNull('parent_id');
			}
		}

		$like_filters = ['code', 'name'];
		foreach($like_filters as $filter) {
			if($request->has($filter) && !empty($request->get($filter))) {
				$q = $request->get($filter);
				$goods = $goods->where($filter, 'like', '%'.$q.'%');
			}		
		}

		$goods = $goods->paginate(20);

		return view('dashboard.goods.index', compact('goods', 'parent'));
	}

	public function show(Request $request, $good)
	{
		$good = Good::find($good);
		
		if($request->ajax()) {
			return response()->json($good, 200);
		}
	}

	public function trades(Good $good)
	{
		$sell_factors = $good->sell_factors()->orderBy('issued_at')->orderBy('created_at')->get();
		$buy_factors = $good->buy_factors()->orderBy('issued_at')->orderBy('created_at')->get();
		$factors = $buy_factors->concat($sell_factors)->sortBy('issued_at')->sortBy('created_at');

		return view('dashboard.goods.trades', compact('good', 'factors'));
	}

	public function search(Request $request)
	{
		$goods = Good::where(function($query) use ($request) {
				$query->where('code', 'like', '%'.$request->q.'%')
							->orWhere('name', 'like', '%'.$request->q.'%');
				});

		if($request->ajax()) {			
			$goods = $goods->where('is_cat', $request->get('is_cat', false))->take(10)->get();
			return response()->json($goods, 200);
		}
	}

	public function create()
	{
		return view('dashboard.goods.create');
	}

	public function store(GoodRequest $request)
	{
		$data = $request->except('warehouse_id', 'parent_name');

		if(!isset($data['parent_id']) || !$data['parent_id']) {
			unset($data['parent_id']);
		}
		if(!isset($data['unit']) || empty($data['unit'])) {
			$data['unit'] = 'عدد';
		}

		if(!$request->has('is_cat') || !$request->is_cat) {
			$warehouse = Warehouse::find($request->warehouse_id);

			$good = $warehouse->goods()->create($data);
		} else {
			$good = Good::create($data);
		}

		if(!$good){
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return redirect()->route('dashboard.goods.create'); 
		// return redirect()->route('dashboard.goods.edit', $good->id); 
	}

	public function edit(Good $good)
	{
		return view('dashboard.goods.edit', compact('good'));
	}

	public function update(GoodRequest $request, Good $good)
	{
		$data = $request->except('warehouse_id', 'code', 'parent_name');

		$good->warehouse()->detach();		
		if($request->has('is_cat') && $request->is_cat) {
			$good->warehouse()->attach($request->warehouse_id);
		}

		$update = $good->update($data);

		if(!$update){
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return back(); 
	}

	public function destroy(Good $good)
	{
		$delete = $good->delete();

		if(!$delete){
			return back()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return redirect()->route('dashboard.goods.index');
	}
}
