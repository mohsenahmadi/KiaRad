<?php

namespace App\Http\Controllers\Dashboard\Incomes;

use App\Tenant\Models\Check;
use App\Tenant\Models\Turnover;
use App\Tenant\Models\Customer;
use App\Tenant\Models\BankAccount;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\BankTurnoverRequest;
use App\Http\Requests\Dashboard\CheckTurnoverRequest;

class ChecksController extends Controller
{
	public function index()
	{
		$transactions = Turnover::income()->ofType('check')->paginate(20);

		return view('dashboard.turnovers.index', compact('transactions'));
	}

	public function report(Request $request)
	{
		$transactions = Turnover::income()->ofType('check');

		$transactions = $transactions->filter($request);		
		if($request->has('number') && $request->number) {
			$transactions = $transactions->whereHas('checks', function($query) use ($request) {
				$query->where('number', $request->number);
			});
		}
		if($request->has('begin_due') && !empty($request->begin_due)) {
			$transactions = $transactions->whereHas('checks', function($query) use ($request) {
				$query->where('due_at', '>=', $request->begin_due);
			});
			// dd('here');
		}
		if($request->has('end_due') && !empty($request->end_due)) {
			$transactions = $transactions->whereHas('checks', function($query) use ($request) {
				$query->where('due_at', '<=', $request->end_due);
			});
			// dd('here');
		}

		$transactions = $transactions->paginate(100);

		
		return view('dashboard.turnovers.incomes.check.report', compact('transactions'));
	}

	public function manage(Request $request)
	{
		$transactions = Turnover::with('transaction', 'customer')->income()->ofType('check')
						->join('customers', 'customers.id', '=', 'turnovers.customer_id')
						->join('checks', function ($join) {
							$join->on('checks.id', '=', 'turnovers.transaction_id')
								->where('turnovers.transaction_type', '=', \App\Tenant\Models\Check::class);
						})->select(['turnovers.*', 'customers.name', 'checks.due_at']);

		switch ($request->get('order_by')) {
			case 'due':
				$transactions = $transactions->orderBy('checks.due_at', $request->get('order', 'desc'));
				break;
			case 'customer':
				$transactions = $transactions->orderBy('customers.name', $request->get('order', 'desc'));
				break;
			default:
				$transactions = $transactions->orderBy('turnovers.turnedover_at', $request->get('order', 'desc'));
				break;
		}

		$transactions = $transactions->paginate(20);

		return view('dashboard.turnovers.incomes.check.manage', compact('transactions'));	
	}

	public function spend($turnover)
	{
		$turnover = Turnover::with('transaction')->find($turnover);

		return view('dashboard.turnovers.incomes.check.spend', compact('turnover'));		
	}

	public function spendStore(Request $request, Turnover $turnover)
	{
		$check = $turnover->transaction;

		$data = $request->except('check');		
		$data['transaction_id'] = $turnover->transaction->id;		
		$data['transaction_type'] = Check::class;		
		$data['type'] = 'CHECK';
		$data['income'] = false;

		$turnover = Turnover::create($data);

		if(!$turnover) {
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		$check->transfered_id = $turnover->customer_id;
		$check->transfered_type = Customer::class;
		$check->save();

		return redirect()->route('dashboard.incomes.checks.manage');	
	}

	public function pass($turnover)
	{
		$turnover = Turnover::with('transaction')->find($turnover);

		return view('dashboard.turnovers.incomes.check.pass', compact('turnover'));
	}

	public function passStore(Request $request, Turnover $turnover)
	{
		$check = $turnover->transaction;

		$data = $request->all();				
		$data['transaction_type'] = BankAccount::class;		
		$data['customer_id'] = $turnover->customer_id;
		$data['type'] = 'BANK_ACCOUNT';
		$data['income'] = false;

		$turnover = Turnover::create($data);

		if(!$turnover) {
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		$check->transfered_id = $turnover->transaction_id;
		$check->transfered_type = BankAccount::class;
		$check->save();

		return redirect()->route('dashboard.incomes.checks.manage');
	}

	public function create()
	{
		return view('dashboard.turnovers.incomes.check.create');
	}

	public function store(CheckTurnoverRequest $request)
	{
		$check = Check::create($request->check);
		
		$data = $request->except('check');
		$data['transaction_id'] = $check->id;		
		$data['transaction_type'] = Check::class;		
		$data['type'] = 'CHECK';

		$turnover = Turnover::create($data);

		if(!$turnover) {
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return redirect()->route('dashboard.incomes.checks.edit', $turnover->id);
	}

	public function edit($turnover)
	{
		$turnover = Turnover::with('transaction')->find($turnover);
		$turnover->check = $turnover->transaction;

		return view('dashboard.turnovers.incomes.check.edit', compact('turnover'));
	}

	public function update(CheckTurnoverRequest $request, $turnover)
	{
		$turnover = Turnover::find($turnover);
		$check = $turnover->checks;

		$check->update($request->check);
		
		$data = $request->except('check');
		$data['transaction_type'] = Check::class;		
		
		$update = $turnover->update($data);

		if(!$update) {
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return back();
	}

	public function destroy($turnover)
	{
		$turnover = Turnover::find($turnover);

		$delete = $turnover->delete();

		if(!$delete) {
			return back()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return back();
	}
}
