<?php

namespace App\Http\Controllers\Dashboard\Incomes;

use App\Tenant\Models\Chest;
use App\Tenant\Models\Turnover;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\ChestTurnoverRequest;

class ChestsController extends Controller
{
	public function index()
	{
		$transactions = Turnover::income()->ofType('chest')->paginate(20);

		return view('dashboard.turnovers.index', compact('transactions'));
	}

	public function report(Request $request)
	{
		$transactions = Turnover::income()->ofType('chest');

		$transactions = $transactions->filter($request);		
		if($request->has('chest') && $request->chest) {
			$transactions = $transactions->where('transaction_id', $request->chest);
		}

		$transactions = $transactions->paginate(100);
		
		return view('dashboard.turnovers.incomes.chest.report', compact('transactions'));
	}

	public function create()
	{
		return view('dashboard.turnovers.incomes.chest.create');
	}

	public function store(ChestTurnoverRequest $request)
	{
		$data = $request->all();	
		$data['transaction_type'] = Chest::class;		
		$data['type'] = 'CASH';

		$turnover = Turnover::create($data);

		if(!$turnover) {
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return redirect()->route('dashboard.incomes.chests.edit', $turnover->id);
	}

	public function edit($turnover)
	{
		$turnover = Turnover::with('transaction')->find($turnover);

		return view('dashboard.turnovers.incomes.chest.edit', compact('turnover'));
	}

	public function update(ChestTurnoverRequest $request, $turnover)
	{
		$turnover = Turnover::find($turnover);
		
		$data = $request->all();
		$data['transaction_type'] = Chest::class;

		$update = $turnover->update($data);

		if(!$update) {
			return back()->withInput()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return back();
	}

	public function destroy($turnover)
	{
		$turnover = Turnover::find($turnover);

		$delete = $turnover->delete();

		if(!$delete) {
			return back()
				->with(['message' => ['type' => 'error', 'text' => 'مشکلی پیش آمده مجدد تلاش کنید.']]);
		}

		return back();
	}
}
