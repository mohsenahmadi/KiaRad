<?php

namespace App\Http\Controllers\Dashboard;

use App\Tenant\Models\Check;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChecksController extends Controller
{
	public function create()
	{
		return view('dashboard.checks.create');
	}
}
