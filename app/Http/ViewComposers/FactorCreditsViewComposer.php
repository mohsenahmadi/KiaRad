<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Tenant\Models\BuyFactor;
use App\Tenant\Models\FactorCredit;
use App\Tenant\Models\SellFactor;

class FactorCreditsViewComposer
{
	public function compose(View $view)
	{		
		$view->with('factor_credits', ['buy' => FactorCredit::sum('buy_factor') - BuyFactor::count(), 
										'sell' => FactorCredit::sum('sell_factor') - SellFactor::count()]);
	}
}