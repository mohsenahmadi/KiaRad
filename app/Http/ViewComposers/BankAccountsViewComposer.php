<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Tenant\Models\BankAccount;

class BankAccountsViewComposer
{
	protected $bank_accounts;

	public function __construct()
	{
		$this->bank_accounts = BankAccount::all();
	}

	public function compose(View $view)
	{
		$view->with('bank_accounts', $this->bank_accounts);
	}
}