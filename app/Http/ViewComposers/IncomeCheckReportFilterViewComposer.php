<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Tenant\Models\Check;

class IncomeCheckReportFilterViewComposer
{
	public function compose(View $view)
	{
		$filters = [
            'min' => Check::min('due_at'),
            'max' => Check::max('due_at')
        ];

		$view->with('check_filters', $filters);
	}
}