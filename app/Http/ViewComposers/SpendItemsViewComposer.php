<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Tenant\Models\SpendItem;

class SpendItemsViewComposer
{
	protected $spend_items;

	public function __construct()
	{
		$this->spend_items = SpendItem::all();
	}

	public function compose(View $view)
	{
		$view->with('spend_items', $this->spend_items);
	}
}