<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Tenant\Models\Good;

class GoodsViewComposer
{
	protected $goods;

	public function __construct()
	{
		$this->goods = Good::all();
	}

	public function compose(View $view)
	{
		$view->with('goods', $this->goods);
	}
}