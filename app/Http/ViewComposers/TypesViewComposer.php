<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Tenant\Models\Type;

class TypesViewComposer
{
	protected $types;

	public function __construct()
	{
		$this->types = Type::all();
	}

	public function compose(View $view)
	{
		$view->with('types', $this->types);
	}
}