<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Tenant\Models\SpendItem;

class SpendItemNumberViewComposer
{
	public function compose(View $view)
	{
		$code = SpendItem::max('code') + 1;
		$view->with('spend_item_code', $code);
	}
}