<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Tenant\Models\Chest;

class ChestsViewComposer
{
	protected $chests;

	public function __construct()
	{
		$this->chests = Chest::all();
	}

	public function compose(View $view)
	{
		$view->with('chests', $this->chests);
	}
}