<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Tenant\Models\BuyFactor as Factor;

class BuyFactorNumberViewComposer
{
	protected $factor_numbers;

	public function __construct()
	{
		$max = Factor::max('number');
		$min = Factor::min('number');		
		$max = $max < 0 ? 0 : $max;
		$min = $min > 0 ? 0 : $min;

		$this->factor_numbers['person'] =  $max + 1;
		$this->factor_numbers['company'] = abs($min) + 1;
	}

	public function compose(View $view)
	{
		$view->with('factor_numbers', $this->factor_numbers);
	}
}