<?php

namespace App\Http\ViewComposers;

use Carbon\Carbon;

use Illuminate\View\View;
use App\Tenant\Models\Subscription;

class SubscriptionViewComposer
{	

	public function compose(View $view)
	{		
		$view->with('subscription', Subscription::where('expired_at', '>', Carbon::now())->first());
	}
}