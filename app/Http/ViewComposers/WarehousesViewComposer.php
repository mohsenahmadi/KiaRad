<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Tenant\Models\Warehouse;

class WarehousesViewComposer
{
	protected $warehouses;

	public function __construct()
	{
		$this->warehouses = Warehouse::all();
	}

	public function compose(View $view)
	{
		$view->with('warehouses', $this->warehouses);
	}
}