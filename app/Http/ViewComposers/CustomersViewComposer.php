<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Tenant\Models\Customer;

class CustomersViewComposer
{
	protected $customers;

	public function __construct()
	{
		$this->customers = Customer::with('data', 'type')->get();
	}

	public function compose(View $view)
	{
		$view->with('customers', $this->customers);
	}
}