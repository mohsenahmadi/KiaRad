<?php

namespace App\Http\ViewComposers;

use Carbon\Carbon;

use Illuminate\View\View;
use App\Tenant\Models\Subscription;

class SubscriptionStatusViewComposer
{	

	public function compose(View $view)
	{		
		$view->with('subscription_status', Subscription::where('expired_at', '>', Carbon::now())->exists());
	}
}