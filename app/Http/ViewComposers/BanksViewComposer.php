<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Tenant\Models\Bank;

class BanksViewComposer
{
	protected $banks;

	public function __construct()
	{
		$this->banks = Bank::all();
	}

	public function compose(View $view)
	{
		$view->with('banks', $this->banks);
	}
}