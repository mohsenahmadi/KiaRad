<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Tenant\Models\Turnover;

class IncomeReportFilterViewComposer
{
	public function compose(View $view)
	{
		$filters = [
            'min' => Turnover::min('turnedover_at'),
            'max' => Turnover::max('turnedover_at')
        ];

		$view->with('filters', $filters);
	}
}