<?php

namespace App\Shared\Traits;

use Request;

use Spatie\Permission\Traits\HasRoles as SpatieHasRoles;

use Illuminate\Database\Eloquent\Relations\MorphToMany;

trait HasRoles
{
    use SpatieHasRoles {
        roles as parentRoles;
    }

    /**
     * A model may have multiple roles.
     */
    public function roles(): MorphToMany
    {
        $type = Request::route('account') ? 'tenant' : 'system';
        
        return $this->morphToMany(
            config('permission.models.'.$type.'.role'),
            'model',
            config('permission.table_names.model_has_roles'),
            'model_id',
            'role_id'
        );
    }
}