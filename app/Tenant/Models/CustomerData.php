<?php

namespace App\Tenant\Models;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class CustomerData extends Model
{
	use UsesTenantConnection;
	
	protected $fillable = ['key', 'value'];

	public function customer()
	{
		return $this->belongsTo(Customer::class);
	}
}
