<?php

namespace App\Tenant\Models;

use DB;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kalnoy\Nestedset\NodeTrait;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class Good extends Model
{
	use UsesTenantConnection, SoftDeletes, NodeTrait;

	protected $fillable = ['parent_id', 'warehouse_id', 'code', 'name', 'suggestion_buy_price', 'suggestion_sell_price', 'unit', 'service', 'tax', 'desctiption', 'is_cat'];
	protected $appends = ['stock'];

	public function warehouse()
	{
		return $this->belongsToMany(Warehouse::class, 'warehouse_goods');
	}

	public function buy_factors()
	{
		return $this->morphedByMany(BuyFactor::class, 'factor', 'factor_goods');
	}

	public function sell_factors()
	{
		return $this->morphedByMany(SellFactor::class, 'factor', 'factor_goods');
	}

	public function getStockAttribute()
	{
		$buy = DB::connection('tenant')->table('factor_goods')->where('factor_type', BuyFactor::class)->sum('quantity');
		$sell = DB::connection('tenant')->table('factor_goods')->where('factor_type', SellFactor::class)->sum('quantity');
		
		return $buy - $sell;
	}
}
