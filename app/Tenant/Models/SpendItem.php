<?php

namespace App\Tenant\Models;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class SpendItem extends Model
{
	use UsesTenantConnection;
	
	protected $fillable = ['code', 'name'];

	public function expenses()
	{
		return $this->hasMany(Expense::class, 'spend_id');
	}
}
