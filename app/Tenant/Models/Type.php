<?php

namespace App\Tenant\Models;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class Type extends Model
{
	use UsesTenantConnection;
	
	protected $fillable = ['name', 'label', 'description'];

	public function customers()
	{
		return $this->hasMany(Customer::class);
	}
}
