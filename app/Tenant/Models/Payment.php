<?php

namespace App\Tenant\Models;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class Payment extends Model
{
	use UsesTenantConnection;
	
	protected $fillable = ['amount', 'transaction_code', 'status', 'message', 'plan'];
	
	public function factor_credit()
	{
		return $this->hasOne(FactorCredit::class);
	}

	public function subscription()
	{
		return $this->hasOne(Subscription::class);
	}
}
