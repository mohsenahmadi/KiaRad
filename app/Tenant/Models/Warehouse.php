<?php

namespace App\Tenant\Models;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class Warehouse extends Model
{
	use UsesTenantConnection;
	
	protected $fillable = ['name', 'description', 'type'];

	public function goods()
	{
		return $this->belongsToMany(Good::class, 'warehouse_goods');
	}
}
