<?php

namespace App\Tenant\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class Customer extends Model
{
	use UsesTenantConnection, SoftDeletes;

	protected $fillable = ['type_id', 'code', 'name', 'state_code', 'phone', 'mobile', 'address'];

	public function type()
	{
		return $this->belongsTo(Type::class);
	}

	public function data()
	{
		return $this->hasMany(CustomerData::class);
	}

	public function addData($data)
	{
		$type = $this->type->name;
		if($type == 'person'){
			$fields = ['id_number'];
		}else if($type == 'company'){
			$fields = ['economic_code', 'national_number'];
		}
		foreach(array_only($data, $fields) as $key => $value){
			if(empty($key) || empty($value)) continue;

			$this->data()->create(['key' => $key, 'value' => $value]);
		}

		return $this;
	}

	public function __get($name)
	{
		$data = $this->data()->where('key', $name)->first();
		if($data) {
			return $data->value;
		}

		return parent::__get($name);
	}
}
