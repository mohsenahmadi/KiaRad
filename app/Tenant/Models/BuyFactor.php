<?php

namespace App\Tenant\Models;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class BuyFactor extends Model
{
	use UsesTenantConnection;
	
	protected $fillable = ['customer_id', 'number', 'description', 'price', 'discount', 'tax', 'total', 'pre_factor', 'official', 'issued_at'];	

	public function customer()
	{
		return $this->belongsTo(Customer::class);
	}

	public function goods()
	{
		return $this->morphToMany(Good::class, 'factor', 'factor_goods')
				->withPivot('price', 'quantity', 'discount', 'tax', 'total')
				->withTimestamps()->orderBy('created_at');
	}
	
	public function getNumberAttribute($number)
	{
		return abs($number);
	}

	public function addGoods($data)
	{
		$totals = ['price' => 0, 'discount' => 0, 'tax' => 0, 'total' => 0];
		foreach($data as $good){
			$data = array_only($good, ['price', 'quantity', 'discount', 'tax']);
			$price = ($data['price'] * $data['quantity']) - $data['discount'];
			$tax = $price * (($data['tax'] ?? 0) / 100);
			$data['total'] = $price + $tax;

			$totals['price'] += $price;
			$totals['discount'] += $data['discount'];
			$totals['tax'] += $tax;
			$totals['total'] += $data['total'];

			$this->goods()->attach($good['id'], $data);
		}

		return $totals;
	}
}
