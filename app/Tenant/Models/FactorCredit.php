<?php

namespace App\Tenant\Models;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class FactorCredit extends Model
{
	use UsesTenantConnection;
	
	protected $fillable = ['buy_factor', 'sell_factor'];
	
}
