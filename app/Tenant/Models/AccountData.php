<?php

namespace App\Tenant\Models;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class AccountData extends Model
{
	use UsesTenantConnection;

	protected $fillable = ['key', 'value'];
}
