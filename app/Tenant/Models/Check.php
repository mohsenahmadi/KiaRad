<?php

namespace App\Tenant\Models;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class Check extends Model
{
	use UsesTenantConnection;
	
	protected $fillable = ['bank_id', 'number', 'due_at'];
	protected $dates = ['due_at'];

	public function turnovers()
	{
		return $this->morphMany(Turnover::class, 'transaction');
	}

	public function transfered()
	{
		return $this->morphTo();
	}
}
