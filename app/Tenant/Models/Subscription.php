<?php

namespace App\Tenant\Models;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class Subscription extends Model
{
	use UsesTenantConnection;
	
	protected $fillable = ['expired_at'];
	protected $dates = ['expired_at'];
}
