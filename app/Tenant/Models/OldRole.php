<?php

namespace App\Tenant\Models;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Spatie\Permission\Models\Role as BaseRole;

class OldRole extends BaseRole
{
	use UsesTenantConnection;
}
