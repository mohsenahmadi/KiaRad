<?php

namespace App\Tenant\Models;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class Factor extends Model
{
	use UsesTenantConnection;
	
	protected $fillable = ['customer_id', 'number', 'description', 'pre_factor', 'type', 'official', 'issued_at'];
	
	protected static function boot()
	{
		parent::boot();

		static::saving(function ($model) {
			if(!$model->number) {
				$model->number = Factor::max('number') + 1;
			}
		});
	}

	public function customer()
	{
		return $this->belongsTo(Customer::class);
	}

	public function goods()
	{
		return $this->morphToMany(Good::class, 'factor', 'factor_goods')->withPivot('price', 'quantity', 'discount', 'tax');	
	}

	public function addGoods($data)
	{
		$goods = [];
		foreach($data as $good){
			$this->goods()->attach($good['id'], array_except($good, ['id', 'name', 'code']));			
		}
		

		return $this;
	}
}
