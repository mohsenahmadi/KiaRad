<?php

namespace App\Tenant\Models;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class Bank extends Model
{
	use UsesTenantConnection;
	protected $fillable = ['logo', 'name', 'description'];

	public function accounts()
	{
		return $this->hasMany(BankAccount::class);
	}
}
