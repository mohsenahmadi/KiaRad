<?php

namespace App\Tenant\Models;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class Turnover extends Model
{
	use UsesTenantConnection;
	
	protected $fillable = ['customer_id', 'transaction_id', 'transaction_type', 'amount', 'description', 'type', 'turnedover_at', 'income'];
	protected $dates = ['turnedover_at'];

	public function transaction()
	{
		return $this->morphTo();
	}

	public function checks()
	{		
		if($this->transaction_type == Check::class) {
			return $this->belongsTo(Check::class, 'transaction_id');
		}

		return null;
	}

	public function chests()
	{
		if($this->transaction_type == Chest::class) {
			return $this->belongsTo(Chest::class, 'transaction_id');
		}

		return null;
	}

	public function banks()
	{
		if($this->transaction_type == BankAccount::class) {
			return $this->belongsTo(BankAccount::class, 'transaction_id');
		}

		return null;
	}

	public function customer()
	{
		return $this->belongsTo(Customer::class);
	}

	public function scopeOfType($query, $type)
    {
    	$types = ['bank' => 'BANK_ACCOUNT', 'check' => 'CHECK', 'chest' => 'CASH'];

        return $query->where('type', $types[$type]);
    }

    public function scopeIncome($query)
    {    	
        return $query->where('income', true);
    }

    public function scopeOutcome($query)
    {    	
        return $query->where('income', false);
    }

    public function scopeFilter($query, $request)
    {
    	if($request->has('begin') && !empty($request->begin)) {
			$query = $query->where('turnedover_at', '>=', $request->begin);
		}
		if($request->has('end') && !empty($request->end)) {
			$query = $query->where('turnedover_at', '<=', $request->end);
		}
		if($request->has('amount') && !empty($request->amount)) {
			$query = $query->where('amount', '=', $request->amount);
		}
		if($request->has('description') && !empty($request->description)) {
			$query = $query->where('description', 'like', '%'.$request->description.'%');
		}
		if($request->has('customer') && $request->customer) {
			$query = $query->where('customer_id', '=', $request->customer);
		}

		return $query;
    }
}
