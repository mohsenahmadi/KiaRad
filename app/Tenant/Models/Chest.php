<?php

namespace App\Tenant\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class Chest extends Model
{
	use UsesTenantConnection, SoftDeletes;

	protected $fillable = ['name', 'description'];
	protected $appends = ['type'];

	public function turnovers()
	{
		return $this->morphMany(Turnover::class, 'transaction');
	}

	public function getTypeAttribute()
	{
		return 'صندوق';
	}
}
