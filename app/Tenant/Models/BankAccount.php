<?php

namespace App\Tenant\Models;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class BankAccount extends Model
{
	use UsesTenantConnection;

	protected $fillable = ['bank_id', 'account_number', 'IBAN', 'card_number'];
	protected $appends = ['type'];

	public function bank()
	{
		return $this->belongsTo(Bank::class);
	}

	public function turnovers()
	{
		return $this->morphMany(Turnover::class, 'transaction');
	}
	
	public function getTypeAttribute()
	{
		return 'حساب بانک';
	}
}
