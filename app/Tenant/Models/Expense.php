<?php

namespace App\Tenant\Models;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class Expense extends Model
{
	use UsesTenantConnection;

	protected $fillable = ['spend_id', 'source_type', 'source_id', 'amount', 'expended_at', 'description'];

	public function source()
	{
		return $this->morphTo();
	}

	public function spend()
	{
		return $this->belongsTo(SpendItem::class, 'spend_id');
	}
}
