<?php

namespace App\Tenant\Models;

use App\Shared\Models\User as Shared;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class User extends Shared
{
	use UsesTenantConnection;

	protected $fillable = [
		'first_name', 'last_name', 'username', 'email', 'mobile', 'id_number', 'password',
	];

	protected $hidden = ['password', 'remember_token'];

	protected $append = ['sure_name'];

	public function role()
	{
		return $this->belongsTo(Role::class);
	}

	public function getSureNameAttribute()
	{
		if($this->first_name && $this->last_name) {
			return $this->first_name.' '.$this->last_name; 
		}
		return $this->username; 
	}

	public function __get($name)
	{
		$data = AccountData::where('key', $name)->first();
		if($data) {
			return $data->value;
		}

		return parent::__get($name);
	}
}
