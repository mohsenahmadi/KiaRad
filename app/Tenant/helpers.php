<?php

if (! function_exists('tenant_route')) {
    /**
     * Generate the URL to a named tenant_route.
     *
     * @param  array|string  $name
     * @param  mixed  $parameters
     * @param  bool  $absolute
     * @return string
     */
    function tenant_route($name, $parameters = [], $absolute = true)
    {
        return app('url')->route($name, array_merge(['account' => 'alireza'], $parameters), $absolute);
    }
}