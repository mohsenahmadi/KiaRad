<?php

namespace App\Console\Commands;

use Hyn\Tenancy\Database\Connection;
use Hyn\Tenancy\Models\Customer;

use Illuminate\Console\Command;

class TenantArtisan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenant:tinker {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set tenant database for tenant';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');

        $customer = Customer::where('name', $name)->first();
        if(!$customer) {
            $this->error("A tenant with name '{$name}' is not exists.");

            return;
        }

        $website = $customer->websites->first();
        if(!$website) {
            $this->error("A tenant with name '{$name}' doesn't have any website.");

            return;
        }

        app()->make(Connection::class)->set($website);

        config(['database.default' => 'tenant']);

        $this->call('tinker');
    }
}
