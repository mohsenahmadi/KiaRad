<?php

namespace App\Jobs;

use Exception;
use Log;

use App\Notifications\TenantCreated;
use App\System\Models\Tenant;

use Hyn\Tenancy\Database\Connection;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CreateTenant implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 1;

    protected $name, $email, $password, $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($name, $email, $user)
    {
        $this->name = $name;
        $this->email = $email;
        $this->password = $user['password'] ?? null;
        $this->user = array_except($user, 'password');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $tenant = Tenant::createFrom($this->name, $this->email, $this->password, $this->user);

        // invite admin
        $tenant->admin->notify(new TenantCreated($tenant->hostname));
    }

    public function failed(Exception $exception)
    {
        Log::error('Create Tanat Error: ', ['exception' => $exception]);
        // dd($exception);
    }

    /**
     * Get the tags that should be assigned to the job.
     *
     * @return array
     */
    public function tags()
    {
        return ['tenancy', 'tenant::'.$this->name];
    }
}
