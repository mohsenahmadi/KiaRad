<section class="footer">

  <div class="footer-elements">
    <div class="row">
      <div class="col-sm-12">
        <div class="row no-gutter">

          <!-- FOOTER TOP ELEMENT -->
          <div class="col-sm-2">
            <div class="footer-element">
              <span class="typcn typcn-watch"></span>
              <p>هفت روز هفته<span>۲۴ ساعته</span></p>
            </div>
          </div>
          <!-- FOOTER TOP ELEMENT -->

          <!-- FOOTER TOP ELEMENT -->
          <div class="col-sm-4">
            <div class="footer-element">
              <span class="typcn typcn-location-outline"></span>
              <p>دفتر مرکزی تهران، میدان هفت حوض، خیابان جانبازان شرقی<span>جنب بانک ملی، پلاک ۴۸۴، طبقه اول</span></p>
            </div>
          </div>
          <!-- FOOTER TOP ELEMENT -->

          <!-- FOOTER TOP ELEMENT -->
          <div class="col-sm-3">
            <div class="footer-element">
              <span class="typcn typcn-mail"></span>
              <p>ارتباط با پست الکترونیکی<span><a href="#">info@kiarad.tax</a></span></p>
            </div>
          </div>
          <!-- FOOTER TOP ELEMENT -->

          <!-- FOOTER TOP ELEMENT -->
          <div class="col-sm-3">
            <div class="footer-element">
              <span class="typcn typcn-phone-outline"></span>
              <p>شماره تماس:<span style="direction: ltr">۰۲۱ ۷۷ ۹۴ ۹۴ ۰۷</span></p>
            </div>
          </div>
          <!-- FOOTER TOP ELEMENT -->
        </div>
      </div>
    </div>
  </div>

  <div class="footer-widgets">
    <div class="row">

      <!-- FOOTER WIDGET -->
      <div class="col-sm-3">
        <div class="footer-widget">
          <h5>کیاراد</h5>
          <p style="text-align: justify;">موسسه حسابدار کیاراد، با پشتوانه سال‌ها تجربه مدیران خود در حوزه حسابداری توانسته است با بهره‌گیری از تیمی مجرب و متخصص در حوزه فناوری اطلاعات به عنوان یکی از پیشگامان حسابداری آنلاین در ایران فعالیت خود را به صورت مستمر و پیگیر شروع کرده و راهی را برای استفاده همه اقشار جامعه از این سرویس‌ها فراهم آورد.</p>
          {{-- <ul class="social-links">
            <li><a href="index.html#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
            <li><a href="index.html#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="index.html#" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
            <li><a href="index.html#" title="Pinterest"><i class="fa fa-pinterest-p"></i></a></li>
          </ul> --}}
        </div>
      </div>
      <!-- END FOOTER WIDGET -->

      {{-- <!-- FOOTER WIDGET -->
      <div class="col-sm-3">
        <div class="footer-widget">
          <h5>RECENT NEWS</h5>
          <ul class="posts">
            <li>
              <a href="index.html">Front-end processes with effective convergence</a>
              <span>3 days ago</span>
            </li>
            <li>
              <a href="index.html">User friendly intellectual capital</a>
              <span>5 days ago</span>
            </li>
            <li><a href="index.html">Innovate open-source infrastructures</a>
              <span>1 month ago</span>
            </li>
          </ul>
        </div>
      </div>
      <!-- END FOOTER WIDGET --> --}}

      <!-- FOOTER WIDGET -->
      <div class="col-sm-3">
        <div class="footer-widget">
          <h5></h5>
          <ul class="posts">
            <li><a href="{{ route('about') }}">درباره کیاراد</a></li>
            {{-- <li><a href="{{ route('privacy') }}">حریم شخصی</a></li> --}}
            <li><a href="{{ route('complains') }}">فرم ثبت شکایات</a></li>
            {{-- <li><a href="#">قوانین و مقررات</a></li> --}}
            <li><a href="{{ route('faq') }}">سوالات پر تکرار</a></li>
            <li><a href="{{ route('contact') }}">تماس با ما</a></li>
          </ul>
        </div>
      </div>
      <!-- END FOOTER WIDGET -->

      <div class="col-sm-3"></div>

      <!-- FOOTER WIDGET -->
      <div class="col-sm-3">
        <div class="footer-widget">
          <h5>دریافت آخرین تغییرات</h5>
          <p>برای دریافت آخرین اخبار و به‌روز رسانی‌های ما ایمیل خود را وارد کنید.</p>
          <div id="mc_embed_signup">
            <form class="form-inline validate material" action="#" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank" novalidate>
              <input id="mce-EMAIL" type="email" name="EMAIL" placeholder="آدرس ایمیل" required>
              <div style="position: absolute; right: -5000px;">
                <input type="text" name="b_b5638e105dac814ad84960d90_9345afa0aa" tabindex="-1" value="">
              </div>
              <input type="submit" value="عضویت" name="subscribe" id="mc-embedded-subscribe" class="mtr-btn button-blue">
            </form>
          </div>

        </div>
      </div>
      <!-- END FOOTER WIDGET -->

    </div>
  </div>
</section>
