<div class="header header1">
  <!-- TOP BAR -->
  @include('partials.top-bar')
  <!-- END OF TOP BAR -->

  <!-- SEARCH FORM -->
  <div id="search">
    <button type="button" class="close">×</button>
    <form>
      <span>Type and Press enter</span>
      <input type="search" value="" placeholder="" />
    </form>
  </div>
  <!-- END OF SEARCH FORM -->

  <!-- LOGO & NAVIGATION -->
  @include('partials.navigation')
  <!-- END OF LOGO & NAVIGATION -->
</div>