<div class="topbar">
  <div class="row">
    <div class="col-sm-6">
      <ul class="phone">
        <li style="direction: ltr"><span class="typcn typcn-phone-outline"></span> ۰۲۱ ۷۷ ۹۴ ۹۴ ۰۷</li>
        {{-- <li><a href="#"><i class="fa fa-facebook"></i></a></li>
        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
        <li><a href="#"><i class="fa fa-linkedin"></i></a></li> --}}
      </ul>
    </div>
    <div class="col-sm-6">
      <ul class="toplinks">
        <li>هفت روز هفته - تمام ساعات شبانه روز</li>
        {{-- <li><a href="#">E-MAIL US</a></li> --}}
        {{-- <li><a href="#search"><i class="fa fa-search"></i></a></li> --}}
      </ul>
    </div>
  </div>
</div>
