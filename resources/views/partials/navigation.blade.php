<div class="header-navigation">
  <div class="row">
    <div class="col-sm-12">
      <div class="logonav">
        <div class="row no-gutter">
          <div class="col-sm-3">
            <div class="logo">
              <h3>کیاراد</h3>
            </div>
          </div>
          <div class="col-sm-9">
            <nav id="desktop-menu">
              <ul class="sf-menu" id="navigation">
                <li class="current"><a href="/">خانه</a> </li>
                <li><a href="{{ route('about') }}">درباره کیاراد</a> </li>
                <li><a href="{{ route('services') }}">خدمات کیاراد</a> </li>
                <li><a href="{{ route('faq') }}">سوال های پر تکرار</a> </li>
               <li><a href="{{ route('prices') }}">قیمت ها</a> </li>
              <li><a href="{{ route('contact') }}">تماس با ما</a></li>
              <li><a href="{{ route('login') }}">ورود</a></li>
              <li><a href="{{ route('register') }}">عضویت</a></li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
</div>
</div>