@extends('dashboard.layouts.dashboard')

@section('page_title', 'ایجاد صندوق جدید')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		@include('dashboard.partials.errors')
		
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  ایجاد صندوق جدید
				</div>
			</div>
			<div class="portlet-body form">
				<form action="{{ route('dashboard.chests.store') }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
				
					@php
						$data = empty(old()) ? new App\Tenant\Models\Chest : old();
					@endphp
					
					<div class="form-body">
						@include('dashboard.chests.form', ['data' => $data])
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection