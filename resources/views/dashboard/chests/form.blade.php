<div class="form-group">
	<label class="control-label col-md-3">نام صندوق:</label>
	<div class="col-md-9">
		<input class="form-control" type="text" name="name" placeholder="نام صندوق" value="{{ $data['name'] }}" required>		
	</div>
</div>

<div class="row">
	<div class="col-md-9 col-md-push-3">
		<button class="btn btn-success">ذخیره</button>
	</div>
</div>