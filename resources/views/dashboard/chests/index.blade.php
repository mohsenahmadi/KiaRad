@extends('dashboard.layouts.dashboard')

@section('page_title', ' لیست صندوقها')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  لیست صندوقها
				</div>
				<div class="actions">
					<a class="btn green" href="{{ route('dashboard.chests.create') }}">+</a>
				</div>
			</div>
			<div class="portlet-body form">
				<div class="table-scrollable">	
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>ردیف</th>
								<th>نام</th>
								<th>اصلاحات</th>
							</tr>
						</thead>
						<tbody>
							@foreach($chests as $index => $chest)
								<tr>
									<td> {{ $index + 1 }} </td>
									<td> {{ $chest->name }} </td>
									<td>
										<a class="btn btn-outline blue" href="{{ route('dashboard.chests.edit', $chest->id) }}">
											<i class="fa fa-pencil"></i>
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection