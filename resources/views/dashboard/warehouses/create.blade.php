@extends('dashboard.layouts.dashboard')

@section('page_title', ' ایجاد انبار جدید')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		@include('dashboard.partials.errors')
		
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  ایجاد انبار جدید
				</div>
			</div>
			<div class="portlet-body form">
				<form action="{{ route('dashboard.warehouses.store') }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
					
					@php
						$data = empty(old()) ? new App\Tenant\Models\Warehouse : old();
					@endphp
					
					<div class="form-body">
						@include('dashboard.warehouses.form', ['data' => $data])
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection