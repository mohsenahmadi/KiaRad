@extends('dashboard.layouts.dashboard')

@section('page_title', ' لیست انبارها')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  لیست انبارها
				</div>
				<div class="actions">
					<a class="btn green" href="{{ route('dashboard.warehouses.create') }}">+</a>
				</div>
			</div>
			<div class="portlet-body form">
				<div class="table-scrollable">	
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>ردیف</th>
								<th>نام انبار</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($warehouses as $index => $warehouse)
								<tr>
									<td> {{ $index + 1 }} </td>
									<td> {{ $warehouse->name }} </td>
									<td>
										<a class="btn dark" href="{{ route('dashboard.warehouses.goods', $warehouse->id) }}">
											<i class="fa fa-eye"></i>
										</a>
										<a class="btn btn-outline blue" href="{{ route('dashboard.warehouses.edit', $warehouse->id) }}">
											<i class="fa fa-pencil"></i>
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection