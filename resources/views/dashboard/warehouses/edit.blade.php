@extends('dashboard.layouts.dashboard')

@section('page_title', 'اصلاح اطلاعات انبار')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		@include('dashboard.partials.errors')
		
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  اصلاح اطلاعات انبار
				</div>
				<div class="actions">
					@include('dashboard.partials.buttons.delete', ['route' => 'dashboard.warehouses.destroy', 'id' => $warehouse->id])
				</div>
			</div>
			<div class="portlet-body form">
				<form action="{{ route('dashboard.warehouses.update', $warehouse->id) }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
					{{ method_field('PUT') }}
				
					@php
						$data = empty(old()) ? $warehouse : old();
					@endphp
					
					<div class="form-body">
						@include('dashboard.warehouses.form', ['data' => $data])
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('page_scripts')
<script src="{{ asset('metronic/js/helpers.js') }}"></script>
@endsection