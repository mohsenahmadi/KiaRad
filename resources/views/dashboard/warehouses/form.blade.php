<div class="form-group">
	<label class="control-label col-md-3">نام:</label>
	<div class="col-md-9">
		<input class="form-control" type="text" name="name" value="{{ $data['name'] }}" placeholder="نام" autocomplete="off">
	</div>
</div>
<div class="form-group">
	<label class="col-md-3 control-label">نوع:</label>
	<div class="mt-radio-inline">
		<label class="mt-radio">
			<input type="radio" name="type" value="GOOD"  {{ !$data['type'] || $data['type'] == 'GOOD' ? 'checked': '' }} required> کالا
			<span></span>
		</label>
		<label class="mt-radio">
			<input type="radio" name="type" value="SERVICE" {{ $data['type'] == 'SERVICE' ? 'checked': '' }} required> خدمات
			<span></span>
		</label>
	</div>
</div>
<div class="row">
	<div class="col-md-9 col-md-push-3">		
		<button class="btn btn-success">ذخیره</button>
	</div>
</div>
