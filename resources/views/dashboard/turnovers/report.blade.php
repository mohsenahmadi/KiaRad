@extends('dashboard.layouts.dashboard')

@section('page_title', ' گزارش گردش حساب')

@section('page_content')
<div class="row">
	<div class="col-md-12">
		@php
			$type = Route::is('dashboard.incomes.*') ? 'incomes' : 'outcomes';
			
			$route = 'chests';
			if(Route::is('dashboard.'.$type.'.banks.*')){
				$route = 'banks';
			} else if(Route::is('dashboard.'.$type.'.checks.*')){
				$route = 'checks';
			}
		@endphp
		<ul class="nav nav-tabs nav-justified">
			<li class="{{ ($route == 'banks') ? 'active' : '' }}">	
				<a href="{{ route('dashboard.'.$type.'.banks.report') }}"> واریزی به بانک </a>
			</li>
			<li class="{{ ($route == 'chests') ? 'active' : '' }}">	
				<a href="{{ route('dashboard.'.$type.'.chests.report') }}"> دریافت وجه نقد </a>
			</li>
			<li class="{{ ($route == 'checks') ? 'active' : '' }}">	
				<a href="{{ route('dashboard.'.$type.'.checks.report') }}"> دریافت چک </a>
			</li>
		</ul>
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  گزارش گردش حساب
				</div>
			</div>
			<div class="portlet-body form">
				@yield('table')
			</div>
			<div class="portlet-footer">
				{{ $transactions->links() }}
			</div>
		</div>
	</div>
</div>
@endsection

@push('page_scripts')
<script type="text/javascript">	
	$(document).ready( function () {
	  var beginFilter = $('input#begin-filter').pDatepicker({    
	    initialValueType: 'gregorian',
	    format: 'L',
	    altField: 'input[name=begin]',
	    altFieldFormatter: function(unix) {
	      return new persianDate(unix).toCalendar('gregorian').toLocale('en').format('YYYY-MM-DD')
	    },
	    maxDate: new persianDate().valueOf(),
	    onSelect: function(date) {
	      endFilter.options.minDate = date;
	    }
	  });
	  var endFilter = $('input#end-filter').pDatepicker({    
	    initialValueType: 'gregorian',
	    format: 'L',
	    altField: 'input[name=end]',
	    altFieldFormatter: function(unix) {
	      return new persianDate(unix).toCalendar('gregorian').toLocale('en').format('YYYY-MM-DD')
	    },
	    maxDate: new persianDate().valueOf(),
	    onSelect: function(date) {
	      beginFilter.options.maxDate = date;
	    }
	  });

	  $('.table-filter button').on('click', function(){
			var clone = $('.table-filter').clone();
			$('.table-filter select').each(function(i) {
				var select = this;
				$(clone).find("select").eq(i).val($(select).val());
			});
			$('form#filter').html(clone).submit();
		});
	});
</script>
@endpush