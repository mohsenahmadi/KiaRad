@extends('dashboard.layouts.dashboard')

@section('page_title', 'ایجاد دریافتی به بانک')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		@include('dashboard.partials.errors')
		
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  ایجاد دریافتی به بانک
				</div>
			</div>
			<div class="portlet-body form">
				<form action="{{ route('dashboard.incomes.banks.store') }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
				
					@php
						$data = empty(old()) ? new App\Tenant\Models\Turnover : old();
					@endphp
					
					<div class="form-body">
						@include('dashboard.turnovers.incomes.bank.form', ['data' => $data])
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection