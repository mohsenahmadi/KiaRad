@extends('dashboard.layouts.dashboard')

@section('page_title', 'اصلاح دریافتی به صندوق')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		@include('dashboard.partials.errors')
		
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  اصلاح دریافتی به صندوق
				</div>
			</div>
			<div class="portlet-body form">
				<form action="{{ route('dashboard.incomes.chests.update', $turnover->id) }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
					{{ method_field('PUT') }}
				
					@php
						$data = empty(old()) ? $turnover : old();
					@endphp
					
					<div class="form-body">
						@include('dashboard.turnovers.incomes.chest.form', ['data' => $data])
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection