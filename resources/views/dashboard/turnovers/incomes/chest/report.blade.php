@extends('dashboard.turnovers.report')

@section('table')
<form id="filter" action="{{ route('dashboard.incomes.chests.report') }}" method="GET" style="display: none"></form>
<div class="table-scrollable">	
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>ردیف</th>
				<th width="150">تاریخ واریز</th>
				<th width="150">به صندوق</th>
				<th width="100">مبلغ</th>
				<th>بابت</th>
				<th>نام مشتری</th>
			</tr>
			<tr class="table-filter">
				<th>
					<button class="btn btn-primary">						
						<i class="fa fa-search"></i>
					</button>
				</th>
				<th>
					<div>
						<label>از:</label>
						<input type="text" id="begin-filter" value="{{ Request::get('begin', $filters['min']) }}" class="form-control" placeholder="شروع بازه" autocomplete="off" style="display: inline-block; max-width: 100px;">
						<input type="hidden" name="begin" value="{{ Request::get('begin', $filters['min']) }}">                
					</div>
					<div>
						<label>تا:</label>
						<input type="text" id="end-filter" value="{{ Request::get('end', $filters['max']) }}" class="form-control " placeholder="پایان بازه" autocomplete="off" style="display: inline-block; max-width: 100px;">
						<input type="hidden" name="end" value="{{ Request::get('end', $filters['max']) }}">
					</div>
				</th>
				<th>
					<select class="form-control" name="chest">
						<option value="0">همه صندوق‌ها</option>
						@foreach($chests as $chest)
							@if($chest->id == Request::get('chest'))
								<option value="{{ $chest->id }}" selected> {{ $chest->name }} </option>
							@else
								<option value="{{ $chest->id }}"> {{ $chest->name }} </option>
							@endif
						@endforeach
					</select>
				</th>
				<th>
					<input type="number" class="form-control" name="amount" value="{{ Request::get('amount') }}" >
				</th>
				<th><input class="form-control" name="description" value="{{ Request::get('description') }}"></th>
				<th>
					<select class="form-control" name="customer">
						<option value="0">همه مشتریها</option>							
						@foreach($customers as $customer)
							@if($customer->id == Request::get('customer'))
								<option value="{{ $customer->id }}" selected> {{ $customer->name }} </option>
							@else
								<option value="{{ $customer->id }}"> {{ $customer->name }} </option>
							@endif
						@endforeach
					</select>
				</th>
			</tr>			
		</thead>
		<tbody>
			@foreach($transactions as $index => $transaction)
			<tr>
				<td> {{ $index + 1 }} </td>
				<td> {{ jdate($transaction->turnedover_at)->format('%Y/%m/%d') }}</td>
				<td> {{ $transaction->transaction->name }} </td>
				<td> {{ number_format($transaction->amount) }} </td>
				<td> {{ $transaction->description }} </td>
				<td> {{ $transaction->customer->name }} </td>

			</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endsection