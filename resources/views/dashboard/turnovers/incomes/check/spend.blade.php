@extends('dashboard.layouts.dashboard')

@section('page_title', 'خرج چک')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		@include('dashboard.partials.errors')
		
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  خرج چک
				</div>
			</div>
			<div class="portlet-body form">
				<form action="{{ route('dashboard.incomes.checks.spend', $turnover) }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
					@method('PUT')
				
					@php
						if(empty(old())) {
							$data = $turnover;
							$data['description'] = 'خرج چک '.$turnover->transaction->number;
							$data['turnedover_at'] = null;							
						} else {
							$data = old();
						}
					
					@endphp
					
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3">مشتری:</label>
							<div class="col-md-9">
								<select class="form-control" name="customer_id">
									@foreach($customers as $customer)
									@if($customer->id == $data['customer_id'])
									<option value="{{ $customer->id }}" selected>
										@else
										<option value="{{ $customer->id }}">
											@endif
											{{ $customer->code }} - {{ $customer->name }}
										</option>
										@endforeach
									</select>
								</div>
							</div>							
								<div class="form-group">
									<label class="control-label col-md-3">تاریخ:</label>
									<div class="col-md-9">
										<input id="date-picker" type="text" class="form-control" value="{{ $data['turnedover_at'] }}" placeholder="تاریخ"/>
										<input type="hidden" name="turnedover_at" id="turnedover-at" value="{{ $data['turnedover_at'] }}">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">مبلغ:</label>
									<div class="col-md-9">
										<input class="form-control" value="{{ $data['amount'] }}" disabled>
										<input type="hidden" name="amount" value="{{ $data['amount'] }}">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">بابت:</label>
									<div class="col-md-9">
										<input class="form-control" type="text" name="description" placeholder="بابت" value="{{ $data['description'] }}">
									</div>
								</div>
								<div class="row">
									<div class="col-md-9 col-md-push-3">
										<button class="btn btn-success">ثبت</button>
									</div>
								</div>

								@section('page_scripts')
								<script type="text/javascript">
									$(document).ready(function() {
										$("#date-picker").pDatepicker({
											'format': 'dddd DD/MMMM/YYYY',
											'altField': '#turnedover-at',
											'altFieldFormatter': function(unix) {
												var date = new persianDate(unix);
												var gregorian = date.toCalendar('gregorian');
												var year = gregorian.year(),
												month = gregorian.month(),
												day = gregorian.date();

												return year+'-'+month+'-'+day;
											}
										});
									});
								</script>
								@endsection
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection