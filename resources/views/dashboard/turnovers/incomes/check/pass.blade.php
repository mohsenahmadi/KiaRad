@extends('dashboard.layouts.dashboard')

@section('page_title', 'وصول چک')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		@include('dashboard.partials.errors')
		
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  وصول چک
				</div>
			</div>
			<div class="portlet-body form">
				<form action="{{ route('dashboard.incomes.checks.pass', $turnover) }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
					@method('PUT')

					@php
					if(empty(old())) {
						$data = $turnover;
						$data['description'] = 'وصول چک '.$turnover->transaction->number;
						$data['turnedover_at'] = null;							
					} else {
						$data = old();
					}
					
					@endphp
					
					<div class="form-body">						
						<div class="form-group">
							<label class="control-label col-md-3">حساب بانک:</label>
							<div class="col-md-9">
								<select class="form-control" name="transaction_id">
									@foreach($bank_accounts as $bank_account)
										@if($bank_account->id == $data['transaction_id'])
										<option value="{{ $bank_account->id }}" selected>
										@else
										<option value="{{ $bank_account->id }}">
											@endif
											{{ $bank_account->account_number }}
										</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">تاریخ:</label>
							<div class="col-md-9">
								<input id="date-picker" type="text" class="form-control" value="{{ $data['turnedover_at'] }}" placeholder="تاریخ"/>
								<input type="hidden" name="turnedover_at" id="turnedover-at" value="{{ $data['turnedover_at'] }}">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">مبلغ:</label>
							<div class="col-md-9">
								<input class="form-control" type="number" value="{{ $data['amount'] }}" disabled>
								<input type="hidden" name="amount" value="{{ $data['amount'] }}">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">بابت:</label>
							<div class="col-md-9">
								<input class="form-control" type="text" name="description" placeholder="بابت" value="{{ $data['description'] }}">
							</div>
						</div>
						<div class="row">
							<div class="col-md-9 col-md-push-3">
								<button class="btn btn-success">ثبت</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('page_scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$("#date-picker").pDatepicker({
			'format': 'dddd DD/MMMM/YYYY',
			'altField': '#turnedover-at',
			'altFieldFormatter': function(unix) {
				var date = new persianDate(unix);
				var gregorian = date.toCalendar('gregorian');
				var year = gregorian.year(),
				month = gregorian.month(),
				day = gregorian.date();

				return year+'-'+month+'-'+day;
			}
		});
	});
</script>
@endsection