@extends('dashboard.layouts.dashboard')

@section('page_title', ' لیست گردش حساب')

@section('page_content')
<div class="row">
	<div class="col-md-10 col-md-push-1">	
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i> وصول و خرج چک
				</div>
			</div>
			<div class="portlet-body form">
				<div class="table-scrollable">	
					@php
						$order = ['asc' => 'desc', 'desc' => 'asc'][Request::get('order', 'asc')];
					@endphp
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>
									<a href="{{ route('dashboard.incomes.checks.manage', ['order_by' => 'due', 'order' => $order]) }}">
										تاریخ چک
									</a>
								<th>مبلغ</th>
								<th>
									<a href="{{ route('dashboard.incomes.checks.manage', ['order_by' => 'customer', 'order' => $order]) }}">
										مشتری
									</a>
								</th>
								<th>شماره چک</th>
								<th>									
									<a href="{{ route('dashboard.incomes.checks.manage', ['order_by' => 'create', 'order' => $order]) }}">
										تاریخ دریافت
									</a>
								</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($transactions as $index => $transaction)
								<tr>
									<td> {{ $index + 1 }} </td>
									<td> {{ jdate($transaction->transaction->due_at)->format('%Y/%m/%d') }}</td>
									<td> {{ $transaction->amount }} </td>
									<td> {{ $transaction->customer->name }} </td>
									<td> {{ $transaction->transaction->number }} </td>
									<td> {{ jdate($transaction->turnedover_at)->format('%Y/%m/%d') }}</td>
									<td>
										@if(!$transaction->transaction->transfered)										
											<a class="btn btn-outline blue" href="{{ route('dashboard.incomes.checks.spend', $transaction->id) }}">
												خرج چک
											</a>
											<a class="btn btn-outline green" href="{{ route('dashboard.incomes.checks.pass', $transaction->id) }}">
												وصول چک
											</a>
										@else
											@if($transaction->transaction->transfered_type == \App\Tenant\Models\Customer::class)
												خرج شد
											@else
												وصول شد
											@endif
										@endif
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection