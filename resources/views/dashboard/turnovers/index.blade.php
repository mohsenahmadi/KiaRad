@extends('dashboard.layouts.dashboard')

@section('page_title', ' لیست گردش حساب')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		@php
			$type = Route::is('dashboard.incomes.*') ? 'incomes' : 'outcomes';
			
			$route = 'chests';
			if(Route::is('dashboard.'.$type.'.banks.*')){
				$route = 'banks';
			} else if(Route::is('dashboard.'.$type.'.checks.*')){
				$route = 'checks';
			}
		@endphp
		<ul class="nav nav-tabs nav-justified">
			<li class="{{ ($route == 'banks') ? 'active' : '' }}">	
				<a href="{{ route('dashboard.'.$type.'.banks.index') }}"> واریزی به بانک </a>
			</li>
			<li class="{{ ($route == 'chests') ? 'active' : '' }}">	
				<a href="{{ route('dashboard.'.$type.'.chests.index') }}"> دریافت وجه نقد </a>
			</li>
			<li class="{{ ($route == 'checks') ? 'active' : '' }}">	
				<a href="{{ route('dashboard.'.$type.'.checks.index') }}"> دریافت چک </a>
			</li>
		</ul>
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  لیست گردش حساب
				</div>
				<div class="actions">
					<a class="btn green" href="{{ route('dashboard.'.$type.'.'.$route.'.create') }}">+</a>
				</div>
			</div>
			<div class="portlet-body form">
				<div class="table-scrollable">	
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>مبلغ</th>
								<th>مشتری</th>
								<th>تاریخ</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($transactions as $index => $transaction)
								<tr>
									<td> {{ $index + 1 }} </td>
									<td> {{ $transaction->amount }} </td>
									<td> {{ $transaction->customer->name }} </td>
									<td> {{ jdate($transaction->turnedover_at)->format('%Y/%m/%d') }}</td>
									<td>
										<a class="btn btn-outline blue" href="{{ route('dashboard.'.$type.'.'.$route.'.edit', $transaction->id) }}">
											<i class="fa fa-pencil"></i>
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection