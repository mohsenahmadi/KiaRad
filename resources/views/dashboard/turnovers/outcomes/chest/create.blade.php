@extends('dashboard.layouts.dashboard')

@section('page_title', 'ایجاد پرداختی از صندوق')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		@include('dashboard.partials.errors')
		
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  ایجاد پرداختی از صندوق
				</div>
			</div>
			<div class="portlet-body form">
				<form action="{{ route('dashboard.outcomes.chests.store') }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
				
					@php
						$data = empty(old()) ? new App\Tenant\Models\Turnover : old();
					@endphp
					
					<div class="form-body">
						@include('dashboard.turnovers.outcomes.chest.form', ['data' => $data])
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection