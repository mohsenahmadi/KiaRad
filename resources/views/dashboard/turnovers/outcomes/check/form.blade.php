@section('library_styles')
<link rel="stylesheet" href="{{ asset('assets/global/plugins/typeahead/typeahead.css') }}">
@endsection

<div class="form-group">
	<label class="control-label col-md-3">مشتری:</label>
	<div class="col-md-9">
		<input type="hidden" name="customer_id" value="{{ $data['customer_id'] }}">
		<input type="text" class="form-control" id="customer-typehead">
	</div>
</div>
{{-- <div class="form-group">
	<label class="control-label col-md-3">مشتری:</label>
	<div class="col-md-9">
		<select class="form-control" name="customer_id">
			@foreach($customers as $customer)
				@if($customer->id == $data['customer_id'])
					<option value="{{ $customer->id }}" selected>
				@else
					<option value="{{ $customer->id }}">
				@endif
					{{ $customer->code }} - {{ $customer->name }}
				</option>
			@endforeach
		</select>
	</div>
</div> --}}
<div class="form-group">
	<label class="control-label col-md-3">بانک:</label>
	<div class="col-md-9">
		<select class="form-control" name="check[bank_id]">
			@foreach($banks as $bank)
				@if($bank->id == $data['check']['bank_id'])
					<option value="{{ $bank->id }}" selected>
				@else
					<option value="{{ $bank->id }}">
				@endif
					{{ $bank->name }}
				</option>
			@endforeach
		</select>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">تاربخ دریافت:</label>
	<div class="col-md-9">
		<input id="date-picker" type="text" class="form-control" value="{{ $data['turnedover_at'] }}" placeholder="تاریخ"/>
		<input type="hidden" name="turnedover_at" id="turnedover-at" value="{{ $data['turnedover_at'] }}">
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">تاربخ چک:</label>
	<div class="col-md-9">
		<input id="check-date-picker" type="text" class="form-control" value="{{ $data['check']['due_at'] }}" placeholder="تاریخ"/>
		<input type="hidden" name="check[due_at]" id="due-at" value="{{ $data['check']['due_at'] }}">
	</div>	
</div>
<div class="form-group">
	<label class="control-label col-md-3">شماره چک:</label>
	<div class="col-md-9">
		<input class="form-control" type="number" name="check[number]" min="0" placeholder="شماره چک" value="{{ $data['check']['number'] }}" required>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">مبلغ:</label>
	<div class="col-md-9">
		<input class="form-control" type="number" name="amount" min="0" placeholder="مبلغ پرداختی" value="{{ $data['amount'] }}" required>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">بابت:</label>
	<div class="col-md-9">
		<input class="form-control" type="text" name="description" placeholder="بابت" value="{{ $data['description'] }}">
	</div>
</div>
<div class="row">
	<div class="col-md-9 col-md-push-3">
		<button class="btn btn-success">ثبت</button>
	</div>
</div>

@section('library_scripts')
<script src="{{ asset('js/bloodhound.min.js') }}"></script>
<script src="{{ asset('js/typeahead.bundle.min.js') }}"></script>
@endsection

@section('page_scripts')
<script type="text/javascript">
	var customersSearchUrl = "{{ route('dashboard.customers.search') }}";	
	var customerFetch = "{{ isset($data['customer_id']) && !empty($data['customer_id']) ? 
													route('dashboard.customers.show', $data['customer_id']) : null }}";

  $(document).ready(function() {
    $("#date-picker").pDatepicker({
    	'format': 'dddd DD/MMMM/YYYY',
    	'altField': '#turnedover-at',
    	'altFieldFormatter': function(unix) {
    		var date = new persianDate(unix);
    		var gregorian = date.toCalendar('gregorian');
    		var year = gregorian.year(),
    			month = gregorian.month(),
    			day = gregorian.date();

			return year+'-'+month+'-'+day;
    	}
    });

    $("#check-date-picker").pDatepicker({
    	'format': 'dddd DD/MMMM/YYYY',
    	'altField': '#due-at',
    	'altFieldFormatter': function(unix) {
    		var date = new persianDate(unix);
    		var gregorian = date.toCalendar('gregorian');
    		var year = gregorian.year(),
    			month = gregorian.month(),
    			day = gregorian.date();

			return year+'-'+month+'-'+day;
    	}
    });
  });
</script>
<script src="{{ asset('assets/partials/scripts/customers.js') }}"></script>
@endsection