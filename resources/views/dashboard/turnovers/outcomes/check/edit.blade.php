@extends('dashboard.layouts.dashboard')

@section('page_title', 'اصلاح پرداختی چک')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		@include('dashboard.partials.errors')
		
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  اصلاح پرداختی چک
				</div>
			</div>
			<div class="portlet-body form">
				<form action="{{ route('dashboard.outcomes.checks.update', $turnover->id) }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
				
					@php
						$data = empty(old()) ? $turnover : old();
					@endphp
					
					<div class="form-body">
						@include('dashboard.turnovers.outcomes.check.form', ['data' => $data])
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection