@push('page_styles')
<style>
/* Hide HTML5 Up and Down arrows. */
input[type="number"]::-webkit-outer-spin-button, input[type="number"]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}
 
input[type="number"] {
    -moz-appearance: textfield;
}
</style>
@endpush

<div class="table-scrollable" id="goods-table">	
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th> # </th>
				<th style="min-width: 120px"> کد کالا </th>
				<th style="min-width: 120px"> نام کالا </th>
				<th> تعداد </th>
				<th> فی </th>
				<th> واحد </th>
				<th> فی x تعداد </th>
				<th> تخفیف </th>
				<th> مالیات بر ارزش افزوده</th>
				<th> قیمت کل </th>
				<th> </th>
			</tr>
		</thead>
		<tbody data-repeater-list="goods">
			@forelse($data['goods'] as $index => $good)
				<tr data-repeater-item>
					<td> {{ $index + 1 }} </td>
					<td>
						<input class="good-id" type="hidden" name="goods[{{ $index }}][id]" value="{{ $good['id'] }}">
						<input class="good-code" type="hidden" name="goods[{{ $index }}][code]" value="{{ $good['code'] }}">
						<p class="form-control-static">{{ $good['code'] }}</p>
					</td>
					<td class="good-name">
						<input class="form-control" type="text" name="goods[{{ $index }}][name]" value="{{ $good['name'] }}" placeholder="نام کالا" required>
					</td>
					<td class="good-quantity">
						<input class="form-control" type="number" name="goods[{{ $index }}][quantity]" value="{{ $good['quantity'] }}" min="1" placeholder="تعداد">
					</td>
					<td class="good-price"> 
						<input class="form-control" type="text" value="{{ number_format($good['price']) }}" data-price="{{ $good['price'] }}" min="0" placeholder="قیمت">
						<input type="hidden" name="goods[{{ $index }}][price]" value="{{ $good['price'] }}" data-price="{{ $good['price'] }}" required>
					</td>
					<td class="good-unit">
						<input type="hidden" name="goods[{{ $index }}][unit]" value="{{ $good['unit'] }}">
						<p class="form-control-static">{{ $good['unit'] }}</p>
					</td>
					<td class="good-row-price">
						<p class="form-control-static"></p>
					</td>
					<td class="good-discount">
						<input class="form-control" type="number" name="goods[{{ $index }}][discount]" value="{{ $good['discount'] }}" min="0" placeholder="تخفیف">
					</td>
					<td class="good-tax">
						<input type="hidden" name="goods[{{ $index }}][tax]" value="{{ $good['tax'] ?? null }}">
						<p class="form-control-static"></p>
					</td>
					<td class="good-total">
						<p class="form-control-static"></p>
					</td>
					<td>
						<div class="btn-group">
							<a href="javascript:;" data-repeater-delete class="btn red btn-outline mt-repeater-delete"> 
								<i class="fa fa-trash"></i> 
							</a>
						</div>
					</td>
				</tr>
			@empty
				<tr data-repeater-item>
					<td> 1 </td>
					<td>
						<input class="good-id" type="hidden" name="goods[0][id]">
						<input class="good-code" type="hidden" name="goods[0][code]">						
						<p class="form-control-static"></p>
					</td>
					<td class="good-name">
						<input class="form-control" type="text" name="goods[0][name]" placeholder="نام کالا" required>
					</td>
					<td class="good-quantity">
						<input class="form-control" type="number" name="goods[0][quantity]" min="1" placeholder="تعداد">
					</td>
					<td class="good-price"> 
						<input class="form-control" type="text" min="0" placeholder="قیمت">
						<input type="hidden" name="goods[0][price]" required> 
					</td>
					<td class="good-unit">
						<input type="hidden" name="goods[0][unit]">
						<p class="form-control-static"></p>
					</td>
					<td class="good-row-price">
						<p class="form-control-static"></p>
					</td>
					<td class="good-discount">
						<input class="form-control" type="number" name="goods[0][discount]" min="0" placeholder="تخفیف">
					</td>
					<td class="good-tax">
						<input type="hidden" name="goods[0][tax]">
						<p class="form-control-static"></p>
					</td>
					<td class="good-total">
						<p class="form-control-static"></p>
					</td>
					<td>
						<div class="btn-group">
							<a href="javascript:;" data-repeater-delete class="btn red btn-outline mt-repeater-delete"> 
								<i class="fa fa-trash"></i> 
							</a>
						</div>
					</td>
				</tr>
			@endforelse
		</tbody>
		<tfoot>
			<tr style="border: 0">
				<th colspan="6" style="border: 0"></th>
				<th class="total-row-price"> 0 </th>
				<th class="total-discount"> 0 </th>
				<th class="total-tax"> 0 </th>
				<th class="total-price"> 0 </th>
				<th style="border: 0"> </th>
			</tr>
		</tfoot>
	</table>
	<a href="javascript:;" data-repeater-create class="btn btn-success mt-repeater-add">
		<i class="fa fa-plus"></i> افزودن
	</a>
</div>

@section('page_scripts')
@parent
<script src="{{ asset('assets/global/plugins/jquery-repeater/jquery.repeater.js') }}" type="text/javascript"></script>
<script>
	var goodsSearchUrl = "{{ route('dashboard.goods.search') }}";
	var factor_type = "{{ Route::is('dashboard.buy.*') ? 'BUY' : 'SELL' }}";
</script>
<script src="{{ asset('assets/partials/scripts/factors/goods.js') }}" type="text/javascript"></script>
@endsection