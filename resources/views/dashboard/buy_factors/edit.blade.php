@extends('dashboard.layouts.dashboard')

@section('page_title', 'اصلاح فاکتور خرید')

@section('library_styles')
<link rel="stylesheet" href="{{ asset('assets/global/plugins/typeahead/typeahead.css') }}">
@endsection

@section('page_content')
<div class="row">
	<div class="col-md-12">
		@include('dashboard.partials.errors')

		<form action="{{ route('dashboard.buy.update', $factor->id) }}" method="POST">
			{{ csrf_field() }}
			{{ method_field('PUT') }}
			
			@php
				$data = empty(old()) ? $factor : old();
				if(empty(old())) {
					foreach($data['goods'] as $index => $good) {
						$data['goods'][$index] = array_merge($good->toArray(), $good->pivot->toArray());
					}
					$data['user'] = ['id_number' => '', 'address' => ''];
					$data['customer'] = $factor->customer->data->pluck('value', 'key');
				}
			@endphp

			@include('dashboard.buy_factors.dealers')
			
			@include('dashboard.buy_factors.goods')
		
			<div class="row margin-top-20">
				<div class="col-md-12">
					@include('dashboard.sell_factors.description')
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-md-push-8">
					<div class="row">
						<div class="col-md-12">
							@include('dashboard.sell_factors.summery')
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="pull-right">
								<button class="btn btn-success">ثبت</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>	
@endsection

@section('library_scripts')
<script src="{{ asset('js/bloodhound.min.js') }}"></script>
<script src="{{ asset('js/typeahead.bundle.min.js') }}"></script>
@endsection