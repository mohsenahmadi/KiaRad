<div class="form-group">
	<label class="control-label col-md-3">مشتری:</label>
	<div class="col-md-9">
		<input type="hidden" name="customer_id" value="{{ $data['customer_id'] }}">
		<input type="text" class="form-control" id="customer-typehead">
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">موبایل:</label>
	<div class="col-md-9" id="customer-mobile"> </div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">آدرس:</label>
	<div class="col-md-9" id="customer-address"> </div>
</div>
<div  id="customer-data"></div>