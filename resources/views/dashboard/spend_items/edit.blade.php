@extends('dashboard.layouts.dashboard')

@section('page_title', 'اصلاح سرفصل هزینه')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		@include('dashboard.partials.errors')
		
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  اصلاح سرفصل هزینه
				</div>
				<div class="actions">
					@include('dashboard.partials.buttons.delete', ['route' => 'dashboard.spend_items.destroy', 'id' => $spend_item->id])
				</div>
			</div>
			<div class="portlet-body form">
				<form action="{{ route('dashboard.spend_items.update', $spend_item->id) }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
					{{ method_field('PUT') }}
	
					@php
						$data = empty(old()) ? $spend_item : old();
						$data['code'] = $spend_item->code;
						$data['disabled'] = true;
					@endphp
					
					<div class="form-body">
						@include('dashboard.spend_items.form', ['data' => $data])
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('page_scripts')
<script src="{{ asset('metronic/js/helpers.js') }}"></script>
@endsection
