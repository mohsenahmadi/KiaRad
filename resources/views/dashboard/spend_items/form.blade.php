<div class="form-group">
	<label class="control-label col-md-3">کد:</label>
	<div class="col-md-9">
		@if($data['disabled'])
		<input class="form-control" type="text" value="{{ $data['code'] }}" disabled>		
		@else
		<input class="form-control" type="text" name="code" placeholder="کد" value="{{ $data['code'] }}" required>		
		@endif
	</div>
</div>

<div class="form-group">
	<label class="control-label col-md-3">نام:</label>
	<div class="col-md-9">
		<input class="form-control" type="text" name="name" placeholder="نام" value="{{ $data['name'] }}" required>		
	</div>
</div>

<div class="row">
	<div class="col-md-9 col-md-push-3">
		<button class="btn btn-success">ذخیره</button>
	</div>
</div>