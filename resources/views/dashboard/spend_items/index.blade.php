@extends('dashboard.layouts.dashboard')

@section('page_title', ' سرفصل هزینه ها ')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  سرفصل هزینه ها 
				</div>
				<div class="actions">
					<a class="btn green" href="{{ route('dashboard.spend_items.create') }}">+</a>
				</div>
			</div>
			<div class="portlet-body form">
				<div class="table-scrollable">	
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>ردیف</th>
								<th>کد</th>
								<th>نام</th>
								<th>اصلاحات</th>
							</tr>
						</thead>
						<tbody>
							@foreach($spend_items as $index => $spend_item)
								<tr>
									<td> {{ $index + 1 }} </td>
									<td> {{ $spend_item->code }} </td>
									<td> {{ $spend_item->name }} </td>
									<td>
										@if(!$spend_item->default)
											<a class="btn btn-outline blue" href="{{ route('dashboard.spend_items.edit', $spend_item->id) }}">
												<i class="fa fa-pencil"></i>
											</a>
										@endif
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection