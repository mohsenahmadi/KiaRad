<div class="page-footer">
	<div class="page-footer-inner"> تمامی حقوق متعلق به کیاراد است &copy; ۱۳۹۷
		<a href="{{ config('app.url') }}" target="_blank">کیاراد!</a>
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>