<a class="btn btn-sm btn-danger" href="{{ route($route, $id) }}" onclick="deleteItem(event, 'delete-form-{{ $id }}')">حذف</a>
<form action="{{ route($route, $id) }}" method="POST" id="delete-form-{{$id }}">
	{{ csrf_field() }}
	{{ method_field('DELETE') }}
</form>