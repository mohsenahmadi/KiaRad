<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('dashboard.home') }}">خانه</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span></span>
        </li>
    </ul>
    <div class="page-toolbar">
        {{-- <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
            <i class="icon-calendar"></i>&nbsp;
            <span class="thin uppercase hidden-xs"></span>&nbsp;
            <i class="fa fa-angle-down"></i>
        </div> --}}
    </div>
</div>
@if(!$subscription_status)
    <div class="alert alert-danger">

        اشتراک حساب شما به پایان رسیده است. لطفا برای تمدید اشتراک خود بر روی کلید تمدید کلیک کنید.
        <a onclick="event.preventDefault(); document.getElementById('subscribe-form').submit();" class="btn btn-xs btn-danger pull-right">تمدید اشتراک</a>
    </div>

    <form action="{{ route('dashboard.account.subscribe') }}" id="subscribe-form" method="POST">
        @csrf    
    </form>
@endif