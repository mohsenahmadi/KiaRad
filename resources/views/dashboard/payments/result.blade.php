@extends('dashboard.layouts.dashboard')

@section('page_content')
<div class="row">
	<div class="col-md-6 col-md-push-3">			    
	    <div class="portlet light bordered">
	    	<div class="portlet-title">
	    		<div class="caption {{ $payment->status == 100 ? 'font-green-sharp' : 'font-red-mint' }}"> نتیجه افزایش فاکتور </div>
	    	</div>
	    	<div class="portlet-body form">
			    @if($payment->status == 100)
			    	<p class="alert alert-success">افزایش فاکتور با موفقیت انجام شد</p>
			    @else
			    	<p class="alert alert-danger">افزایش فاکتور با موفقیت انجام نشد.</p>			        
			    @endif
			</div>
		</div>
	</div>
</div>
@endsection