@extends('dashboard.layouts.dashboard')

@section('library_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/css/pricing-rtl.min.css') }}">
@endsection

@section('page_content')
<div class="pricing-content-2">
    <div class="pricing-table-container">
        <div class="row padding-fix">
            <div class="col-md-4 no-padding">
                <div class="price-column-container border-top border-left">
                    <div class="price-table-head price-1">
                        <h4 class="uppercase no-margin">پایه</h4>
                    </div>
                    <div class="price-table-pricing">
                        <h3>{{ number_format(1000000) }}<span>ریال</span></h3>
                        <p class="uppercase">یکساله</p>
                    </div>
                    <div class="price-table-content">
                        <div class="row no-margin">
                            امکان صدور 100 فاکتور خرید
                        </div>
                        <div class="row no-margin">
                            امکان صدور 100 فاکتور خرید
                        </div>
                        <div class="row no-margin">
                            ثبت هزینه نامحدود
                        </div>
                        <div class="row no-margin">
                            عملیات بانکی نامحدود
                        </div>
                        <div class="row no-margin">
                            عملیات انبار نامحدود
                        </div>
                    </div>
                    <div class="price-table-footer">
                        <form action="{{ route('dashboard.payments.pay') }}" method="POST">
                            @csrf
                            <input type="hidden" name="plan" value="base">
                            <button class="btn grey uppercase bold">خرید</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4 no-padding">
                <div class="price-column-container border-top  featured-price border-top">
                    <div class="price-table-head price-1">
                        <h4 class="uppercase no-margin">عادی</h4>
                    </div>
                    <div class="price-table-pricing">
                        <h3>{{ number_format(2000000) }}<span>ریال</span></h3>
                        <p class="uppercase">یکساله</p>
                    </div>
                    <div class="price-table-content">
                        <div class="row no-margin">
                            امکان صدور 200 فاکتور خرید
                        </div>
                        <div class="row no-margin">
                            امکان صدور 200 فاکتور خرید
                        </div>
                        <div class="row no-margin">
                            ثبت هزینه نامحدود
                        </div>
                        <div class="row no-margin">
                            عملیات بانکی نامحدود
                        </div>
                        <div class="row no-margin">
                            عملیات انبار نامحدود
                        </div>
                    </div>
                    <div class="price-table-footer">
                        <form action="{{ route('dashboard.payments.pay') }}" method="POST">
                            @csrf
                            <input type="hidden" name="plan" value="normal">
                            <button class="btn grey uppercase bold">خرید</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4 no-padding">
                <div class="price-column-container border-top border-right">
                    <div class="price-table-head price-1">
                        <h4 class="uppercase no-margin">ویژه</h4>
                    </div>
                    <div class="price-table-pricing">
                        <h3>{{ number_format(3000000) }}<span>ریال</span></h3>
                        <p class="uppercase">یکساله</p>
                    </div>
                    <div class="price-table-content">
                        <div class="row no-margin">
                            امکان صدور نامحدود فاکتور خرید
                        </div>
                        <div class="row no-margin">
                            امکان صدور نامحدود فاکتور خرید
                        </div>
                        <div class="row no-margin">
                            ثبت هزینه نامحدود
                        </div>
                        <div class="row no-margin">
                            عملیات بانکی نامحدود
                        </div>
                        <div class="row no-margin">
                            عملیات انبار نامحدود
                        </div>
                    </div>
                    <div class="price-table-footer">
                        <form action="{{ route('dashboard.payments.pay') }}" method="POST">
                            @csrf
                            <input type="hidden" name="plan" value="special">
                            <button class="btn grey uppercase bold">خرید</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection