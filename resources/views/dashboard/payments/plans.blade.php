@extends('dashboard.layouts.dashboard')

@section('library_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/css/pricing-rtl.min.css') }}">
@endsection

@section('page_content')
<div class="row">
    @php
        $plans = [['name' => 'pack1', 'price' => 50000, 'factor' => 50], 
                    ['name' => 'pack2', 'price' => 75000, 'factor' => 100], 
                    ['name' => 'pack3', 'price' => 100000, 'factor' => 200]]
    @endphp
    @foreach($plans as $plan)
    <div class="col-md-4">
        <div class="portlet green-sharp box">
            <div class="portlet-title">
                <div class="caption"> بسته فاکتور {{ $plan['factor'] }}تای </div>                
            </div>
            <div class="portlet-body">             
                <form action="{{ route('dashboard.payments.pay') }}" method="POST">
                    @csrf
                    <input type="hidden" name="plan" value="{{ $plan['name'] }}">
                    <button class="btn btn-lg btn-block green">
                        <i class="fa fa-plus"></i> خرید                        
                    </button>
                </form>   
            </div>
        </div>
    </div>
    @endforeach    
</div>
@endsection