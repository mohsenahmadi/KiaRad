@extends('dashboard.layouts.dashboard')

@section('page_title', 'ایجاد دریافتی به صندوق')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		@include('dashboard.partials.errors')
		
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  ایجاد دریافتی به صندوق
				</div>
			</div>
			<div class="portlet-body form">
				<form action="#" method="POST" class="form-horizontal">
					{{ csrf_field() }}
				
					@php
						$data = empty(old()) ? new App\Tenant\Models\Turnover : old();
					@endphp
					
					<div class="form-body">
						@include('dashboard.checks.form', ['data' => $data])
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection