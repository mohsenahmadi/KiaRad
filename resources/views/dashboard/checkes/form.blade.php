<div class="form-group">
	<label class="control-label col-md-3">حساب بانک:</label>
	<div class="col-md-9">
		<input type="hidden" name="transaction_type" value="{{ App\Tenant\Models\BankAccount::class }}">
		<select class="form-control" name="transaction_id">
			@foreach($bank_accounts as $bank_account)
				<option value="{{ $bank_account->id }}">{{ $bank_account->account_number }}</option>
			@endforeach
		</select>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">مبلغ:</label>
	<div class="col-md-9">
		<input class="form-control" type="number" name="amount" min="0" required>
	</div>
</div>
<div class="row">
	<div class="col-md-9 col-md-push-3">
		<button class="btn btn-success">ثبت</button>
	</div>
</div>