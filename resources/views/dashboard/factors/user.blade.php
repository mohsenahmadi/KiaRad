@php $user = Auth::user(); @endphp
<div class="form-group">
	<label class="control-label col-md-3">نام:</label>
	<div class="col-md-9">
		<p class="form-control-static">
			{{ $user->sure_name }}
		</p>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">شماره ملی:</label>
	<div class="col-md-9">
		@if(!$user->id_number)
		<input type="text" class="form-control" name="user[id_number]" value="{{ $data['user']['id_number'] }}" placeholder="شماره ملی">
		@else
		<p class="form-control-static"> {{ $user->id_number }} </p>
		@endif
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">آدرس:</label>
	<div class="col-md-9">
		@if(!$user->address)
		<textarea class="form-control" name="user[address]" placeholder="آدرس">{{ $data['user']['address'] }}</textarea>
		@else
		<p class="form-control-static"> {{ $user->address }} </p>
		@endif
	</div>
</div>