<div class="table-scrollable" id="goods-table">	
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th> # </th>
				<th style="min-width: 120px"> کد کالا </th>
				<th style="min-width: 120px"> نام کالا </th>
				<th> فی </th>
				<th> تعداد </th>
				<th> واحد </th>
				<th> فی x تعداد </th>
				<th> تخفیف </th>
				<th> مالیات بر ارزش افزوده</th>
				<th> قیمت کل </th>
				<th> </th>
			</tr>
		</thead>
		<tbody data-repeater-list="goods">
			@forelse($data['goods'] as $index => $good)
				<tr data-repeater-item>
					<td> {{ $index + 1 }} </td>
					<td>
						<select class="form-control" name="goods[{{ $index }}][id]" required>
							<option>انتخاب کالا</option>
							@foreach($goods as $each)
								@if($each->id == $good['id'])
									<option value="{{ $each->id }}" selected> 
								@else
									<option value="{{ $each->id }}"> 
								@endif
									{{ $each->name }}
								</option>
							@endforeach
						</select> 
					</td>
					<td class="good-name">
						<p class="form-control-static"></p>
					</td>
					<td class="good-price"> 
						<input class="form-control" type="number" name="goods[{{ $index }}][price]" value="{{ $good['price'] }}" min="0" placeholder="قیمت" required>
					</td>
					<td class="good-quantity">
						<input class="form-control" type="number" name="goods[{{ $index }}][quantity]" value="{{ $good['quantity'] }}" min="1" placeholder="تعداد">
					</td>
					<td class="good-unit">
						<p class="form-control-static"></p>
					</td>
					<td class="good-row-price">
						<p class="form-control-static"></p>
					</td>
					<td class="good-discount">
						<input class="form-control" type="number" name="goods[{{ $index }}][discount]" value="{{ $good['discount'] }}" min="0" placeholder="تخفیف">
					</td>
					<td class="good-tax">
						<input class="form-control" type="number" name="goods[{{ $index }}][tax]" value="{{ $good['tax'] ?? null }}" min="0" placeholder="مالیات">
					</td>
					<td class="good-total">
						<p class="form-control-static"></p>
					</td>
					<td>
						<div class="btn-group">
							<a href="javascript:;" data-repeater-delete class="btn red btn-outline mt-repeater-delete"> 
								<i class="fa fa-trash"></i> 
							</a>
						</div>
					</td>
				</tr>
			@empty
				<tr data-repeater-item>
					<td> 1 </td>
					<td>
						<select class="form-control" name="goods[0][id]" required>
							<option>انتخاب کالا</option>
							@foreach($goods as $good)
								<option value="{{ $good->id }}"> {{ $good->name }}</option>
							@endforeach
						</select> 
					</td>
					<td class="good-name">
						<p class="form-control-static"></p>
					</td>
					<td class="good-price"> 
						<input class="form-control" type="number" name="goods[0][price]" min="0" placeholder="قیمت" required>
					</td>
					<td class="good-quantity">
						<input class="form-control" type="number" name="goods[0][quantity]" min="1" placeholder="تعداد">
					</td>
					<td class="good-unit">
						<p class="form-control-static"></p>
					</td>
					<td class="good-row-price">
						<p class="form-control-static"></p>
					</td>
					<td class="good-discount">
						<input class="form-control" type="number" name="goods[0][discount]" min="0" placeholder="تخفیف">
					</td>
					<td class="good-tax">
						<input class="form-control" type="number" name="goods[0][tax]" min="0" placeholder="مالیات">
					</td>
					<td class="good-total">
						<p class="form-control-static"></p>
					</td>
					<td>
						<div class="btn-group">
							<a href="javascript:;" data-repeater-delete class="btn red btn-outline mt-repeater-delete"> 
								<i class="fa fa-trash"></i> 
							</a>
						</div>
					</td>
				</tr>
			@endforelse
		</tbody>
		<tfoot>
			<tr style="border: 0">
				<th colspan="6" style="border: 0"></th>
				<th class="total-row-price"> 0 </th>
				<th class="total-discount"> 0 </th>
				<th class="total-tax"> 0 </th>
				<th class="total-price"> 0 </th>
				<th style="border: 0"> </th>
			</tr>
		</tfoot>
	</table>
	<a href="javascript:;" data-repeater-create class="btn btn-success mt-repeater-add">
		<i class="fa fa-plus"></i> افزودن
	</a>
</div>

@section('page_scripts')
@parent
<script src="{{ asset('assets/global/plugins/jquery-repeater/jquery.repeater.js') }}" type="text/javascript"></script>
<script>
	var goods = {!! $goods->toJson() !!};
</script>
<script src="{{ asset('assets/partials/scripts/factors/goods.js') }}" type="text/javascript"></script>
@endsection
