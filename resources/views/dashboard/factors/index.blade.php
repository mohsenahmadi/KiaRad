@extends('dashboard.layouts.dashboard')

@section('page_title', ' لیست فاکتورهای '.(Route::is('dashboard.buy.*') ? 'خرید' : 'فروش'))

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  لیست فاکتورهای {{ Route::is('dashboard.buy.*') ? 'خرید' : 'فروش' }}
				</div>
				<div class="actions">
					@php
						$route = 'factors';
						if(Route::is('dashboard.sell.*')){
							$route = 'sell';
						} else if(Route::is('dashboard.buy.*')){
							$route = 'buy';
						}
					@endphp
					<a class="btn green" href="{{ route('dashboard.'.$route.'.create') }}">+</a>
				</div>
			</div>
			<div class="portlet-body form">
				<div class="table-scrollable">	
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>ردیف</th>
								<th>شماره فاکتور</th>
								<th>مشتری</th>
								<th>فاکتور رسمی</th>
								<th>اصلاحات</th>
							</tr>
						</thead>
						<tbody>
							@foreach($factors as $index => $factor)
								<tr>
									<td> {{ $index + 1 }} </td>
									<td> {{ $factor->number }} </td>
									<td> {{ $factor->customer->name }} </td>
									<td> {{ $factor->official ? 'بله' : 'خیر' }} </td>
									<td>
										<a class="btn btn-outline blue" href="{{ route('dashboard.'.$route.'.edit', $factor->id) }}">
											<i class="fa fa-pencil"></i>
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection