<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i> اطلاعات طرفین 
		</div>
		<div class="actions">
			<label>
				<input type="checkbox" name="official" value="1" {{ isset($data['official']) && $data['official'] ? 'checked' : '' }}> فاکتور رسمی
			</label>
			<div class="input-inline">
				<input class="form-control" type="text" name="number" value="{{ $data['number'] }}" placeholder="شماره فاکتور">
			</div>
		</div>
	</div>
	<div class="portlet-body form">
		<div class="form-horizontal">
			<div class="form-body">
				<div class="row">
					<div class="col-md-6">
						<legend>فروشنده</legend>
						@if($type == 'SELL')
							@include('dashboard.factors.user')
						@else
							@include('dashboard.factors.customer')
						@endif
					</div>

					<div class="col-md-6">
						<legend>خریدار</legend>
						@if($type == 'BUY')
							@include('dashboard.factors.user')
						@else
							@include('dashboard.factors.customer')
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@section('page_scripts')
@parent
<script>
	var customers = {!! $customers->toJson() !!};
</script>
<script src="{{ asset('assets/partials/scripts/factors/dealers.js') }}"></script>
@endsection