@extends('dashboard.layouts.dashboard')

@section('page_title', 'صدور فاکتور '.(Route::is('dashboard.buy.*') ? 'خرید' : 'فروش'))

@section('page_content')
<div class="row">
	<div class="col-md-12">
		@include('dashboard.partials.errors')
		
		<form action="{{ route('dashboard.factors.store') }}" method="POST">
			{{ csrf_field() }}
				
			@php
				$data = empty(old()) ? new App\Tenant\Models\Factor : old();
				if(empty(old())) {
					$data['goods'] = [];
					$data['user'] = ['id_number' => '', 'address' => ''];
					$data['customer'] = ['id_number' => '', 'address' => '', 'mobile' => '', 'national_number' => '', 'economic_code' => ''];
				}
				$type = Route::is('dashboard.buy.*') ? 'BUY' : 'SELL';
			@endphp
			
			<input type="hidden" name="type" value="{{ $type }}" id="factor_type">

			@include('dashboard.factors.dealers')
			
			@include('dashboard.factors.goods')
		
			<div class="row">
				<div class="col-md-4 col-md-push-8">
					<div class="row">
						<div class="col-md-12">
							@include('dashboard.factors.summery')
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="pull-right">
								<button class="btn btn-success">ثبت</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>	
@endsection