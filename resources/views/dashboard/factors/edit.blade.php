@extends('dashboard.layouts.dashboard')

@section('page_title', 'اصلاح فاکتور '.(Route::is('dashboard.buy.*') ? 'خرید' : 'فروش'))

@section('page_content')
<div class="row">
	<div class="col-md-12">
		@include('dashboard.partials.errors')

		<form action="{{ route('dashboard.factors.update', $factor->id) }}" method="POST">
			{{ csrf_field() }}
			{{ method_field('PUT') }}
			
			@php
				$data = empty(old()) ? $factor : old();
				if(empty(old())) {
					foreach($data['goods'] as $index => $good) {
						$data['goods'][$index] = array_merge($good->toArray(), $good->pivot->toArray());
					}
					$data['user'] = ['id_number' => '', 'address' => ''];
					$data['customer'] = $factor->customer->data->pluck('value', 'key');
				}
				$type = $data['type'];
			@endphp

			@include('dashboard.factors.dealers')
			
			@include('dashboard.factors.goods')
		
			<div class="row">
				<div class="col-md-4 col-md-push-8">
					<div class="row">
						<div class="col-md-12">
							@include('dashboard.factors.summery')
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="pull-right">
								<button class="btn btn-success">ثبت</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>	
@endsection