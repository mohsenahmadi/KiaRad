<div class="form-group">
	<label class="control-label col-md-3">مشتری:</label>
	<div class="col-md-9">
		<input type="hidden" name="customer_id" value="{{ $data['customer_id'] }}">
		<input type="text" class="form-control" id="customer-typehead" placeholder="نام یا کد مشتری" required>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">موبایل:</label>
	<div class="col-md-9" id="customer-mobile" data-mobile="{{ $data['customer']['mobile'] ?? null }}"> </div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">آدرس:</label>
	<div class="col-md-9" id="customer-address" data-address="{{ $data['customer']['address'] ?? null }}"> </div>
</div>
<div  id="customer-data" 
	data-id-number="{{ $data['customer']['id_number'] ?? null }}"
	data-national-number="{{ $data['customer']['national_number'] ?? null }}"
	data-economic-code="{{ $data['customer']['economic_code'] ?? null }}"
	></div>
