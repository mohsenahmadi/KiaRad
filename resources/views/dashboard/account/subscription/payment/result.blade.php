@extends('dashboard.layouts.dashboard')

@section('page_content')
<div class="row">
	<div class="col-md-6 col-md-push-3">			    
	    <div class="portlet light bordered">
	    	<div class="portlet-title">
	    		<div class="caption {{ $payment->status == 100 ? 'font-green-sharp' : 'font-red-mint' }}"> نتیجه تمدید اشتراک </div>
	    	</div>
	    	<div class="portlet-body form">
			    @if($payment->status == 100)
			    	<p class="alert alert-success">تمدید اشتراک شما با موفقیت انجام شد</p>
			    @else
			    	<p class="alert alert-danger">تمدید اشتراک شما با موفقیت انجام نشد.</p>			        
			    @endif
			</div>
		</div>
	</div>
</div>
@endsection