<div class="form-group">
	<label class="control-label col-md-3">هزینه:</label>
	<div class="col-md-9">
		{{-- <select class="form-control" name="spend_id">
			@foreach($spend_items as $spend_item)
				@if($spend_item->id === $data['spend_id'])
					<option value="{{ $spend_item->id }}" selected>{{ $spend_item->name }}</option>
				@else
					<option value="{{ $spend_item->id }}">{{ $spend_item->name }}</option>
				@endif
			@endforeach	
		</select> --}}
		<input class="form-control" id="spend_input" type="text" name="spend_name" value="{{ $data['spend_name'] }}">
		<input type="hidden" name="spend_id" value="{{ $data['spend_id'] }}">
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">محل پرداخت:</label>
	<div class="col-md-9">
		@php
			$type = $data['source_type'] == App\Tenant\Models\Chest::class ? 'chest' : 'bank';
		@endphp
		<ul class="nav nav-tabs nav-justified" id="source-type-tab">
			<li class="{{ ($type == 'bank') ? 'active' : '' }}">	
				<a href="#bank" data-toggle="tab"> 
					بانک
				</a>				
			</li>
			<li class="{{ ($type == 'chest') ? 'active' : '' }}">	
				<a href="#chest" data-toggle="tab"> 
					صندوق
				</a>				
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane {{ $type == 'bank' ? 'active' : '' }}" id="bank">
				<input type="hidden" name="source_type" value="{{ App\Tenant\Models\BankAccount::class }}" {{ $type != 'bank' ? 'disabled' : '' }}>
				<div class="form-group">
					<select class="form-control" name="source_id" {{ $type != 'bank' ? 'disabled' : '' }}>
						@foreach($bank_accounts as $bank_account)
							@if($bank_account->id === $data['source_id'])
								<option value="{{ $bank_account->id }}" selected>
							@else
								<option value="{{ $bank_account->id }}">
							@endif
								{{ $bank_account->bank->name }} - {{ $bank_account->account_number }}
							</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="tab-pane {{ $type == 'chest' ? 'active' : '' }}" id="chest">
				<input type="hidden" name="source_type" value="{{ App\Tenant\Models\Chest::class }}" {{ $type != 'chest' ? 'disabled' : '' }}>
				<div class="form-group">
					<select class="form-control" name="source_id" {{ $type != 'chest' ? 'disabled' : '' }}>
						@foreach($chests as $chest)
							@if($chest->id === $data['source_id'])
								<option value="{{ $chest->id }}" selected>
							@else
								<option value="{{ $chest->id }}">
							@endif
								{{ $chest->name }}
							</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">مبلغ:</label>
	<div class="col-md-9">
		<input class="form-control" type="number" name="amount" min="0" value="{{ $data['amount'] }}" placeholder="مبلغ" required>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">تاریخ:</label>
	<div class="col-md-9">
		<input id="date-picker" type="text" class="form-control" value="{{ $data['expended_at'] }}" placeholder="تاریخ"/>
		<input type="hidden" name="expended_at" id="expended-at" value="{{ $data['expended_at'] }}">
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">شرح هزینه:</label>
	<div class="col-md-9">
		<textarea name="description" class="form-control" rows="5" placeholder="توضیح مختصر بابت علت انجام هزینه">{!! $data['description'] !!}</textarea>
	</div>
</div>

<div class="row">
	<div class="col-md-9 col-md-push-3">		
		<button class="btn btn-success">ذخیره</button>
	</div>
</div>

@push('page_scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $("#date-picker").pDatepicker({
    	'format': 'dddd DD/MMMM/YYYY',
    	'altField': '#expended-at',
    	'altFieldFormatter': function(unix) {
    		var date = new persianDate(unix);
    		var gregorian = date.toCalendar('gregorian');
    		var year = gregorian.year(),
    			month = gregorian.month(),
    			day = gregorian.date();

			return year+'-'+month+'-'+day;
    	}
    });

	var spendItemsSearch = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.whitespace,
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: {
			url: "{{ route('dashboard.spend_items.search') }}"+'?q=%QUERY',
			wildcard: '%QUERY'
		}
	});	

	setTypeaHead($('#spend_input'), spendItemsSearch);
	
	$('#spend_input').bind('typeahead:select', function(e, spend) {		
		$('input[name=spend_id]').val(spend.id)
	});
  });
</script>
@endpush