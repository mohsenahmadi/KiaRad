@extends('dashboard.layouts.dashboard')

@section('page_title', 'اصلاح هزینه')

@section('library_styles')
<link rel="stylesheet" href="{{ asset('assets/global/plugins/typeahead/typeahead.css') }}">
@endsection


@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		@include('dashboard.partials.errors')
		
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  اصلاح هزینه
				</div>
			</div>
			<div class="portlet-body form">
				<form action="{{ route('dashboard.expenses.update', $expense->id) }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
					{{ method_field('PUT') }}
				
					@php
						$data = empty(old()) ? $expense : old();
						$data['spend_name'] = empty(old()) ? $expense->spend->name : $data['spend_name'];
					@endphp
					
					<div class="form-body">
						@include('dashboard.expenses.form', ['data' => $data])
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('library_scripts')
<script src="{{ asset('js/bloodhound.min.js') }}"></script>
<script src="{{ asset('js/typeahead.bundle.min.js') }}"></script>
@endsection

@section('page_scripts')
<script>
	$(function() {
		$('#source-type-tab a').on('click', function(e) {
			var id = $(this).attr('href');

			$('.tab-pane:not('+id+') input, .tab-pane:not('+id+') select').prop('disabled', true);
			$('.tab-pane:not('+id+') input, .tab-pane'+id+' select').prop('disabled', false);
		});	
	});
</script>
@endsection