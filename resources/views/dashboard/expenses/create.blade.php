@extends('dashboard.layouts.dashboard')

@section('page_title', 'ثبت هزینه جدید')

@section('library_styles')
<link rel="stylesheet" href="{{ asset('assets/global/plugins/typeahead/typeahead.css') }}">
@endsection


@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		@include('dashboard.partials.errors')
		
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  ثبت هزینه جدید
				</div>
			</div>
			<div class="portlet-body form">
				<form action="{{ route('dashboard.expenses.store') }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
				
					@php
						$data = empty(old()) ? new App\Tenant\Models\Expense : old();
						$data['spend_name'] = empty(old()) ? '' : $data['spend_name'];
					@endphp
					
					<div class="form-body">
						@include('dashboard.expenses.form', ['data' => $data])
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('library_scripts')
<script src="{{ asset('js/bloodhound.min.js') }}"></script>
<script src="{{ asset('js/typeahead.bundle.min.js') }}"></script>
@endsection

@section('page_scripts')
<script>
	$(function() {
		$('#source-type-tab a').on('click', function(e) {
			var id = $(this).attr('href');

			$('.tab-pane:not('+id+') input, .tab-pane:not('+id+') select').prop('disabled', true);
			$('.tab-pane:not('+id+') input, .tab-pane'+id+' select').prop('disabled', false);
		});	
	});
</script>
@endsection