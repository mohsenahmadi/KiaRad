@extends('dashboard.layouts.dashboard')

@section('page_title', ' دفتر حساب '. $spend->name)

@section('page_content')
<form id="filter" action="{{ route('dashboard.expenses.show', $spend) }}" method="GET" style="display: none"></form>
<div class="row">
	<div class="col-md-8 col-md-push-2">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  دفتر حساب {{ $spend->name }}
				</div>
				<div class="actions">
					<a class="btn green" href="{{ route('dashboard.expenses.create') }}">+</a>
				</div>
			</div>
			<div class="portlet-body form">
				<div class="table-scrollable">	
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>ردیف</th>								
								<th width="150">تاریخ پرداخت</th>
								<th>توضیحات</th>
								<th>میزان هزینه</th>
								<th></th>
							</tr>
							<tr class="table-filter">
								<th>
									<button class="btn btn-primary">						
										<i class="fa fa-search"></i>
									</button>
								</th>
								<th>
									<div>
										<label>از:</label>
										<input type="text" id="begin-filter" value="{{ Request::get('begin') }}" class="form-control" placeholder="شروع بازه" autocomplete="off" style="display: inline-block; max-width: 100px;">
										<input type="hidden" name="begin" value="{{ Request::get('begin') }}">                
									</div>
									<div>
										<label>تا:</label>
										<input type="text" id="end-filter" value="{{ Request::get('end') }}" class="form-control " placeholder="پایان بازه" autocomplete="off" style="display: inline-block; max-width: 100px;">
										<input type="hidden" name="end" value="{{ Request::get('end') }}">
									</div>
								</th>
								<th> </th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($expenses as $index => $expense)
								<tr>
									<td> {{ $index + 1 }} </td>
									<td> {{ jdate($expense->expended_at)->format('%Y/%m/%d') }}</td>
									<td> {{ $expense->description }} </td>
									<td> {{ $expense->amount }} </td>
									<td>
										<a class="btn btn-outline blue" href="{{ route('dashboard.expenses.edit', $expense->id) }}">
											<i class="fa fa-pencil"></i>
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('page_scripts')
<script type="text/javascript">	
	$(document).ready( function () {
	  var beginFilter = $('input#begin-filter').pDatepicker({    
	    initialValueType: 'gregorian',
	    format: 'L',
	    altField: 'input[name=begin]',
	    altFieldFormatter: function(unix) {
	      return new persianDate(unix).toCalendar('gregorian').toLocale('en').format('YYYY-MM-DD')
	    },
	    maxDate: new persianDate().valueOf(),
	    onSelect: function(date) {
	      endFilter.options.minDate = date;
	    }
	  });
	  var endFilter = $('input#end-filter').pDatepicker({    
	    initialValueType: 'gregorian',
	    format: 'L',
	    altField: 'input[name=end]',
	    altFieldFormatter: function(unix) {
	      return new persianDate(unix).toCalendar('gregorian').toLocale('en').format('YYYY-MM-DD')
	    },
	    maxDate: new persianDate().valueOf(),
	    onSelect: function(date) {
	      beginFilter.options.maxDate = date;
	    }
	  });

	  $('.table-filter button').on('click', function(){
			var clone = $('.table-filter').clone();			
			$('form#filter').html(clone).submit();
		});
	});
</script>
@endpush