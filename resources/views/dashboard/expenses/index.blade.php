@extends('dashboard.layouts.dashboard')

@section('page_title', ' دفتر هزینه ها ')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  دفتر هزینه ها 
				</div>
			</div>
			<div class="portlet-body form">
				<div class="table-scrollable">	
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>کد سرفصل هزینه</th>
								<th>سرفصل هزینه</th>
								<th>میزان هزینه</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($spends as $index => $spend)
								<tr>
									<td> {{ $spend->spend->code }} </td>
									<td> {{ $spend->spend->name }} </td>
									<td> {{ $spend->total }} </td>
									<td>
										<a class="btn btn-outline blue" href="{{ route('dashboard.expenses.show', $spend->spend_id) }}">
											<i class="fa fa-eye"></i>
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection