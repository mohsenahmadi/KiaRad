@extends('dashboard.layouts.dashboard')

@section('page_title', ' لیست حسابهای بانکی')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  لیست حسابهای بانکی
				</div>
				<div class="actions">
					<a class="btn green" href="{{ route('dashboard.bank_accounts.create') }}">+</a>
				</div>
			</div>
			<div class="portlet-body form">
				<div class="table-scrollable">	
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>بانک</th>
								<th>شماره حساب</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($bank_accounts as $index => $bank_account)
								<tr>
									<td> {{ $index + 1 }} </td>
									<td> {{ $bank_account->bank->name }} </td>
									<td> {{ $bank_account->account_number }} </td>
									<td>
										<a class="btn btn-outline blue" href="{{ route('dashboard.bank_accounts.edit', $bank_account->id) }}">
											<i class="fa fa-pencil"></i>
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection