@extends('dashboard.layouts.dashboard')

@section('page_title', 'ایجاد حساب بانکی جدید')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		@include('dashboard.partials.errors')
		
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  ایجاد حساب بانکی جدید
				</div>
				<div class="actions">
					@include('dashboard.partials.buttons.delete', ['route' => 'dashboard.bank_accounts.destroy', 'id' => $bank_account->id])
				</div>
			</div>
			<div class="portlet-body form">


			<form action="{{ route('dashboard.bank_accounts.update', $bank_account->id) }}" method="POST" class="form-horizontal">
				{{ csrf_field() }}
				{{ method_field('PUT') }}
				
				@php
					$data = empty(old()) ? $bank_account : old();
				@endphp
				
				<div class="form-body">
					@include('dashboard.bank_accounts.form', ['data' => $data])
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('page_scripts')
<script src="{{ asset('metronic/js/helpers.js') }}"></script>
@endsection