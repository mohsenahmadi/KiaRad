<div class="form-group">
	<label class="control-label col-md-3">بانک:</label>
	<div class="col-md-9">		
		<select class="form-control" name="bank_id">
			@foreach($banks as $bank)
				@if($bank->id == $data['bank_id'])
					<option value="{{ $bank->id }}" selected> {{ $bank->name }} </option>
				@else
					<option value="{{ $bank->id }}"> {{ $bank->name }} </option>
				@endif
				
			@endforeach
		</select>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">شماره حساب:</label>
	<div class="col-md-9">
		<input class="form-control" type="text" name="account_number" value="{{ $data['account_number'] }}">
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">شبا:</label>
	<div class="col-md-9">
		<div class="input-icon right">
			<i style="margin-top: 7px;">IR</i>
			<input class="form-control" type="text" name="IBAN" style="text-align:left" value="{{ $data['IBAN'] }}">
			<div class="help-block">شماره شبا باید ۲۴ رقم باشد</div>
		</div>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">شماره کارت:</label>
	<div class="col-md-9">
		<input class="form-control" type="text" name="card_number" value="{{ $data['card_number'] }}">
		<div class="help-block">شماره کارت باید ۱۶ رقم باشد</div>
	</div>
</div>

<div class="row">
	<div class="col-md-9 col-md-push-3">		
		<button class="btn btn-success">ذخیره</button>
	</div>
</div>