<div class="form-group">
	<label class="control-label col-md-3">مشتری:</label>
	<div class="col-md-9">
		<select name="customer_id" class="form-control" id="customer-select">
			<option>انتخاب مشتری</option>
			@foreach($customers as $customer)
			@if($customer->id == $data['customer_id'])
			<option value="{{ $customer->id }}" selected>
				@else
				<option value="{{ $customer->id }}">
					@endif
					{{ $customer->code }} - {{ $customer->name }}
				</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3">موبایل:</label>
		<div class="col-md-9" id="customer-mobile"> </div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3">آدرس:</label>
		<div class="col-md-9" id="customer-address"> </div>
	</div>
	<div  id="customer-data"></div>