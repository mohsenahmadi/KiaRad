<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i> اطلاعات طرفین 
		</div>
		<div class="actions">
			<div>
				<label>
					<input id="factor-official" type="checkbox" name="official" value="1" {{ isset($data['official']) && $data['official'] ? 'checked' : '' }} disabled> فاکتور رسمی
				</label>
				<div class="input-inline">
					@php
						if(isset($data['number'])) {
							$number = $data['number'];
							if(isset($data['official']) && $data['official']) {
								$alternative = $factor_numbers['person']; 
							} else {
								$alternative = $factor_numbers['company'];
							}
						} else {
							if(isset($data['official']) && $data['official']) {
								$number = $factor_numbers['company']; 
								$alternative = $factor_numbers['person']; 
							} else {
								$number = $factor_numbers['person'];
								$alternative = $factor_numbers['company'];
							}
						}
					@endphp
					<input id="factor-number" class="form-control" type="text" name="number" value="{{ $number }}" placeholder="شماره فاکتور" data-other="{{ $alternative }}" required>
				</div>
			</div>
			<div>
				<label >تاریخ</label>
				<div class="input-inline pull-right">
					<input id="date-picker" type="text" class="form-control" value="{{ $data['issued_at'] }}" placeholder="تاریخ"/>
				</div>
				<input type="hidden" name="issued_at" id="issued-at" value="{{ $data['issued_at'] }}">
			</div>
		</div>
	</div>
	<div class="portlet-body form">
		<div class="form-horizontal">
			<div class="form-body">
				<div class="row">
					<div class="col-md-6">
						<legend>فروشنده</legend>						
						@include('dashboard.factors.user')						
					</div>

					<div class="col-md-6">
						<legend>خریدار</legend>
						@include('dashboard.factors.customer')						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@push('page_scripts')
<script>	
	var customersSearchUrl = "{{ route('dashboard.customers.search') }}";
	var customerFetch = "{{ isset($data['customer_id']) && !empty($data['customer_id']) ? 
													route('dashboard.customers.show', $data['customer_id']) : null }}";
	@php
		if($subscription) {
			$year = jdate($subscription->expired_at->subYears(1))->format('Y');
			$min = \Morilog\Jalali\jDateTime::toGregorian($year, 1, 1);
			$max = \Morilog\Jalali\jDateTime::toGregorian($year, 12, 29);
		} else {
			$now = Carbon\Carbon::now();
			$min = [$now->year, $now->month, $now->day];
			$max = [$now->year, $now->month, $now->day];
		}		
	@endphp
	var limit = @json(['min' => $min, 'max' => $max]);
</script>
<script src="{{ asset('assets/partials/scripts/factors/dealers.js') }}"></script>
@endpush