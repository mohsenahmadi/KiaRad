@extends('dashboard.layouts.dashboard')

@section('page_title', 'صدور فاکتور فروش'))

@section('library_styles')
<link rel="stylesheet" href="{{ asset('assets/global/plugins/typeahead/typeahead.css') }}">
@endsection

@section('page_content')
<div class="row">
	<div class="col-md-12">
		@include('dashboard.partials.errors')
		
		<form action="{{ route('dashboard.sell.store') }}" method="POST">
			{{ csrf_field() }}
				
			@php
				$data = empty(old()) ? new App\Tenant\Models\Factor : old();
				if(empty(old())) {
					$data['goods'] = [];
					$data['user'] = ['id_number' => '', 'address' => ''];
					$data['customer'] = ['id_number' => '', 'address' => '', 'mobile' => '', 'national_number' => '', 'economic_code' => ''];
				}
			@endphp
					
			@include('dashboard.sell_factors.dealers')
			
			@include('dashboard.sell_factors.goods')
			
			<div class="row margin-top-20">
				<div class="col-md-12">
					@include('dashboard.sell_factors.description')
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-md-push-8">
					<div class="row">
						<div class="col-md-12">
							@include('dashboard.sell_factors.summery')
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="pull-right">
								<button class="btn btn-success">ثبت</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>	
@endsection

@section('library_scripts')
<script src="{{ asset('js/bloodhound.min.js') }}"></script>
<script src="{{ asset('js/typeahead.bundle.min.js') }}"></script>
@endsection