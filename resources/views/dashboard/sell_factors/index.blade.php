@extends('dashboard.layouts.dashboard')

@section('page_title', ' لیست فاکتورهای فروش')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  لیست فاکتورهای فروش
				</div>
				<div class="actions">
					<a class="btn green" href="{{ route('dashboard.sell.create') }}">+</a>
					
				</div>
			</div>
			<div class="portlet-body form">
				<form action="{{ route('dashboard.sell.index') }}" class="form-horizontal" onchange="$(this).submit();">
					@php
						$official = Request::get('official', -1);
					@endphp
					<div class="form-group">
						<label class="col-md-3 control-label">نمایش:</label>
						<div class="mt-radio-inline">
							<label class="mt-radio">
								<input type="radio" name="official" value="1"  {{ $official == 1 ? 'checked': '' }}> فاکتورهای رسمی
								<span></span>
							</label>
							<label class="mt-radio">
								<input type="radio" name="official" value="0" {{ $official == 0 ? 'checked': '' }}> فاکتورهای غیررسمی
								<span></span>
							</label>
							<label class="mt-radio">
								<input type="radio" name="official" value="-1" {{ $official == -1 ? 'checked': '' }}> هردو
								<span></span>
							</label>
						</div>
					</div>
				</form>
				<div class="table-scrollable">	
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>ردیف</th>
								<th>شماره فاکتور</th>
								<th>مشتری</th>
								<th>تاریخ</th>
								<th>فاکتور رسمی</th>
								<th>اصلاحات</th>
							</tr>
						</thead>
						<tbody>
							@foreach($factors as $index => $factor)
								<tr>
									<td> {{ $index + 1 }} </td>
									<td> {{ $factor->number }} </td>
									<td> {{ $factor->customer->name }} </td>
									<td> {{ jdate($factor->issued_at)->format('%Y/%m/%d') }}</td>
									<td> {{ $factor->official ? 'بله' : 'خیر' }} </td>
									<td>
										<a class="btn btn-outline blue" href="{{ route('dashboard.sell.edit', $factor->id) }}">
											<i class="fa fa-pencil"></i>
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection