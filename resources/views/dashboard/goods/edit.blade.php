@extends('dashboard.layouts.dashboard')

@section('page_title', 'اصلاح اطلاعات کالا')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		@include('dashboard.partials.errors')
		
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  اصلاح اطلاعات کالا
				</div>
				<div class="actions">
					@include('dashboard.partials.buttons.delete', ['route' => 'dashboard.goods.destroy', 'id' => $good->id])
				</div>
			</div>
			<div class="portlet-body form">
				<form action="{{ route('dashboard.goods.update', $good->id) }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
					{{ method_field('PUT') }}
				
					@php
						$data = empty(old()) ? $good : old();
						$data['disabled'] = true;
						$data['code'] = old('code', $good->code);
					@endphp
					
					<div class="form-body">
						@include('dashboard.goods.form', ['data' => $data])
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('page_scripts')
<script src="{{ asset('metronic/js/helpers.js') }}"></script>
@endsection