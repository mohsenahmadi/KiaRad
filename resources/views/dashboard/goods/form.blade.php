@section('library_styles')
<link rel="stylesheet" href="{{ asset('assets/global/plugins/typeahead/typeahead.css') }}">
@endsection


<div class="form-group">
	<label class="control-label col-md-3">کد:</label>
	<div class="col-md-9">
		@if($data['disabled'])
		<input class="form-control" type="text" value="{{ $data['code'] }}" disabled>
		@else
		<input class="form-control" type="text" name="code" value="{{ $data['code'] }}" placeholder="کد" autocomplete="off">
		@endif
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">نام:</label>
	<div class="col-md-9">
		<input class="form-control" type="text" name="name" value="{{ $data['name'] }}" placeholder="نام" autocomplete="off">
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">دسته:</label>
	<div class="col-md-9">
		<input type="hidden" name="parent_id" value="{{ $data['parent_id'] }}">
		<input class="form-control" id="parent-typeahead" type="text" placeholder="دسته">
	</div>
</div>
@if(!$data['disabled'])
<div class="form-group">
	<label class="col-md-3 control-label">نوع:</label>
	<div class="mt-radio-inline">
		<label class="mt-radio">
			<input type="radio" name="is_cat" value="1"  {{ $data['is_cat'] ? 'checked': '' }}> دسته
			<span></span>
		</label>
		<label class="mt-radio">
			<input type="radio" name="is_cat" value="0" {{ !$data['is_cat'] ? 'checked': '' }}> کالا
			<span></span>
		</label>
	</div>
</div>
@endif
<div id="good-attributes" style="{{ $data['is_cat'] ? 'display: none' : '' }}">
	<div class="form-group">
		<label class="control-label col-md-3">انبار:</label>
		<div class="col-md-9">
			<select class="form-control" name="warehouse_id">
				@foreach($warehouses as $warehouse)
				@if($warehouse->id === $data['warehouse_id'])
				<option value="{{ $warehouse->id }}" selected>{{ $warehouse->name }}</option>
				@else
				<option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
				@endif
				@endforeach	
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3">واحد:</label>
		<div class="col-md-9">
			<input class="form-control" type="text" name="unit" value="{{ $data['unit'] }}" placeholder="واحد">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3">قیمت پیشنهادی خرید:</label>
		<div class="col-md-9">
			<input class="form-control" type="text" name="suggestion_buy_price" value="{{ $data['suggestion_buy_price'] }}" placeholder="قیمت پیشنهادی خرید">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3">قیمت پیشنهادی فروش:</label>
		<div class="col-md-9">
			<input class="form-control" type="text" name="suggestion_sell_price" value="{{ $data['suggestion_sell_price'] }}" placeholder="قیمت پیشنهادی فروش">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3">مالیات ارزش افزوده:</label>			
		<div class="col-md-9">
			<input class="form-control" type="number" name="tax" value="{{ $data['tax'] ?? 9 }}" placeholder="مالیات ارزش افزوده">
			<div class="help-block">در صورتی که خالی باشد صفر در نظر گرفته میشود</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-9 col-md-push-3">		
		<button class="btn btn-success">ذخیره</button>
	</div>
</div>

@section('library_scripts')
<script src="{{ asset('js/bloodhound.min.js') }}"></script>
<script src="{{ asset('js/typeahead.bundle.min.js') }}"></script>
@endsection

@section('page_scripts')
<script>
	jQuery(document).ready(function() {
		$('input[name=is_cat]').on('change', function(){
			if($(this).val() != 1){
				$('#good-attributes').css('display', 'block');
			}else{
				$('#good-attributes').css('display', 'none');
			}
		});

		var goodsSearch = new Bloodhound({
			datumTokenizer: Bloodhound.tokenizers.whitespace,
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			remote: {
				url: "{{ route('dashboard.goods.search', ['is_cat' => true, 'q' => '%QUERY']) }}",
				wildcard: '%QUERY'
			}
		});

		setTypeaHead($('#parent-typeahead'), goodsSearch);		

		$('#parent-typeahead').bind('typeahead:select', '.good-name input', function(e, good) {		$('input[name=parent_id]').val(good.id);
		});

		@if($data['parent_id'])
			$.ajax({
				url: "{{ route('dashboard.goods.show', $data['parent_id'] ) }}",
			})
			.done(function(good) {
				$('#parent-typeahead').val(good.name);
			});	
		@endif
	});
</script>
@endsection