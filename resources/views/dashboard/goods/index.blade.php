@extends('dashboard.layouts.dashboard')

@section('page_title', ' لیست کالاها و خدمات')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  لیست کالاها و خدمات
				</div>
				<div class="actions">
					<a class="btn green" href="{{ route('dashboard.goods.create') }}">+</a>
				</div>
			</div>
			<div class="portlet-body form">
				<div>
					@if($parent)
						@php
							if($parent->parent_id) {
								$route = route('dashboard.goods.childs', [$parent->parent_id]);
							}else{
								$route = route('dashboard.goods.index');
							}
						@endphp
						<a href="{{ $route }}" class="btn btn-success">بازگشت</a>
					@endif					
				</div>
				@php
					$action = $parent ? route('dashboard.goods.childs', $parent->id) : route('dashboard.goods.index');
				@endphp
				<form action="{{ $action }}" method="GET" id="search-form"> 					
					<input type="hidden" name="warehouse" value="{{ Request::route('warehouse.id', null) }}">
					<input type="hidden" name="name">
					<input type="hidden" name="code">
				</form>
				<div class="table-scrollable">	
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>ردیف</th>
								<th>کد کالا یا دسته</th>
								<th>نام کالا یا دسته</th>
								<th>کالا یا دسته</th>
								<th>تعداد فرزند</th>
								{{-- <th>قیمت پیشنهادی خرید</th>
								<th>قیمت پیشنهادی فروش</th>
								<th>واحد</th> --}}
								<th></th>
							</tr>
							<tr id="search-row">
								<td></td>
								<td>
									<input type="text" class="form-control" name="code" placeholder="کد" value="{{ Request::get('code') }}">
								</td>
								<td>
									<input type="text" class="form-control" name="name" placeholder="نام" value="{{ Request::get('name') }}">
								</td>
								<td colspan="2"></td>
								<td>
									<button type="button" class="btn btn-success btn-sm">جستجو</button>
								</td>
							</tr>
						</thead>
						<tbody>
							@foreach($goods as $index => $good)
								<tr>
									<td> {{ $index + 1 }} </td>
									<td> {{ $good->code }} </td>
									<td> {{ $good->name }} </td>
									<td> {{ $good->is_cat ? 'دسته' : 'کالا' }} </td>
									<td> {{ $good->is_cat ? $good->children()->count() : '-' }} </td>
									{{-- <td> {{ $good->suggestion_buy_price }} </td> --}}
									{{-- <td> {{ $good->suggestion_sell_price }} </td> --}}
									{{-- <td> {{ $good->unit }} </td> --}}
									<td style="width:130px">
										<div class="btn-group btn-group-lg btn-group-solid"> 
											@if($good->is_cat)
												<a class="btn btn-sm green-dark" href="{{ route('dashboard.goods.childs', $good) }}">
													<i class="fa fa-indent"></i>
												</a>
											@else
												<a class="btn btn-sm green-dark" href="{{ route('dashboard.goods.trades', $good) }}">
													<i class="fa fa-eye"></i>
												</a>
											@endif
											<a class="btn btn-sm btn-outline blue" href="{{ route('dashboard.goods.edit', $good) }}">
												<i class="fa fa-pencil"></i>
											</a>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('page_scripts')
<script>
	jQuery(document).ready(function() {
		$('#search-row button').on('click', function(e) {
			$.each($('#search-row input'), function(index, input) {
				$('#search-form input[name='+$(input).attr('name')+']').val($(input).val());
			});
			$('#search-form').submit();
		});
	});	
</script>
@endsection