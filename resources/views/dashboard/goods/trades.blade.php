@extends('dashboard.layouts.dashboard')

@section('page_title', ' کاردکس '.$good->name)

@section('page_content')
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  کاردکس {{ $good->name }}
				</div>
				<div class="actions">
					<a class="btn" href="{{ url()->previous() }}">بازگشت</a>
				</div>
			</div>
			<div class="portlet-body form">				
				<div class="table-scrollable">	
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th colspan="3"></th>
								<th colspan="3">وارده</th>
								<th colspan="3">صادره</th>
								<th colspan="3">موجودی</th>
							</tr>
							<tr>
								<th>ردیف</th>
								<th>تاریخ</th>
								<th>شرح</th>
								<th>تعداد</th>
								<th>مبلغ واحد</th>
								<th>مبلغ کل</th>
								<th>تعداد</th>
								<th>مبلغ واحد</th>
								<th>مبلغ کل</th>
								<th>تعداد</th>
								<th>مبلغ واحد</th>
								<th>مبلغ کل</th>
							</tr>							
						</thead>
						<tbody>
							@php
								$total_quantity = 0;
								$total_price = 0;
							@endphp
							@foreach($factors as $index => $factor)
								<tr>
									<td> {{ $index + 1 }} </td>
									<td> {{ jdate($factor->issued_at)->format('%Y/%m/%d') }}</td>
									@php
										$factor_good = $factor->goods()->find($good->id);
										$price = $factor_good->pivot->price;
										// $discount = $factor_good->pivot->discount;
										$quantity = $factor_good->pivot->quantity;
									@endphp
									@if(get_class($factor) == App\Tenant\Models\SellFactor::class)
										@php
											$price = $total_quantity ? $total_price / $total_quantity : 0;
											$total_quantity -= $quantity;
											$total_price -= ($quantity * $price);
										@endphp
										<td>فروش</td>
										<td></td> <td></td> <td></td>
										<td>{{ $quantity }}</td>
										<td>{{ $price }}</td>
										<td>{{ $quantity * $price }}</td>
									@else
										@php
											$total_quantity += $quantity;
											$total_price += ($quantity * $price);
										@endphp
										<td>خرید</td>
										<td>{{ $quantity }}</td>
										<td>{{ $price }}</td>
										<td>{{ $quantity * $price }}</td>
										<td></td> <td></td> <td></td>
									@endif							
									<td>{{ $total_quantity }}</td>
									<td>{{ $total_quantity ? $total_price / $total_quantity : 0 }}</td>
									<td>{{ $total_price }}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('page_scripts')
<script>
	jQuery(document).ready(function() {
		$('#search-row button').on('click', function(e) {
			$.each($('#search-row input'), function(index, input) {
				$('#search-form input[name='+$(input).attr('name')+']').val($(input).val());
			});
			$('#search-form').submit();
		});
	});	
</script>
@endsection