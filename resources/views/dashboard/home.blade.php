@extends('dashboard.layouts.dashboard')

@section('page_content')
<!-- BEGIN DASHBOARD STATS 1-->
<div class="row">
    @php
    $counters = [
        [
            'color' => 'blue',
            'number' => number_format($statistics['total_sell']),
            'title' => 'فروش کل'
        ],
        [        
            'color' => 'red',
            'number' => number_format($statistics['total_buy']),
            'title' => 'خرید کل'
        ],
        [
            'color' => 'purple',
            'number' => number_format($statistics['total_expense']),
            'title' => 'هزینه ها'
        ],
        [
            'color' => 'green',
            'number' => number_format($statistics['total_sell'] - $statistics['total_buy'] -  $statistics['total_expense']),
            'title' => 'سود کل'
        ]
    ];
    @endphp

    @foreach($counters as $counter)
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        @include('dashboard.modules.counter', array_merge($counter, ['suffix' => 'تومان']))
    </div>
    @endforeach
</div>
<div class="clearfix"></div>
<!-- END DASHBOARD STATS 1-->
<div class="row">
    @foreach($graphs as $id => $graph)
        @php
            $data = $graph->map(function($item, $index) {
                return [$item->issued_at, $item->total];
            });
            
            $title = ['sells' => 'نمودار فروش', 'buys' => 'نمودار خرید'][$id];
            $color = ['sells' => '#BAD9F5', 'buys' => '#f89f9f'][$id];
        @endphp
        <div class="col-lg-6 col-xs-12 col-sm-12">
            @include('dashboard.modules.graph', compact('data', 'title', 'id'))
        </div>
    @endforeach
</div>
@endsection

@section('page_scripts')
<script type="text/javascript">
    function showChartTooltip(x, y, xValue, yValue) {
        $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
            position: 'absolute',
            display: 'none',
            top: y - 40,
            left: x - 40,
            border: '0px solid #ccc',
            padding: '2px 6px',
            'background-color': '#fff'
        }).appendTo("body").fadeIn(200);
    }
</script>
{{-- <script src="{{ asset('assets/pages/scripts/dashboard.min.js') }}" type="text/javascript"></script> --}}
@endsection