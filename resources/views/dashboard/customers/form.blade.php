<ul class="nav nav-tabs nav-justified" id="person-type-tab">
	@foreach($types as $type)
		<li class="{{ ($data['type_id'] ? $data['type_id'] == $type->id : $loop->first) ? 'active' : '' }}">	
			<a href="#tab_{{ $type->id }}" data-toggle="tab" data-id="{{ $type->id }}"> 
				{{ $type->label }} 
			</a>				
		</li>
	@endforeach
</ul>
<input type="hidden" name="type_id" value="{{ $data['type_id'] ?? $types->first()->id }}">
<div class="form-group">
	<label class="control-label col-md-3">کد مشتری:</label>
	<div class="col-md-9">
		@if($data['disabled'])
		<input class="form-control" type="text" value="{{ $data['code'] }}" disabled>
		@else
		<input class="form-control" type="text" name="code" placeholder="کد مشتری" value="{{ $data['code'] }}" required>
		@endif		
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">نام مشتری:</label>
	<div class="col-md-9">
		<input class="form-control" type="text" name="name" placeholder="نام مشتری" value="{{ $data['name'] }}" autocomplete="off" required>		
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">تلفن:</label>
	<div class="col-md-7">
		<input class="form-control" type="text" name="phone"  placeholder="تلفن" value="{{ $data['phone'] }}">		
	</div>
	<div class="col-md-2">
		<input class="form-control" type="text" name="state_code"  placeholder="کد شهر" value="{{ $data['state_code'] }}">		
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">موبایل:</label>
	<div class="col-md-9">
		<input class="form-control" type="text" name="mobile"  placeholder="موبایل" value="{{ $data['mobile'] }}">		
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">آدرس:</label>
	<div class="col-md-9">
		<textarea class="form-control" name="address" rows="2" placeholder="آدرس">{{ $data['address'] }}</textarea>
	</div>
</div>
<hr>
<div class="tab-content">
	<div class="tab-pane {{ ($data['type_id'] ? $data['type_id'] == 1 : true) ? 'active' : '' }}" id="tab_1">
		<div class="form-group">
			<label class="control-label col-md-3">کد ملی:</label>
			<div class="col-md-9">
				<input class="form-control" type="text" name="data[id_number]"  placeholder="کد ملی" value="{{ $data['data']['id_number'] ?? null }}">		
			</div>
		</div>
	</div>
	<div class="tab-pane {{ ($data['type_id'] ? $data['type_id'] == 2 : false) ? 'active' : '' }}" id="tab_2">
		<div class="form-group">
			<label class="control-label col-md-3">کد اقتصادی:</label>
			<div class="col-md-9">
				<input class="form-control" type="text" name="data[economic_code]"  placeholder="کد اقتصادی" value="{{ $data['data']['economic_code'] ?? null }}">		
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">شناسه ملی:</label>
			<div class="col-md-9">
				<input class="form-control" type="text" name="data[national_number]"  placeholder="شناسه ملی" value="{{ $data['data']['national_number'] ?? null }}">		
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-9 col-md-push-3">
		<button class="btn btn-success">ذخیره</button>
	</div>
</div>
