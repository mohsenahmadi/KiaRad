@extends('dashboard.layouts.dashboard')

@section('page_title', 'ایجاد مشتری جدید')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		@include('dashboard.partials.errors')
		
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  ایجاد مشتری جدید
				</div>
			</div>
			<div class="portlet-body form">
				<form action="{{ route('dashboard.customers.store') }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
					
					@php
						$data = empty(old()) ? new App\Tenant\Models\Customer : old();
						$data['disabled'] = false;
					@endphp
					<div class="form-body">
						@include('dashboard.customers.form', ['data' => $data])
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('page_scripts')
<script>
	$(function() {
		$('#person-type-tab a').on('click', function(e) {
			$('input[name=type_id]').val($(this).data('id'));
		});	
	});
</script>
@endsection
