@extends('dashboard.layouts.dashboard')

@section('page_title', ' لیست مشتریان')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  لیست مشتریان
				</div>
				<div class="actions">
					<div class="portlet-input input-inline input-small">
						<form action="{{ route('dashboard.customers.index', Request::all()) }}">
							@foreach(Request::except('q') as $key => $value)
								<input type="hidden" name="{{ $key }}" value="{{ $value }}">
							@endforeach
							<div class="input-icon right">
								<i class="icon-magnifier"></i>
								<input type="text" class="form-control input-circle" name="q" placeholder="جستجو..." value="{{ Request::get('q') }}"> 
							</div>
						</form>
					</div>
					<a class="btn green" href="{{ route('dashboard.customers.create') }}">+</a>
				</div>
			</div>
			<div class="portlet-body">
				<ul class="nav nav-tabs nav-justified">
					@php $pageType = Request::get('type', 'all'); @endphp
					<li class="{{ $pageType == 'all' ? 'active' : '' }}">	
						<a href="{{ route('dashboard.customers.index') }}"> 
							همه مشتریان
						</a>				
					</li>
					@foreach($types as $type)
					<li class="{{ $pageType == $type->name ? 'active' : '' }}">	
						<a href="{{ route('dashboard.customers.index', ['type' => $type->name]) }}"> 
							{{ $type->label }} 
						</a>				
					</li>
					@endforeach
				</ul>

				<div class="table-scrollable">	
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>کد مشتری</th>
								<th>نام مشتری</th>
								<th>شخصیت</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($customers as $index => $customer)
								<tr>
									<td> {{ $index + 1 }} </td>
									<td> {{ $customer->code }} </td>
									<td> {{ $customer->name }} </td>
									<td> {{ $customer->type->label }} </td>
									<td>
										<a class="btn btn-outline blue" href="{{ route('dashboard.customers.edit', $customer->id) }}">
											<i class="fa fa-pencil"></i>
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
