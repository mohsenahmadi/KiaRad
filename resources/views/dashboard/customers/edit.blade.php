@extends('dashboard.layouts.dashboard')

@section('page_title', 'اصلاح اطلاعات مشتری')

@section('page_content')
<div class="row">
	<div class="col-md-8 col-md-push-2">
		@include('dashboard.partials.errors')
		
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>  اصلاح اطلاعات مشتری
				</div>
				<div class="actions">
					@include('dashboard.partials.buttons.delete', ['route' => 'dashboard.customers.destroy', 'id' => $customer->id])
				</div>
			</div>
			<div class="portlet-body form">
				<form action="{{ route('dashboard.customers.update', $customer->id) }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
					{{ method_field('PUT') }}

					@php
						if(empty(old())){
							$data = $customer;
							$data['data'] = $customer->data->pluck('value', 'key');
						}else{
							$data = old();
							$data['code'] = $customer->code;
						}
						$data['disabled'] = true;
					@endphp

					<div class="form-body">
						@include('dashboard.customers.form', ['data' => $data])
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('page_scripts')
<script>
	$(function() {
		$('#person-type-tab a').on('click', function(e) {
			$('input[name=type_id]').val($(this).data('id'));
		});	
	});
</script>
<script src="{{ asset('metronic/js/helpers.js') }}"></script>
@endsection




