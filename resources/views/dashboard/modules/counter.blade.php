<a class="dashboard-stat dashboard-stat-v2 {{ $color ?? 'blue' }}" href="#">
    <div class="visual">
        <i class="fa fa-{{ $icon ?? 'bar-chart-o' }}"></i>
    </div>
    <div class="details">
        <div class="number">
            <span data-counter="counterup" data-value="{{ $number ?? 0 }}">0</span> {{ $suffix ?? '' }}
        </div>
        <div class="desc"> {{ $title ?? 'شمارنده' }} </div>
    </div>
</a>