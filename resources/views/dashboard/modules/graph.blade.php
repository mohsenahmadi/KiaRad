@php
    $id = $id ?? 'G-'.random_str(5);
@endphp
<!-- BEGIN PORTLET-->
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-dark bold uppercase">{{ $title ?? 'نمودار' }}</span>
        </div>        
    </div>
    <div class="portlet-body">
        <div id="{{ $id }}_loading">
            <img src="{{ asset('assets/global/img/loading.gif') }}" alt="loading" /> 
        </div>
        <div id="{{ $id }}_content" class="display-none">
            <div id="{{ $id }}" class="chart"> </div>
        </div>
    </div>
</div>
<!-- END PORTLET-->

@push('scripts')
<script type="text/javascript">

    if ($('#{{ $id }}').size() != 0) {

        $('#{{ $id }}_loading').hide();
        $('#{{ $id }}_content').show();

        var data = @json($data); 
        var plot_statistics = $.plot($("#{{ $id }}"), [{
            data: data,
            lines: {
                fill: 0.2,
                lineWidth: 0,
            },
            color: ["{{ $color ?? '#f89f9f' }}"]
        }, {
            data: data,
            points: {
                show: true,
                fill: true,
                radius: 4,
                fillColor: "{{ $color ?? '#f89f9f' }}",
                lineWidth: 2
            },
            color: "{{ $color ?? '#f89f9f' }}",
            shadowSize: 1
        }, {
            data: data,
            lines: {
                show: true,
                fill: false,
                lineWidth: 3
            },
            color: "{{ $color ?? '#f89f9f' }}",
            shadowSize: 0
        }],
        {
            xaxis: {
                tickLength: 0,
                tickDecimals: 0,
                mode: "categories",
                min: 0,
                font: {
                    lineHeight: 14,
                    style: "normal",
                    variant: "small-caps",
                    color: "#6F7B8A"
                }
            },
            yaxis: {
                ticks: 5,
                tickDecimals: 0,
                tickColor: "#eee",
                font: {
                    lineHeight: 14,
                    style: "normal",
                    variant: "small-caps",
                    color: "#6F7B8A"
                }
            },
            grid: {
                hoverable: true,
                clickable: true,
                tickColor: "#eee",
                borderColor: "#eee",
                borderWidth: 1
            }
        });

        var previousPoint = null;
        $("#{{ $id }}").bind("plothover", function(event, pos, item) {
            $("#x").text(pos.x.toFixed(2));
            $("#y").text(pos.y.toFixed(2));
            console.log('hover');
            if (item) {
                if (previousPoint != item.dataIndex) {
                    previousPoint = item.dataIndex;

                    $("#tooltip").remove();
                    var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2);

                    showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + ' تومان');
                }
            } else {
                $("#tooltip").remove();
                previousPoint = null;
            }
        });
    }
</script>
@endpush