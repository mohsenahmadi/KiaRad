@extends('layouts.register')

@section('form')
<form  action="{{ route('register') }}" method="post">
    @csrf

    <div class="form-title">
        <span class="form-title">ثبت نام کیاراد</span>                    
    </div>
    @if($errors->any())
    <div class="alert alert-danger">
        <button class="close" data-close="alert"></button>
        <ul>
            @foreach($errors->all() as $error)
                <li> {{ $error }} </li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">نام کاربری</label>
        <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="نام کاربری" name="username" value="{{ old('username') }}" required/> 
    </div>
    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">نام</label>
        <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="نام" name="first_name" value="{{ old('first_name') }}" required/> 
    </div>
    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">نام خانوادگی</label>
        <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="نام خانوادگی" name="last_name" value="{{ old('last_name') }}" required/> 
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">آدرس ایمیل</label>
        <input class="form-control form-control-solid placeholder-no-fix" type="email" autocomplete="off" placeholder="آدرس ایمیل" name="email" value="{{ old('email') }}" required /> 
    </div>
    <div class="form-actions">
        <button type="submit" class="btn red btn-block uppercase">ثبت نام</button>
    </div>
</form>
@endsection
