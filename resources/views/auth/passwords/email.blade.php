@extends('layouts.login')

@section('form')
<div class="login-content">
    <h1>گذرواژه خود را فراموش کردید؟</h1>
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <form class="login-form" action="{{ route('password.email') }}" method="post">
        {{ csrf_field() }}
        <p> آدرس ایمیل خود را وارد کنید. </p>
        {{-- <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}"> --}}
            <input class="form-control form-control-solid placeholder-no-fix form-group{{ $errors->has('email') ? ' has-error' : '' }}" type="email" placeholder="آدرس ایمیل" name="email" value="{{ old('email') }}" required/> 
        {{-- </div> --}}
        @if ($errors->has('email'))
        <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif
        <div class="form-actions">
            <button type="submit" class="btn btn-success uppercase pull-right">ارسال</button>
        </div>
    </form>   
</div>
@endsection
