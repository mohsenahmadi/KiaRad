@extends('layouts.master')

@section('page_header')
<div id="page-header" class="about2" style="max-height: 200px">
</div>
@endsection

@section('page_body')
<section class="introtext">
  <div class="row">
    <div class="col-sm-12">
      <h2>ثبت نام شما با موفقیت انجام شد. برای ورود به سایت ایمیل خود را بررسی کنید.</h2>
    </div>
  </div>
  <div class="spacing-65"></div>  
  </section>
</section>

<!-- END OF SKILLS -->
@endsection
