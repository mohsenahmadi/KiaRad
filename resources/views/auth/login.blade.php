@extends('layouts.login')

@section('form')
<div class="login-content">
    <h1>ورود به حساب کاربری</h1>
    <p> به کیاراد خوش آمدید. برای ورود به حساب کاربری اطلاعات خود را وارد کنید. </p>
    <form class="login-form" method="post" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="alert alert-danger display-hide">
            <span>آدرس ایمیل و گذرواژه را وارد کنید.</span>
        </div>
        @if($errors->any())
        <div class="alert alert-danger">
            <button class="close" data-close="alert"></button>
            @foreach($errors->all() as $error)
                <span>{{ $error }}</span>
            @endforeach
        </div>
        @endif
        <div class="row">
            <div class="col-xs-6">
                <input class="form-control form-control-solid placeholder-no-fix form-group" type="email" placeholder="آدرس ایمیل" name="email" value="{{ old('email') }}" required/> 
            </div>
            <div class="col-xs-6">
                <input class="form-control form-control-solid placeholder-no-fix form-group" type="password" autocomplete="off" placeholder="گذرواژه" name="password" required/> 
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="rem-password">
                    <label class="rememberme mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="remember" value="1" {{ old('remember') ? 'checked' : '' }}/> مرا به خاطر بسپار
                        <span></span>
                    </label>
                </div>
            </div>
            <div class="col-sm-8 text-right">
                <div class="forgot-password">
                    <a href="{{ route('password.request') }}" class="forget-password">گذرواژه خود را فراموش کردید؟</a>
                </div>
                <button class="btn green" type="submit">ورود</button>
            </div>
        </div>
    </form>
</div>
@endsection
