@extends('layouts.master')

@section('page_header')
<div id="page-header" class="about2">
 <div class="title-breadcrumbs">
 <h1>فرم ثبت شکایات</h1>
   <div class="thebreadcumb">
    <ul class="breadcrumb">
      <li><a href="/">خانه</a></li>
      {{-- <li></li> --}}
      <li class="active">فرم ثبت شکایات</li>
      {{-- <li class="active">About 2</li> --}}
    </ul>
  </div>
</div>
</div>
@endsection

@section('page_body')
<section class="introtext">
  <div class="row">
    <div class="col-sm-12">
      <h2>فرم ثبت شکایات</h2>
      <p class="text-center">چنانچه از هر کدام از خدمات عرضه شده توسط کیاراد، رضایت ندارید می‌توانید با تکمیل فرم زیر نظرات و انتقادات خود را با ما مطرح کنید. در اولین فرصت با شما تماس گرفته خواهد شد.</p>
    </div>
  </div>

</section>

<!--  FORM -->
<section class="contact-form">
 <div class="row">
  <div class="col-sm-12">
   <div id="sendstatus"></div>

   <div id="contactform">
    <form method="post" action="#">
      <div class="row">
        <div class="col-sm-6">
          <p><label for="name">نام:*</label> <input type="text" class="form-control" name="name" id="name" tabindex="1" /></p>
          <p><label for="email">آدرس ایمیل:*</label> <input type="text" class="form-control" name="email" id="email" tabindex="2" /></p>
          <p><label for="phone">شماره تلفن:*</label> <input type="text" class="form-control" name="phone" id="phone" tabindex="3" /></p>
        </div>
        <div class="col-sm-6">
          <p><label for="comments">متن شکایت:*</label> <textarea  class="form-control" name="comments" id="comments" tabindex="4"></textarea></p>
        </div>
      </div>
      <p><input name="submit" type="submit" id="submit" class="submit" value="ارسال" tabindex="5" /></p>
    </form>

  </div>
</div>
</div>
</section>
<!--  END OF FORM -->

@endsection
