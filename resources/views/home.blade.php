@extends('layouts.master')

@section('page_header')
  @include('home.slider')
@endsection

@section('page_body')
<!-- FEATURES -->
@include('home.features')
<!-- END OF FEATURES -->

<!-- ABOUT US -->
@include('home.about-us')
<!-- END OF ABOUT US -->

<!-- TESTIMONIALS -->
{{-- @include('home.testimonials') --}}
<!-- END OF TESTIMONIALS -->
@endsection

@section('page_scripts')
<script>
  jQuery(document).ready(function($) {
    "use strict";

    window.odometerOptions = {
      format: 'd'
    };

      //  HEADER SLIDER HOOK
      jQuery('#index-slider').fadeIn(1000);
      $('#index-slider').sliderPro({
        width: '100%',
        height: 760,
        arrows: false,
        buttons: true,
        waitForLayers: true,
        fade: true,
        fadeDuration: 800,
        autoplay: true,
        autoplayDelay: 7500,
        autoplayOnHover: 'none',
        autoScaleLayers: false
      });

          //  TESTIMONIALS CAROUSEL HOOK
          $('#customers-testimonials').owlCarousel({
            rtl:true,
            loop: true,
            center: true,
            items: 3,
            margin: 0,
            autoplay: true,
            autoplayTimeout: 8500,
            smartSpeed: 450,
            responsive: {
              0: {
                items: 1
              },
              768: {
                items: 2
              },
              1170: {
                items: 3
              }
            }
          });

          $('.odometer').waypoint(function() {

            setTimeout(function() {
              $('#odometer1.odometer').html(22);
            }, 500);

            setTimeout(function() {
              $('#odometer2.odometer').html(6102);
            }, 1000);

            setTimeout(function() {
              $('#odometer3.odometer').html(18);
            }, 1500);

            setTimeout(function() {
              $('#odometer4.odometer').html(3144);
            }, 2000);

          }, {
            offset: 800,
            triggerOnce: true
          });

        });
      </script>
      @endsection