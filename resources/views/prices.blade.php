@extends('layouts.master')

@section('page_header')
<div id="page-header" class="about2">
 <div class="title-breadcrumbs">
   <h1>لیست قیمت ها</h1>
   <div class="thebreadcumb">
    <ul class="breadcrumb">
      <li><a href="/">خانه</a></li>
      {{-- <li></li> --}}
      <li class="active">لیست قیمت ها</li>
      {{-- <li class="active">About 2</li> --}}
    </ul>
  </div>
</div>
</div>
@endsection

@section('page_body')
<section class="introtext">
  <div class="row">
    <div class="col-sm-12">
      <h2>از اکنون الی پایان آبان97 کلیه کاربران ثبت نامی از طرح پایه، یکساله به صورت رایگان بهره&zwnj;مند خواهند شد.</h2>
      {{-- <p class="text-center">از اکنون الی پایان آبان97 کلیه کاربران ثبت نامی از طرح پایه، یکساله به صورت رایگان بهره مند خواهند شد.</p> --}}
    </div>
  </div>

</section>

<!--  Pricing Tables -->
<section class="pricingtables">
  <div class="row no-gutter">
   <div class="price-plans">

    <div class="col-sm-3">
      <div class="panel panel-info">
        <div class="panel-heading">
          <h3 class="text-center">رایگان</h3>
        </div>
        <div class="panel-body text-center">
          <h4>رایگان</h4>
        </div>
        <ul class="text-center">
          <li>امکان صدور 20 فاکتور خرید</li>
          <li>امکان صدور 20 فاکتور خرید</li>
          <li>ثبت هزینه نامحدود</li>
          <li>عملیات بانکی نامحدود</li>  
          <li>عملیات انبار نامحدود</li>
        </ul>
        {{-- <div class="panel-footer">
          <a class="btn btn-lg btn-pricetable" href="#">خرید</a>
        </div> --}}
      </div>
    </div>

    <div class="col-sm-3">
      <div class="panel panel-info">
        <div class="panel-heading">
          <h3 class="text-center">پایه یکساله</h3>
        </div>
        <div class="panel-body text-center">
          <h4>{{ number_format(1000000) }}<span>ریال</span></h4>
        </div>
        <ul class="text-center">
          <li>امکان صدور 100 فاکتور خرید</li>
          <li>امکان صدور 100 فاکتور خرید</li>
          <li>ثبت هزینه نامحدود</li>
          <li>عملیات بانکی نامحدود</li>  
          <li>عملیات انبار نامحدود</li>
        </ul>
        {{-- <div class="panel-footer">
          <a class="btn btn-lg btn-pricetable" href="#">خرید</a>
        </div> --}}
      </div>
    </div>

    <div class="col-sm-3 most-popular">
      <div class="panel panel-info">
        <div class="panel-heading">
          <h3 class="text-center">عادی یکساله</h3>
        </div>
        <div class="panel-body text-center">
          <h4>{{ number_format(2000000) }}<span>ریال</span></h4>
        </div>
        <ul class="text-center">
          <li>امکان صدور 200 فاکتور خرید</li>
          <li>امکان صدور 200 فاکتور خرید</li>
          <li>ثبت هزینه نامحدود</li>
          <li>عملیات بانکی نامحدود</li>  
          <li>عملیات انبار نامحدود</li>
        </ul>
        {{-- <div class="panel-footer">
          <a class="btn btn-lg btn-pricetable" href="#">خرید</a>
        </div> --}}
      </div>
    </div>

    <div class="col-sm-3">
      <div class="panel panel-info">
        <div class="panel-heading">
          <h3 class="text-center">ویژه یکساله</h3>
        </div>
        <div class="panel-body text-center">
          <h4>{{ number_format(3000000) }}<span>ریال</span></h4>
        </div>
        <ul class="text-center">
          <li>امکان صدور فاکتور خرید نامحدود</li>
          <li>امکان صدور فاکتور خرید نامحدود</li>
          <li>ثبت هزینه نامحدود</li>
          <li>عملیات بانکی نامحدود</li>  
          <li>عملیات انبار نامحدود</li>
        </ul>
      {{-- <div class="panel-footer">
        <a class="btn btn-lg btn-pricetable" href="#">خرید</a>
      </div> --}}
    </div>
  </div>
</div>
</div>
</section>
<!-- End of Pricing Tables -->

<div class="spacing-65"></div>
@endsection