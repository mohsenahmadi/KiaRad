@extends('layouts.master')

@section('page_header')
<div id="page-header" class="about2">
 <div class="title-breadcrumbs">
   <h1>سوالات پر تکرار</h1>
   <div class="thebreadcumb">
    <ul class="breadcrumb">
      <li><a href="/">خانه</a></li>
      {{-- <li></li> --}}
      <li class="active">سوالات پر تکرار</li>
      {{-- <li class="active">About 2</li> --}}
    </ul>
  </div>
</div>
</div>
@endsection

@section('page_body')
<section class="introtext faq">
  <div class="row">
    {{-- <div class="col-sm-12">
      <h2>ساختگی با تولید سادگی</h2>
      <p class="text-center">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع</p>
      </div> --}}
    </div>

    <div class="spacing-45"></div>

    <div class="row">
      <div class="col-sm-12">
        <div class="shadow-effect">
          <!-- ACCORDION CODE -->
          <div id="accordion" class="style1 panel-group">
            @php
              $items = [
                [
                  'question' => 'کیاراد چه کسب و کارهایی را پاسخگو است؟',
                  'answer' => 'کیاراد بر پایه کمک به صاحبان کسب و کارهایی بنا شده که دانش حسابداری آکادمیک ندارند ولی نیاز به استفاده از نرم افزار حسابداری به زبان ساده دارند. نرم افزاری که برای پوشش خواسته اشخاص حقیقی در شاخه نگهداری حسابها و انجام تکالیف قانونی بنا نهاده شده است.'
                ],
                [
                  'question' => 'نحوه استفاده از خدمات پشتیبانی کیاراد چگونه است؟',
                  'answer' => 'پشتیانی به صورت رایگان می‌باشد و در صورت نیاز می‌توانید از پشتیبانی متنی هر بخش استفاده نمایید. ضمنا از طریق ارسال ایمیل به آدرس support@kiarad.tax می‌توانید پاسخ سوالات خود را در اسرع وقت دریافت نمایید.'
                ],
                [
                  'question' => 'هزینه ثبت‌نام و استفاده از نرم‌افزار چقدر است؟',
                  'answer' => 'هزینه ثبت نام‌رایگان است و استفاده از نرم‌افزار پس از ثبت‌نام تا پایان آبان ماه رایگان می‌باشد. دوره خرید اشتراک جهت استفاده از نرم‌افزار پس از تاریخ اشاره شده یکساله و متناسب با میزان کاربرد شما می‌باشد.'
                ],
                [

                  'question' => 'کدینگ حسابها ثابت است یا در اختیار خودمان می‌باشد؟',
                  'answer' => 'تعریف کلیه طرف حسابها از جمله خریداران و فروشندگان و تعریف کالاها در اختیار شماست. در خصوص هزینه‌ها نیز تعدادی هزینه پر کاربرد تعریف شده که می‌توانید به آنها سرفصل جدید اضافه نمایید.'
                ],
                [
                  'question' => 'نحوه پشتیبانی از اطلاعات ثبت شده من چگونه است؟',
                  'answer' => 'نیاز به هیچگونه ذخیره‌سازی اطلاعات توسط شما نیست، زیرا اطلاعات شما بر روی سرورهای مرکزی کیاراد نگهداری و در هر لحظه آخرین اطلاعات شما در اختیارتان می‌باشد.'
                ]
              ];
            @endphp
            
            @foreach($items as $index => $item)
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"><i class="indicator fa fa-{{ $loop->first ? 'minus' : 'plus' }}-square-o pull-left"></i><a data-toggle="collapse" data-parent="#accordion" href="faq.html#collapse{{ $index }}">{{ $item['question'] }}</a></h4>
              </div>

              <div id="collapse{{ $index }}" class="panel-collapse collapse {{ $loop->first ? 'in' : '' }}">
                <div class="panel-body">
                  <p>{{ $item['answer'] }} </p>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- CALL TO ACTION1 -->
  <section class="calltoaction faq">
    <div class="row">
      <div class="col-sm-12">
      <h2>جواب سوالتان را پیدا نکردید?</h2>
        {{-- <p class="text-center">فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد. </p> --}}

          <a href="{{ route('contact') }}" class="hw-btn">سوال هایتان را اینجا مطرح کنید</a>
        </div>
      </div>
    </section>
    <!-- END OF CALL TO ACTION -->

    <div class="spacing-65"></div>
    @endsection