@extends('layouts.master')

@section('page_header')
<div id="page-header" class="about2">
 <div class="title-breadcrumbs">
   <h1>درباره کیاراد</h1>
   <div class="thebreadcumb">
    <ul class="breadcrumb">
      <li><a href="/">خانه</a></li>
      {{-- <li></li> --}}
      <li class="active">درباره کیاراد</li>
      {{-- <li class="active">About 2</li> --}}
    </ul>
  </div>
</div>
</div>
@endsection

@section('page_body')
<section class="introtext">
  <div class="row">
    <div class="col-sm-12">
      <h2>درباره نرم افزار آنلاین<span> کیاراد </span>بیشتر بدانید</h2>
      <p class="text-center">نرم افزار حسابداری آنلاین کیاراد حاصل تجربه ارزشمند موسسه حسابدار کیاراد در انجام و عرضه خدمات حسابداری و امور مالیاتی به اشخاص و شرکت‌های موفقی است که در کنار اشراف کامل تیم همکاران خود از تمامی ضعف‌ها و نقاط قوت جامعه کسب‌وکار ایران براساس تکنولوژی‌های روز توسعه یافته است. برتری نرم افزار کیاراد به نسبت سایر رقبا، طراحی نرم افزاری آنلاین وکاربردی و در عین حال ساده است که تمامی نیازهای کسب‌وکارهای کوچک متوسط و بزرگ را به خوبی پوشش داده است. این نرم افزار بر اساس تحلیل‌های یک تیم حسابداری قوی که به کار خود اشراف کامل دارند و با به‌کارگیری یک تیم مجرب از افراد موفق در حوزه تولید نرم افزار اجرا گردیده است و این دو عامل می‌تواند اطمینان خاطر شما را در به‌کارگیری از این سرویس برای ما به همراه داشته باشد.</p>
      </div>
    </div>
    <div class="spacing-65"></div>
    <!-- VIDEO BACKGROUND -->
    <div class="row custom-width-90">
      <div class="col-sm-12">
        <div id="about2-video">
          <div class="about2-video-container">
            <div class="videocaption">
             <div class="videocaption-content  wow fadeIn" data-wow-duration="500ms" data-wow-delay="200ms">
              <h2>خدمات حسابداری <br> مشاوره مالی و مالیاتی</h2>
              <hr class="small"/>
              <p>موسسه حسابدار کیاراد به عنوان تیم آنالیز و تحلیل نرم‌افزار حسابداری آنلاین کیاراد با بهره گیری بیش از 13 سال سابقه در ارائه خدمات مالی و حسابداری، مشاوره‌های مالیاتی، پیاده سازی طراحی سیستمهای حسابداری، آموزش حسابداران و پرورش مدیران مالی برجسته و به روز نگهداری دانش خود در بستر حسابداری و امور مالیاتی توانسته کلیه نیازهای حسابداری و تکالیف قانونی اشخاص حقیقی را مطابق آخرین ویرایش ها و اصلاحات پیاده سازی نماید.</p>
            </div>

          </div>
          <video autoplay loop muted poster="images/video-poster.jpg"  class="hidden-xs">
            <source src="video/video2.mp4" type="video/mp4">
            </video>
          </div>
        </div>
      </div>
    </div>
    <!-- END OF VIDEO BACKGROUND -->
  </section>

  <!-- SKILLS -->
  <section class="skills" style="margin-bottom: 60px">
   <div class="row">
   <div class="col-md-6 col-md-push-3">


      <!-- TABS -->
      <div class="tabbable normaltabs tabs-top-horizontal">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab-1" data-toggle="tab">عملیات حسابداری</a></li>
          <li><a href="#tab-2" data-toggle="tab">صدور فاکتور</a></li>
          <li><a href="#tab-3" data-toggle="tab">تکالیف مالیاتی</a></li>
        </ul>
        <div class="tab-content">

         <div class="tab-pane fade in active" id="tab-1">
           <h4>عملیات حسابداری</h4>
           <p class="subtitle">با این نرم افزار می توانید کلیه عملیات روزمره خود را در قالب سرفصلهای هزینه ای و درآمد ثبت نمائید.نگهداری اطلاعات بدهکاران و بستانکاران و چکهای دریافتی و پرداختی به راحتی قابل ردیابی و گزارش گیری می باشد.</p>
         </div>

         <div class="tab-pane fade in" id="tab-2">
           <h4>صدور فاکتور</h4>
           <p class="subtitle">شما میتوانید کالاها و خدمات خود را در قالب فرمتهای استاندارد به فاکتور تبدیل نموده و به شیوه دلخواه برای مشتریان یا شرکا تجاری خود ارسال نمائید.این فاکتورها قابلیت لحاظ تخفیف و مالیات بر ارزش افزوده را دارا می باشند.</p>
         </div>

         <div class="tab-pane fade in" id="tab-3">
           <h4>تکالیف مالیاتی</h4>
           <p class="subtitle">با اجرائی شدن اصلاحات قانون مالیاتهای مستقیم و تاکید دولت بر شفافیت اقتصادی،اشخاص حقیقی موظف به انجام تکالیف قانونی جهت بهره مندی از یکسری معافیت ها می باشند.این سیستم شما را در این زمینه راهنمایی می نماید.</p>
         </div>


       </div>
     </div>
     <!-- END OF TABS -->

   </div>
 </div>
</section>

<!-- END OF SKILLS -->
@endsection
