@extends('layouts.master')

@section('page_header')
<div id="page-header" class="about2">
 <div class="title-breadcrumbs">
 <h1>تماس با ما</h1>
   <div class="thebreadcumb">
    <ul class="breadcrumb">
      <li><a href="/">خانه</a></li>
      {{-- <li></li> --}}
      <li class="active">تماس با ما</li>
      {{-- <li class="active">About 2</li> --}}
    </ul>
  </div>
</div>
</div>
@endsection

@section('page_body')
<!--  ICON BOXES -->
<div class="about1-numbers contact">
  <div class="row">

    <div class="col-sm-4">
      <div class="shadow-effect">
        <div class="row">
          <div class="col-sm-12 col-md-3">
            <div class="number lightgray"><span class="typcn typcn-mail"></span></div>
          </div>
          <div class="col-sm-12 col-md-9">
            <h4>فرم نظرات، پیشنهادات و انتقادات</h4>
            <p>انتقادات، پیشنهادات و نظرات خود را به وسیله فرم زیر با ما مطرح کنید</p>
            <span><a href="contact.html"></a></span>
          </div>
        </div>
      </div>
    </div>

    <div class="col-sm-4">
     <div class="shadow-effect">
      <div class="row">
        <div class="col-sm-12 col-md-3">
          <div class="number blue"><span class="typcn typcn-heart"></span></div>
        </div>
        <div class="col-sm-12 col-md-9">
          <h4>فرم درخواست راهنمایی</h4>
          <p>با استفاده از فرم زیر، درخواست‌های پشتیبانی و راهنمایی‌های لازم را از ما دریافت کنید.</p>
          <span><a href="contact.html">SUPPORT@KIARAD.COM</a></span>
        </div>
      </div>
    </div>
  </div>

  <div class="col-sm-4">
   <div class="shadow-effect">
    <div class="row">
      <div class="col-sm-12 col-md-3">
        <div class="number green"><span class="typcn typcn-location"></span></div>
      </div>
      <div class="col-sm-12 col-md-9">
        <h4>آدرس دفتر مرکزی</h4>
        <p>تهران، میدان هفت حوض<br/>خیابان جانبازان شرقی<br/>جنب بانک ملی، پلاک ۴۸۴، طبقه اول </p>
        <span>شماره تماس:۰۲۱۷۷۹۴۹۴۰۷</span>
      </div>
    </div>
  </div>
</div>
</div>

</div>
<!--  END OF ICON BOXES -->

<!--  FORM -->
<section class="contact-form">
  <div class="row">
    <div class="col-sm-12">
     <h2>فرم تماس</h2>
     <hr class="small"/>
   </div>
 </div>
 <div class="row">
  <div class="col-sm-12">
   <div id="sendstatus"></div>

   <div id="contactform">
    <form method="post" action="#">
      <div class="row">
        <div class="col-sm-6">
          <p><label for="name">نام:*</label> <input type="text" class="form-control" name="name" id="name" tabindex="1" /></p>
          <p><label for="email">آدرس ایمیل:*</label> <input type="text" class="form-control" name="email" id="email" tabindex="2" /></p>
          <p><label for="phone">شماره تلفن:*</label> <input type="text" class="form-control" name="phone" id="phone" tabindex="3" /></p>
        </div>
        <div class="col-sm-6">
          <p><label for="comments">متن پیام:*</label> <textarea  class="form-control" name="comments" id="comments" tabindex="4"></textarea></p>
        </div>
      </div>
      <p><input name="submit" type="submit" id="submit" class="submit" value="ارسال" tabindex="5" /></p>
    </form>

  </div>
</div>
</div>
</section>
<!--  END OF FORM -->

<div id="map_wrapper">
  <div id="map_canvas" class="mapping"></div>
</div>
@endsection

@section('page_scripts')
<script>
  function initMap() {
    var mapDiv = document.getElementById('map_canvas');
    var myLatLng = {lat:35.729261, lng:51.501950};
    var map = new google.maps.Map(mapDiv, {
     center: myLatLng,
     zoom: 15,
     scrollwheel: false,
     draggable: false,
     mapTypeControl: true,
     mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
      position: google.maps.ControlPosition.TOP_RIGHT
    },
    zoomControl: true,
    zoomControlOptions: {
      position: google.maps.ControlPosition.LEFT_TOP
    },
    scaleControl: true,
    streetViewControl: true,
    streetViewControlOptions: {
      position: google.maps.ControlPosition.LEFT_TOP
    }

  });
    var image = 'images/map-pin.png';
    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      icon: image
    });
    marker.addListener('click', function() {
      infowindow.open(map, marker);
    });


    var contentString = '<div id="content">'+
    '<h4>Good Growth</h4>'+
    '<div id="bodyContent">'+
    '<p>Completely synthesize principle-centered information after ethical communities.</p></div>';

    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });

    map.set('styles',[{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"administrative","elementType":"labels","stylers":[{"saturation":"-100"}]},{"featureType":"administrative","elementType":"labels.text","stylers":[{"gamma":"0.75"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.fill","stylers":[{"lightness":"-37"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f9f9f9"}]},{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"saturation":"-100"},{"lightness":"40"},{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"labels.text.fill","stylers":[{"saturation":"-100"},{"lightness":"-37"}]},{"featureType":"landscape.natural","elementType":"labels.text.stroke","stylers":[{"saturation":"-100"},{"lightness":"100"},{"weight":"2"}]},{"featureType":"landscape.natural","elementType":"labels.icon","stylers":[{"saturation":"-100"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"saturation":"-100"},{"lightness":"80"}]},{"featureType":"poi","elementType":"labels","stylers":[{"saturation":"-100"},{"lightness":"0"}]},{"featureType":"poi.attraction","elementType":"geometry","stylers":[{"lightness":"-4"},{"saturation":"-100"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#c5dac6"},{"visibility":"on"},{"saturation":"-95"},{"lightness":"62"}]},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},{"featureType":"road","elementType":"all","stylers":[{"lightness":20}]},{"featureType":"road","elementType":"labels","stylers":[{"saturation":"-100"},{"gamma":"1.00"}]},{"featureType":"road","elementType":"labels.text","stylers":[{"gamma":"0.50"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"saturation":"-100"},{"gamma":"0.50"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#c5c6c6"},{"saturation":"-100"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":"-13"}]},{"featureType":"road.highway","elementType":"labels.icon","stylers":[{"lightness":"0"},{"gamma":"1.09"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#e4d7c6"},{"saturation":"-100"},{"lightness":"47"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"lightness":"-12"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"saturation":"-100"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#fbfaf7"},{"lightness":"77"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"lightness":"-5"},{"saturation":"-100"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"saturation":"-100"},{"lightness":"-15"}]},{"featureType":"transit.station.airport","elementType":"geometry","stylers":[{"lightness":"47"},{"saturation":"-100"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"color":"#acbcc9"}]},{"featureType":"water","elementType":"geometry","stylers":[{"saturation":"53"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"lightness":"-42"},{"saturation":"17"}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"lightness":"61"}]}]);

  }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBa0XKgzJB_erM0hBHbdrjaFB_KQMuRoDY&callback=initMap" async defer></script>
@endsection
