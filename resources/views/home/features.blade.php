<section class="introtext">
  <div class="row">
    <div class="col-sm-12">
      <h2>کیاراد چگونه در <span>حسابداری</span> به شما کمک میکند</h2>
      <p class="text-center">طراحی و توسعه نرم‌افزار آنلاین کیاراد اینگونه است که افراد بدون تخصص و دانش حسابداری هم می‌توانند به راحتی با آن ارتباط برقرار کنند. تمامی بخش‌هایی از سیستم که نیاز به توضیح دارد، راهنمای برنامه به صورت کامل در اختیار شما قرار گرفته است. کیاراد با پشتیبانی یک موسسه حسابداری پاسخگوی پرسش‌های شماست و می‌تواند در تمامی مراحل شما را به رایگان راهنمایی کند. </p>
      </div>
    </div>

    <div class="row spacing-45">
      <div class="col-sm-12">
        <div class="block-grid-sm-4 block-grid-xs-1">

          <!-- FEATURE 1 -->
          <div class="block-grid-item wow fadeInLeft" data-wow-duration="300ms" data-wow-delay="300ms">
            <div class="shadow-effect">
              <span class="typcn typcn-chart-area-outline"></span>
              <h3>حسابداری غیرمتمرکز</h3>
              <p>در هر لحظه می‌توانید عملیات حسابداری خود را انجام دهید یا از اطلاعات ثبت شده استفاده کنید.</p>
              {{-- <a class="btn btn-default" href="index.html#" role="button">Read More</a> --}}
            </div>
          </div>
          <!-- END OF FEATURE 1 -->
          <!-- FEATURE 2 -->
          <div class="block-grid-item wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="400ms">
            <div class="shadow-effect">
              <span class="typcn typcn-chart-bar"></span>
              <h3>سطوح دسترسی مستقل</h3>
              <p>برای کاربران خود دسترسی‌های متفاوت ایجاد کنید تا با خیال راحت وظایف خود را انجام دهند.</p>
              {{-- <a class="btn btn-default" href="index.html#" role="button">Read More</a> --}}
            </div>
          </div>
          <!-- END OF FEATURE 2 -->
          <!-- FEATURE 3 -->
          <div class="block-grid-item wow fadeInRight" data-wow-duration="600ms" data-wow-delay="600ms">
            <div class="shadow-effect">
             <span class="typcn typcn-chart-line-outline"></span>
             <h3>صدور فاکتور</h3>
             <p>با ثبت آسان اطلاعات در فاکتورهای خود، آن‌ها را برای مشتریان خود ارسال و مراحل دریافت وجوه خود را پیگیری کنید.</p>
             {{-- <a class="btn btn-default" href="index.html#" role="button">Read More</a> --}}
           </div>
         </div>
         <!-- END OF FEATURE 3 -->
         <!--FEATURE 4 -->
         <div class="block-grid-item wow fadeInRight" data-wow-duration="750ms" data-wow-delay="800ms">
          <div class="shadow-effect">
           <span class="typcn typcn-chart-pie-outline"></span>
           <h3>انبارداری آسان</h3>
           <p>تعریف کالاها و خدمات متناسب با کسب‌وکار خود را تجربه و هر زمان کنترل موجودی مقداری و ریالی انبار خود را همراه داشته باشید.</p>
           {{-- <a class="btn btn-default" href="index.html#" role="button">Read More</a> --}}
         </div>
       </div>
       <!-- END OF FEATURE 4 -->
     </div>
   </div>
 </div>
</section>
