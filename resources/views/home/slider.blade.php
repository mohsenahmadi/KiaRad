<div class="slider-pro" id="index-slider">
  <div class="sp-slides">
    <!-- Slide 1 -->
    <div class="sp-slide">
      <img class="sp-image" src="images/1.jpg" alt=""/>
      <div class="hw-the-slider-content">
        <div class="row">
          <div class="col-sm-12">
            <div class="hw-the-slider-container">
              <div class="hw-the-slider-captions sp-layer" data-show-transition="left" data-show-delay="800">
                <h2>نرم افزار حسابداری آنلاین کیاراد</h2>
                <p>در هر زمان و هر کجا که هستید به اطلاعات حسابداری خود دسترسی داشته باشید</p>
                <a href="{{ route('services') }}" class="hw-btn">بیشتر بدانید</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End of Slide 1 -->
    <!-- Slide 2 -->
    <div class="sp-slide">
     <img class="sp-image" src="images/2.jpg" alt=""/>
     <div class="hw-the-slider-content">
      <div class="row">
        <div class="col-sm-12">
          <div class="hw-the-slider-container">
            <div class="hw-the-slider-captions sp-layer" data-show-transition="down" data-show-delay="800">
              <h2>محافظت از اطلاعات حسابداری شما</h2>
              <p>محافظت از اطلاعات شخصی و سازمانی شما، برای کیاراد مهم است.</p>
              <a href="{{ route('about') }}" class="hw-btn">بیشتر بدانید</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End of Slide 2 -->
  <!-- Slide 3 -->
  <div class="sp-slide">
   <img class="sp-image" src="images/3.jpg" alt=""/>
   <div class="hw-the-slider-content">
    <div class="row">
      <div class="col-sm-12">
        <div class="hw-the-slider-container">
          <div class="hw-the-slider-captions sp-layer" data-show-transition="right" data-show-delay="800">
            <h2>حذف هزینه‌های نصب و پشتیبانی</h2>
            <p>برای راه‌اندازی نرم افزار حسابداری کیاراد، نیازی به پرداخت هزینه‌های نصب، راه‌اندازی و پشتیبانی نخواهید بود. ما این‌کار رابرای شما به رایگان انجام خواهیم داد.</p>
            <a href="{{ route('account') }}" class="hw-btn">شروع کنید</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End of Slide 3 -->
</div>
</div>
