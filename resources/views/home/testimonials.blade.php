<section class="testimonials">

  <div class="row">
    <div class="col-sm-12">
      <h2>گفته‌های <span>مشتریان خوب ما</span></h2>
      <p class="text-center">برخی از گفته‌های مشتریانی که سال‌ها از خدمات و سرویس‌های ما استفاده کرده‌اند.</p>
    </div>
  </div>

  <div class="row full-width no-gutter-all">
    <div class="col-sm-12">
      <div id="customers-testimonials" class="owl-carousel">

        <!--TESTIMONIAL 1 -->
        <div class="item">
          <div class="shadow-effect">
            <img class="img-circle" src="images/taxi-bisim.jpg" alt="">
            <p>تاکسی بی‌سیم تهران؛ به عنوان اولین سامانه تاکسیرانی خصوصی کشور سال‌هاست که از خدمات کیاراد استفاده کرده و از سرویس‌ها و خدمات این موسسه رضایت کامل دارد.</p>
          </div>
          <div class="testimonial-name">تاکسی بی‌سیم تهران</div>
        </div>
        <!--END OF TESTIMONIAL 1 -->
        <!--TESTIMONIAL 2 -->
        <div class="item">
          <div class="shadow-effect">
            <img class="img-circle" src="images/partosam.jpg" alt="">
            <p>شرکت آینده سازان پتروسام، از مدیران و همکاران موسسه حسابدار کیاراد، بابت تمامی پیگیری‌های انجام شده تشکر می‌کند.</p>
          </div>
          <div class="testimonial-name">آینده سازان پتروسام</div>
        </div>
        <!--END OF TESTIMONIAL 2 -->
        <!--TESTIMONIAL 3 -->
        <div class="item">
          <div class="shadow-effect">
            <img class="img-circle" src="images/atbin.jpg" alt="">
            <p>رفتار حرفه‌ای کارشناسان در پاسخگویی به تمامی ابهامات و راهنمایی‌های موسسه حسابدار کیاراد از مزایای رقابتی این مجموعه نسبت به سایرین است.</p>
          </div>
          <div class="testimonial-name">آذر فولاد ایرانیان</div>
        </div>
        <!--END OF TESTIMONIAL 3 -->
        <!--TESTIMONIAL 4 -->
        <div class="item">
          <div class="shadow-effect">
            <img class="img-circle" src="images/boghche.png" alt="">
            <p>دانش و آگاهی کامل از قوانین و مقررات مالی و نگاه به‌روز مدیران موسسه حسابدار کیاراد در حمایت از استارتاپ‌ها و کسب‌وکارهای نوپا همواره پشتیبان ما بوده است.</p>
          </div>
          <div class="testimonial-name">بقچه، اولین سرویس اینترنتی سفارش نان</div>
        </div>
        <!--END OF TESTIMONIAL 4 -->
        <!--TESTIMONIAL 5 -->
        <div class="item">
          <div class="shadow-effect">
            <img class="img-circle" src="images/testimonial5.jpg" alt="">
            <p>مدیران کیاراد، علاوه بر فعالیت حرفه‌ای با نگاه برد-برد به روابط تجاری همواره به دنبال منافع مشترک کسب‌وکارها هستند. </p>
          </div>
          <div class="testimonial-name">دانش مدیریت چابک</div>
        </div>
        <!--END OF TESTIMONIAL 5 -->
      </div>
    </div>
  </div>
</section>
