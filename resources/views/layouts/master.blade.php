  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>کیاراد، نرم افزار حسابداری آنلاین</title>
    <link rel="shortcut icon" href="images/favicon.png" />
    <!-- GOOGLE WEB FONTS -->
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,100,300,700,900&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- END OF GOOGLE WEB FONTS -->

    <!-- BOOTSTRAP & STYLES -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-theme.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-rtl.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/block_grid_bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/typicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/odometer-theme-default.css') }}" rel="stylesheet">
     <link href="{{ asset('css/slider-pro.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('css/owl.theme.default.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/slicknav.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/iransans-font.css') }}" rel="stylesheet">

    @yield('lib_styles')

    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    @yield('page_styles')
    <!-- END OF BOOTSTRAP & STYLES -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-45545415-14"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-45545415-14');
    </script>

  </head>

  <body>
    <!-- HEADER -->

    <div id="hw-hero">
      @include('partials.header')

      <!-- SLIDER -->
      @yield('page_header')
      <!-- END OF SLIDER -->
   </div>

    @yield('page_body')

    <!-- FOOTER -->
    @include('partials.footer')
    <!-- END OF FOOTER -->

    <div class="copyright">
      <div class="row">
        <div class="col-sm-12">
          <p class="text-center">تمامی حقوق مادی و معنوی این وب سایت، متعلق به موسسه حسابدار کیاراد است.</p>
        </div>
      </div>
    </div>

    <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

    <!-- JAVASCRIPT FILES -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/jquery.hoverIntent.js') }}"></script>
    <script src="{{ asset('js/superfish.min.js') }}"></script>
    <script src="{{ asset('js/jquery.sliderPro.min.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/odometer.min.js') }}"></script>
    <script src="{{ asset('js/waypoints.min.js') }}"></script>
    <script src="{{ asset('js/jquery.slicknav.min.js') }}"></script>
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <script src="{{ asset('js/retina.min.js') }}"></script>

    @yield('lib_scripts')

    <script src="{{ asset('js/custom.js') }}"></script>
    <!-- END OF JAVASCRIPT FILES -->


    @yield('page_scripts')
  </body>

  </html>
