@extends('layouts.master')

@section('page_header')
<div id="page-header" class="about2">
 <div class="title-breadcrumbs">
   <h1>خدمات کیاراد</h1>
   <div class="thebreadcumb">
    <ul class="breadcrumb">
      <li><a href="/">خانه</a></li>
      {{-- <li></li> --}}
      <li class="active">خدمات کیاراد</li>
      {{-- <li class="active">About 2</li> --}}
    </ul>
  </div>
</div>
</div>
@endsection

@section('page_body')
<section class="introtext">
  <div class="row">
    <div class="col-sm-12">
      <h2>قابلیت های نرم افزار کیاراد</h2>
      <p class="text-center">نرم افزار حسابداری آنلاین کیاراد با امکانات کامل و کاربردی شما را در ثبت اسناد خود در هر زمان و هر مکان با هر دستگاه متصل به اینترنت یاری می نماید.بخشهای مربوط به بازرگانی (خرید و فروش)،خزانه داری (دریافت و پرداخت)و انبار تولید کننده اطلاعات جهت گزارشات درون و برون سازمانی می باشند که به صورت یکپارچه در دسترستان می باشد.</p>
        <hr class="small"/>
      </div>
    </div>
  </section>

  <!-- SERVICE 1 -->
  <section class="services3-list">
    <div class="row">
      <div class="col-md-6">
        <div class="text-padding  wow zoomIn" data-wow-duration="500ms" data-wow-delay="300ms">
          <span class="typcn typcn-chart-line"></span>
          <h3>خرید و فروش(بازرگانی)</h3>
          <ul>
            <li> فاکتور خرید کالا/برگشت از خرید </li>
            <li> فاکتور فروش کالا و خدمات/برگشت از فروش </li>
            <li> ارسال فاکتور از طریق ایمیل </li>
            <li> صدور چاپ فاکتور با فرمت قانونی </li>
          </ul>
        </div>
      </div>
      <div class="col-md-6">
        <div class="service-image  wow fadeInUp" data-wow-duration="500ms" data-wow-delay="700ms"><img src="{{ asset('images/services-5-image.png') }}" alt=""/></div>
      </div>
    </div>
  </section>
  <!-- END OF SERVICE 1 -->

  <!-- SERVICE 2 -->
  <section class="services3-list gray">
    <div class="row">
     <div class="col-md-6">
       <div class="service-image  wow fadeInUp" data-wow-duration="500ms" data-wow-delay="700ms"><img src="{{ asset('images/services-2-image.png') }}" alt=""/></div>
     </div>

     <div class="col-md-6">
      <div class="text-padding  wow zoomIn" data-wow-duration="500ms" data-wow-delay="300ms">
        <span class="typcn typcn-chart-area"></span>
        <h3>دریافت و پرداخت (خزانه داری)</h3>
        <ul>
          <li> دریافت و پرداخت وجه نقد </li>
          <li> دریافت و پرداخت چک </li>
          <li> کنترل وضعیت چک ها </li>
        </ul>
      </div>
    </div>
  </div>
</section>
<!-- END OF SERVICE 2 -->

<!-- SERVICE 3 -->
<section class="services3-list">
  <div class="row">
    <div class="col-md-6">
      <div class="text-padding  wow zoomIn" data-wow-duration="500ms" data-wow-delay="300ms">
        <span class="typcn typcn-waves"></span>
        <h3>گزارشات</h3>
        <ul>
          <li> تراز آزمایشی </li>
          <li> صورتحساب سود و زیان </li>
          <li> گزارش موجودی کالا </li>
          <li> گزارش فروش به تفکیک کالا </li>
          <li> گزارش ماده 169(گزارش فصلی) </li>
          <li> گزارش ارزش افزوده </li>
          <li> گزارشات نموداری و گرافیکی </li>
        </ul>
      </div>
    </div>
    <div class="col-md-6">
      <div class="service-image  wow fadeInUp" data-wow-duration="500ms" data-wow-delay="700ms"><img src="{{ asset('images/services-3-image.png') }}" alt=""/></div>
    </div>
  </div>
</section>
<!-- END OF SERVICE 3 -->

<!-- SERVICE 4 -->
<section class="services3-list gray">
  <div class="row">
   <div class="col-md-6">
     <div class="service-image wow fadeInUp" data-wow-duration="500ms" data-wow-delay="700ms"><img src="{{ asset('images/services-4-image.png') }}" alt=""/></div>
   </div>

   <div class="col-md-6">
    <div class="text-padding  wow zoomIn" data-wow-duration="500ms" data-wow-delay="300ms">
      <span class="typcn typcn-adjust-brightness"></span>
      <h3>امکانات دیگر</h3>
      <ul>
        <li> امکان تعریف کدینگ حساب دلخواه </li>
        <li> امکان مدیریت کاربران سیستم و تعیین سطوح دسترسی </li>
        <li> امکان تعریف کالا و کد کالا </li>
        <li> امکان تعریف انبار متعدد </li>
      </ul>
    </div>
  </div>
</div>
</section>
<!-- END OF SERVICE 4 -->

<div class="spacing-65"></div>
@endsection