<?php

use App\Tenant\Models\SpendItem;

use Illuminate\Database\Seeder;

class SpendItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $spend_items = [
            ['code' => 100, 'name' => 'هزینه پذیرایی'],  
            ['code' => 101, 'name' => 'هزینه پیک و ایاب و ذهاب'], 
            ['code' => 102, 'name' => 'هزینه قبوض'], 
            ['code' => 103, 'name' => 'هزینه ملزومات مصرفی'], 
            ['code' => 104, 'name' => 'هزینه اجاره و شارژ'], 
            ['code' => 105, 'name' => 'هزینه تعمیر و نگهداری'], 
            ['code' => 106, 'name' => 'هزینه آگهی و تبلیغات'], 
            ['code' => 107, 'name' => 'هزینه حقوق و دستمزد'], 
            ['code' => 108, 'name' => 'هزینه حق بیمه'], 
            ['code' => 109, 'name' => 'هزینه عیدی و سنوات'], 
            ['code' => 110, 'name' => 'هزینه کارمزد بانکی']];

        foreach($spend_items as $item) {
        	$spend_item = SpendItem::where('code', $item['code'])->first();
        	if($spend_item) $spend_item->delete();

        	$spend_item = SpendItem::create($item);
        	$spend_item->default = true;
        	$spend_item->save();
        }
    }
}
