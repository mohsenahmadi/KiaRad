<?php

use App\Tenant\Models\Bank;

use Illuminate\Database\Seeder;

class BanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$banks = ['آینده', 'اقتصاد نوین', 'انصار', 'ایران زمین', 'پارسیان', 'پاسارگاد', 'تجارت', 'توسعه تعاون', 'توسعه صادرات ایران', 'حکمت ایرانیان', 'خاورمیانه', 'دی', 'رفاه', 'کارگران', 'سامان', 'سپه', 'سرمایه', 'سینا', 'شهر', 'صادرات ایران', 'صنعت و معدن', 'قرض الحسنه رسالت', 'قرض الحسنه مهر ایران', 'قوامین', 'گردشگری', 'مسکن', 'ملت', 'ملی ایران', 'مهراقتصاد', 'کارآفرین', 'کشاورزی', 'پست بانک ایران'];

            if(count($banks) >= Bank::count()) {
                $index = -1;
                foreach(Bank::all() as $index => $bank) {
                    $bank->update(['name' => $banks[$index]]);
                }

                foreach (array_slice($banks, $index+1) as $bank) {
    	           Bank::create(['name' => $bank]);
                }
            }
    }
}
