<?php

use Illuminate\Database\Seeder;

use App\Tenant\Models\Type;

class TypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Type::create(['name' => 'person', 'label' => 'حقیقی']);
    	Type::create(['name' => 'company', 'label' => 'حقوقی']);
    }
}
