<?php

use Illuminate\Database\Seeder;

use App\Tenant\Models\Warehouse;

class WarehouseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Warehouse::create(['name' => 'اصلی']);
    	Warehouse::create(['name' => 'خدمات']);

    }
}
