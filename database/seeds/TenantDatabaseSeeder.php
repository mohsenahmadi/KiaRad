<?php

use Illuminate\Database\Seeder;

class TenantDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TypeTableSeeder::class);
        $this->call(RolesPermissionsTableSeeder::class);
        $this->call(WarehouseTableSeeder::class);
        $this->call(BanksTableSeeder::class);
        $this->call(SpendItemsTableSeeder::class);
    }
}
