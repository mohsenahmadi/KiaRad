<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFactorTotalsToBuyFactorsTabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('buy_factors', function (Blueprint $table) {
            $table->integer('price')->unsigned()->default(0)->after('description');
            $table->integer('discount')->unsigned()->default(0)->after('price');
            $table->integer('tax')->unsigned()->default(0)->after('discount');
            $table->integer('total')->unsigned()->default(0)->after('tax');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('buy_factors', function (Blueprint $table) {
            $table->dropColumn('price');
            $table->dropColumn('discount');
            $table->dropColumn('tax');
            $table->dropColumn('total');
        });
    }
}
