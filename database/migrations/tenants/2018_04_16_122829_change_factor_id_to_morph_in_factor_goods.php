<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFactorIdToMorphInFactorGoods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factor_goods', function (Blueprint $table) {
           $table->increments('id');
           $table->morphs('factor');
           $table->integer('good_id')->unsigned();
           $table->foreign('good_id')->references('id')->on('goods')->onDelete('cascade');
           $table->integer('price')->unsigned();
           $table->integer('tax')->unsigned()->default(0)->nullable();
           $table->integer('discount')->unsigned()->default(0)->nullable();
           $table->integer('quantity')->unsigned()->default(1)->nullable();
           $table->text('description')->nullable();
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factor_goods');;
    }
}
