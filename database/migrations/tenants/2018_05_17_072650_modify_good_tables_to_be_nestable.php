<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyGoodTablesToBeNestable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('goods', function (Blueprint $table) {
            $table->dropColumn('tax');
        });
        
        Schema::table('goods', function (Blueprint $table) {
            $table->integer('tax')->default(0)->nullable()->before('unit');
            
            $table->unsignedInteger('_lft')->default(0)->after('id');
            $table->unsignedInteger('_rgt')->default(0)->after('id');
            $table->unsignedInteger('parent_id')->nullable()->after('id');

            $table->index(['_lft', '_rgt', 'parent_id']);

            $table->boolean('is_cat')->default(true)->before('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('goods', function (Blueprint $table) {
            $table->dropNestedSet();

            $table->dropColumn('is_cat');
        });
    }
}
