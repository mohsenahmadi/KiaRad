<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSuggestionSellPriceToGoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('goods', function (Blueprint $table) {
            $table->integer('suggestion_sell_price')->unsigned()->nullable()->after('suggestion_price');
            $table->renameColumn('suggestion_price', 'suggestion_buy_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('goods', function (Blueprint $table) {
            $table->dropColumn('suggestion_sell_price');
            $table->renameColumn('suggestion_buy_price', 'suggestion_price');
        });
    }
}
