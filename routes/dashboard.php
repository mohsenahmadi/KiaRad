<?php

Route::as('home')->get('/', 'HomeController@index');
Route::as('plans')->get('/plans', 'PaymentsController@plans');
Route::as('payments.')->prefix('payments')->group(function() {
	Route::as('pay')->post('pay', 'PaymentsController@pay');
	Route::as('verify')->any('{payment}/verify', 'PaymentsController@verify');
	
	Route::as('verify.result')->any('{status}/verify/{payment}', 'PaymentsController@result');
});

Route::as('account.')->prefix('account')->group(function() {
	Route::as('subscribe')->post('subscribe')->uses('AccountController@subscribe');
	Route::as('subscription.verify')->any('subscription/{payment}/verify')->uses('AccountController@subscriptionVerify');
	Route::as('subscription.result')->get('subscription/{status}/verify/{payment}')->uses('AccountController@subscriptionResult');
});

Route::as('incomes.')->prefix('incomes')->namespace('\Incomes')->group(function() {
	Route::as('banks.report')->get('banks/report')->uses('BanksController@report');
	Route::as('chests.report')->get('chests/report')->uses('ChestsController@report');
	Route::as('checks.report')->get('checks/report')->uses('ChecksController@report');
	
	Route::resource('banks', 'BanksController', ['parameters' => ['bank' => 'turnover']]);
	Route::resource('chests', 'ChestsController', ['parameters' => ['chest' => 'turnover']]);
	Route::as('checks.manage')->get('checks/manage', 'ChecksController@manage');
	Route::as('checks.pass')->get('checks/{turnover}/pass', 'ChecksController@pass');
	Route::as('checks.pass')->put('checks/{turnover}/pass', 'ChecksController@passStore');
	Route::as('checks.spend')->get('checks/{turnover}/spend', 'ChecksController@spend');
	Route::as('checks.spend')->put('checks/{turnover}/spend', 'ChecksController@spendStore');
	Route::resource('checks', 'ChecksController', ['parameters' => ['check' => 'turnover']]);
});
Route::as('outcomes.')->prefix('outcomes')->namespace('\Outcomes')->group(function() {
	Route::resource('banks', 'BanksController', ['parameters' => ['bank' => 'turnover']]);
	Route::resource('chests', 'ChestsController', ['parameters' => ['chest' => 'turnover']]);
	Route::resource('checks', 'ChecksController', ['parameters' => ['checkt' => 'turnover']]);
});

// Route::as('.incomes.bank')->get('incomes/bank/create', 'IncomesController@bankCreate');
// Route::as('.incomes.chest')->get('incomes/chest/create', 'IncomesController@chestCreate');
// Route::as('.incomes.check')->get('incomes/check/create', 'IncomesController@checkCreate');
// Route::as('.outcomes.bank')->get('outcomes/bank/create', 'OutcomesController@bankCreate');
// Route::as('.outcomes.chest')->get('outcomes/chest/create', 'OutcomesController@chestCreate');
// Route::as('.outcomes.check')->get('outcomes/check/create', 'OutcomesController@checkCreate');
// Route::get('outcomes/bank/create', 'OutcomesController@bankCreate');
// Route::get('outcomes/chest/create', 'OutcomesController@chestCreate');
// Route::resource('incomes', 'IncomesController');
// Route::resource('outcomes', 'OutcomesController');

Route::resource('factors/sell', 'SellFactorsController', ['parameters' => ['sell' => 'factor']]);
Route::resource('factors/buy', 'BuyFactorsController', ['parameters' => ['buy' => 'factor']]);
Route::resource('factors', 'FactorsController', ['except' => 'show']);
Route::resource('expenses', 'ExpensesController');

Route::as('spend_items.search')->get('spend_items/search')->uses('SpendItemsController@search');
Route::resource('spend_items', 'SpendItemsController');
Route::resource('chests', 'ChestsController', ['except' => 'show']);
Route::resource('checks', 'ChecksController', ['except' => 'show']);
Route::resource('bank_accounts', 'BankAccountsController', ['except' => 'show']);

Route::as('goods.search')->get('goods/search')->uses('GoodsController@search');
Route::as('goods.childs')->get('goods/{good}/childs')->uses('GoodsController@index');
Route::as('goods.trades')->get('goods/{good}/trades')->uses('GoodsController@trades');
Route::resource('goods', 'GoodsController');

Route::as('warehouses.goods')->get('warehouses/{warehouse}/goods')->uses('WarehousesController@goods');
Route::resource('warehouses', 'WarehousesController', ['except' => 'show']);

Route::as('customers.search')->get('customers/search')->uses('CustomersController@search');
Route::resource('customers', 'CustomersController');