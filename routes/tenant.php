<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	// dd(route('dashboard.home'));
	return redirect()->route('dashboard.home');
    // return view('tenant::welcome');
    return view('welcome2');
});

Route::prefix('api/v1')->group(function () {
    Route::get('test', function () {
		dump(Request::route('account'), url()->current(), explode('//', Request::root())[1]);
        dd(App\Tenant\Models\User::all(), 'tenant');
    });
});
Route::middleware('tenancy.enforce')->group(function(){
	Auth::routes();
});

Route::get('/home', 'HomeController@index')->name('home');
