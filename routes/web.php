<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {return view('home'); });
Route::get('about', ['as' => 'about', 'uses' => function () {return view('about'); }]);
Route::get('services', ['as' => 'services', 'uses' => function () {return view('services'); }]);
Route::get('account', ['as' => 'account', 'uses' => function () {return view('faq'); }]);
Route::get('faq', ['as' => 'faq', 'uses' => function () {return view('faq'); }]);
Route::get('contact', ['as' => 'contact', 'uses' => function () {return view('contact'); }]);
Route::get('prices', ['as' => 'prices', 'uses' => function () {return view('prices'); }]);
Route::get('privacy', ['as' => 'privacy', 'uses' => function () {return view('privacy'); }]);
Route::get('complains', ['as' => 'complains', 'uses' => function () {return view('complains'); }]);
// Route::get('account', ['as' => 'account', 'uses' => function () {return view('home'); }]);

// Route::get('/', function () {
// 	dump(Request::route('account'));
//     return view('welcome');
// });

Route::prefix('api/v1')->group(function () {
    Route::get('test', function () {
        dump(Request::route('account'), url()->current(), explode('//', Request::root())[1]);
        dd('system', App\System\Models\User::all());
    });
});

Auth::routes();

Route::get('/home', 'SystemHomeController@index')->name('home');
