function setTypeaHead(elem, bloodhound) {
	$(elem).typeahead({
		hint: true,
		highlight: true,
		minLength: 1,
	},
	{
		limit: 10,
		displayKey: 'name',
		templates: {
			suggestion: function(obj){
				return "<div>"+obj.code+" - "+obj.name+"</div>";
			},
			notFound: function() {
				return 'نتیجه ای یافت نشد';
			},
			pending: function() {
				return 'در حال جستجو';
			}
		},
		source: bloodhound
	});
}