$(function() {
	$("#date-picker").pDatepicker({
    	'format': 'dddd DD/MMMM/YYYY',
    	'altField': '#issued-at',
    	'altFieldFormatter': function(unix) {
    		var date = new persianDate(unix);
    		var gregorian = date.toCalendar('gregorian');
    		var year = gregorian.year(),
    			month = gregorian.month(),
    			day = gregorian.date();

    			return year+'-'+month+'-'+day;
    	},
    	// minDate: new persianDate(new Date(limit.min)).valueOf(),
    	// maxDate: new persianDate(new Date(limit.max)).valueOf()
    });

	var customerSerarch = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.whitespace,
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: {
			url: customersSearchUrl+'?q=%QUERY',
			wildcard: '%QUERY'
		}
	});

	setTypeaHead($('#customer-typehead'), customerSerarch);

	$('#customer-typehead').bind('typeahead:select', function(e, customer) {		
		setCustomer(customer);
	});	

	if(customerFetch) {
		$.ajax({
			url: customerFetch,
		})
		.done(function(customer) {
			$('#customer-typehead').val(customer.name);
			setCustomer(customer);
		});		
	}

	$('#factor-official').on('change', function() {
		var input = $('#factor-number');
		var number = input.val();

		$('#factor-number').val(input.attr('data-other'));
		$('#factor-number').attr('data-other', number);
	});

	$('#factor-official').prop('disabled', false);
});

function setCustomer(customer) {	
	$('input[name=customer_id]').val(customer.id);

	if(customer.mobile) {
		$('#customer-mobile').html(`<p class="form-control-static">`+customer.mobile+` </p>`);
	} else {
		$('#customer-mobile').html(`
			<input class="form-control" name="customer[mobile]" placeholder="شماره تلفن موبایل" `+
				(factor_type == "SELL" ? 'required' : '')+` value="`+
				$('#customer-mobile').data('mobile')+`">`);
	}
	if(customer.address) {
		$('#customer-address').html(`
			<p class="form-control-static">`+customer.address+` </p>`);
	} else {
		$('#customer-address').html(`
			<textarea class="form-control" name="customer[address]" placeholder="آدرس">`+
				($('#customer-address').data('address') || '')
			+`</textarea>
			`);
	}

	var data ={};
	$.each(customer.data, function(index, item) {
		data[item.key] = item.value;
	});

	if(customer.type.name == 'person') {
		$('#customer-data').html(`
			<div class="form-group">
			<label class="control-label col-md-3">شماره ملی:</label>
			<div class="col-md-9">`+ 
			(!data.id_number ? 
				'<input class="form-control" name="customer[id_number]" placeholder="شماره ملی" value='+
					($('#customer-data').data('id-number') || '')
				+'>' :
				'<p class="form-control-static">'+data.id_number)
			+`</div>
			</div>
			`);
	} else {
		$('#customer-data').html(`
			<div class="form-group">
			<label class="control-label col-md-3">شناسه ملی:</label>
			<div class="col-md-9">`+ 
			(!data.national_number ? 
				'<input class="form-control" name="customer[national_number]" placeholder="شناسه ملی" value='+
					($('#customer-data').data('national-number') || '')
				+'>' :
				'<p class="form-control-static">'+data.national_number)
			+`</div>
			</div>
			<div class="form-group">
			<label class="control-label col-md-3">کد اقتصادی:</label>
			<div class="col-md-9">`+ 
			(!data.economic_code ? 
				'<input class="form-control" name="customer[economic_code]" placeholder="کد اقتصادی" value='+
					($('#customer-data').data('economic-code') || '')
				+'>' :
				'<p class="form-control-static">'+data.economic_code)
			+`</div>
			</div>
			`);
	}
}