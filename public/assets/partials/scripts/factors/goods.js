$(function() {
	var goodsSearch = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.whitespace,
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: {
			url: goodsSearchUrl+'?q=%QUERY',
			wildcard: '%QUERY'
		}
	});

	$('#goods-table').repeater({
		show: function () {
			$(this).slideDown();

			$(this).find('p').html('');
			$(this).find('input').val('');

			setTypeaHead($(this).find('.good-name input'), goodsSearch);			
		},
		ready: function ( setIndexes ) {
			$(this).find('.good-name').html(`
				<input class="form-control" type="text" name="goods[][name]" placeholder="نام کالا" required>
				`);

			setIndexes;
		},
		hide: function (deleteElement) {
			if(confirm('آیا از حذف این آیتم مطمئن هستید؟')) {
				$(this).slideUp(deleteElement, function() {
					$(deleteElement).remove();
					calculateTotal();					
				});

			}
		},
		// isFirstItemUndeletable: true
	});

	setTypeaHead($('#goods-table tbody .good-name input'), goodsSearch);
	
	$('#goods-table tbody').bind('typeahead:select', '.good-name input', function(e, good) {		
		var row = $(e.target).parents('tr');		
		setGood(good, row);
	});

	$('#goods-table tbody').on('keyup', '.good-price input[type=text]', function(e) {		
		var val = parseFloat($(this).val().replace(/\,/g, ''));		
		if(!isNaN(val)) {
			$(this).siblings('input[type=hidden]').val(val).trigger('change');
			$(this).val((val).toLocaleString());				
		} else {
			$(this).siblings('input[type=hidden]').val(0).trigger('change');
			$(this).val('');
		}
	});

	$('#goods-table tbody').on('change', '.good-price input[type=hidden], .good-quantity input, .good-discount input', function(e) {
		var row = $(this).parents('tr');

		calculateRow(row);

		calculateTotal();
	});

	var rows = $('#goods-table tbody tr');

	if(rows.length > 0) {
		$.each(rows, function(i, row){
			calculateRow($(row));
		});

		calculateTotal();		
	}
});

function calculateRow(row) {	
	var price = row.find('.good-price input[type=hidden]').val(),
	quantity = row.find('.good-quantity input').val(),
	discount = row.find('.good-discount input').val(),
	taxInput = row.find('.good-tax input');

	var total = (price * quantity) - discount; 

	tax = 0;
	percent = parseInt(taxInput.val());
	if(!taxInput.attr('disabled') && percent && $('#factor-official:checked').length > 0) {
		tax = parseInt(total * (percent / 100));
		row.find('.good-tax p').text((tax).toLocaleString());
	}

	row.find('.good-row-price p').html((price * quantity).toLocaleString());
	row.find('.good-total p').html((total + tax).toLocaleString());
}

function setGood(good, row) {	
	row.find('td:nth-child(2) p').html(good.code);
	row.find('input.good-id ').val(good.id);
	row.find(' input.good-code').val(good.id);
	
	if(factor_type == "SELL") {
		row.find('.good-price input[type=text]').val(good.suggestion_sell_price).trigger("keyup");
		row.find('.good-quantity input').attr('data-max', good.stock);
	} else {
		row.find('.good-price input[type=text]').val(good.suggestion_buy_price).trigger("keyup");
	}
	
	row.find('.good-unit p').html(good.unit);
	row.find('.good-unit input').val(good.unit);

	if(!good.tax){
		row.find('.good-tax input').attr('disabled', true);
	} else {
		row.find('.good-tax input').attr('disabled', false);
		row.find('.good-tax input').val(good.tax);
	}
}