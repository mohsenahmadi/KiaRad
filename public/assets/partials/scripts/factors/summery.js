function calculateTotal() {
	var rows = $('#goods-table tbody tr')
	var price = 0, discount = 0, tax = 0;
// console.log(rows);
	$.each(rows, function(index, row) {
		price = price + parseInt($(row).find('.good-price input[type=hidden]').val() * $(row).find('.good-quantity input').val());
		discount = discount + parseInt($(row).find('.good-discount input').val() || 0);
		tax = tax + parseInt($(row).find('.good-tax p').text() || 0);
	});

	$('#goods-table th.total-row-price').text((price).toLocaleString());
	$('#goods-table th.total-price').text((price -discount + tax).toLocaleString());
	$('#goods-table th.total-discount').text((discount).toLocaleString());
	$('#goods-table th.total-tax').text((tax).toLocaleString());
	
	$('#summery-table tr.total-row-price td').text((price).toLocaleString());
	$('#summery-table tr.total-price td').text((price -discount + tax).toLocaleString());
	$('#summery-table tr.total-discount td').text((discount).toLocaleString());
	$('#summery-table tr.total-tax td').text((tax).toLocaleString());
	
	$('#summery-table .total-sum td').text((price - discount + tax).toLocaleString());
}