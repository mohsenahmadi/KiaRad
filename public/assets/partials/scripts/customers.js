$(function() {
	var customerSerarch = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.whitespace,
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: {
			url: customersSearchUrl+'?q=%QUERY',
			wildcard: '%QUERY'
		}
	});

	setTypeaHead($('#customer-typehead'), customerSerarch);

	$('#customer-typehead').bind('typeahead:select', function(e, customer) {		
		$('input[name=customer_id]').val(customer.id);
	});	

	if(customerFetch) {
		$.ajax({
			url: customerFetch,
		})
		.done(function(customer) {
			$('#customer-typehead').val(customer.name);
			$('input[name=customer_id]').val(customer.id);
		});		
	}
});